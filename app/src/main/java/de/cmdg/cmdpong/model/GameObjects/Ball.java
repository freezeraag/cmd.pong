package de.cmdg.cmdpong.model.GameObjects;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import de.cmdg.cmdpong.helper.CmdgLog;
import de.cmdg.cmdpong.helper.SoundStore;
import de.cmdg.cmdpong.viewController.GamePanel;

/**
 * Erstellt von Michael Lorenz
 * am 28.07.2016.
 * GameObjekt-Ableitung zur Darstellung des Balls.
 */
public class Ball extends GameObject {

    private SoundStore soundStore;
    private boolean isOutside;
    private boolean goal;
    private boolean blink;
    private boolean visible = true;
    private static int DEFAULT_VELOCITY = 15;
    private static int MAX_VELOCITY = 25;
    private static int RESPAWN_TIME = 500;

    private boolean paused = false;

    private Paint paint_ball_visible;
    private Paint paint_ball_hidden;

    private Rect ball;

    private int respawn_x = 0;
    private int respawn_y = 0;
    private int respawn_mov_x = DEFAULT_VELOCITY;
    private int respawn_mov_y = DEFAULT_VELOCITY;

    private boolean useOldMovement = false;
    private int old_movementX = 0;
    private int old_movementY = 0;

    private int width = 40;
    private int height = 40;

    private int respawnTime = GamePanel.PAUSE_BEFORE_START;

    public Ball() {
        super();
        blink = false;

        paint_ball_visible = new Paint();
        paint_ball_visible.setARGB(255, 255, 255, 255);
        paint_ball_hidden = new Paint();
        paint_ball_hidden.setARGB(0, 255, 255, 255);

        ball = new Rect(50, 50, width, height);
        respawn_x = (int) (Math.random() * GamePanel.WIDTH);
        respawn_y = (int) (Math.random() * (GamePanel.HEIGHT / 3));
        respawn();
        respawnTime = RESPAWN_TIME;
    }

    /**
     * Setzt den Context, damit der Ball auf den SoundStore zugreifen kann.
     *
     * @param context
     */
    public void setContext(Context context) {
        this.soundStore = new SoundStore(context);
    }

    /**
     * Gibt die Breite des Balls zurück.
     *
     * @return int
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gibt die Höhe des Balls zurück.
     *
     * @return int
     */
    public int getHeight() {
        return height;
    }

    /**
     * Berechnet die neue Position für den nächsten zu zeichnenden Frame.
     * Fragt Kollisionen ab für:
     * - linke und rechte Wand
     * - untere Wand (Tor)
     * - obere Wand (Bildschirmwechsel)
     *
     * @param bat
     */
    public void update(Bat bat) {

        super.update();

        if (x + width >= GamePanel.WIDTH) { // rechte Wandkollision

            soundStore.playWallHit();

            if (movementY > (-5) && movementY <= 0) {
                movementY = -DEFAULT_VELOCITY;
            }

            movementX *= -1;
            x = GamePanel.WIDTH - width - 1;

        } else if (x <= 0) { // linke Wandkollision

            soundStore.playWallHit();

            if (movementY > (-5) && movementY < 0) {
                movementY = -DEFAULT_VELOCITY;
            }

            movementX *= -1;
            x = 1;

        } else if (y < 0 && movementY < 0) { // obere Wandkollision

            isOutside = true;

        } else if (y + height >= (GamePanel.HEIGHT - GamePanel.OFFSET_FROM_GROUND) && movementY > 0 && !isBlink()) { // untere Wandkollision

            soundStore.playGoalEnemy();
            goal = true;

        } else {

            if (!isBlink()) {
                detectCollisionWithBat(bat);
            }

        }
        ball.set(x, y, x + width, y + height);
    }

    /**
     * Veranlasst das Zeichnen des Ballobjektes.
     * @param canvas
     */
    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawRect(ball, (visible) ? paint_ball_visible : paint_ball_hidden);
    }

    /**
     * Kollisionsabfrage mit dem Schläger.
     *
     * @param bat
     */
    private void detectCollisionWithBat(Bat bat) {

        // Schläger an linker Seite getroffen
        if (ball.intersect(bat.getLeftSide())) {

            soundStore.playBatHit();

            x = bat.getLeftSide().left - width - 1;
            movementX *= -1;

            // Schläger an rechter Seite getroffen
        } else if (ball.intersect(bat.getRightSide())) {

            soundStore.playBatHit();

            x = bat.getRightSide().right + 1;
            movementX *= -1;

            // Schläger an oberer Seite getroffen
        } else if ((x + width) >= bat.getX() && x <= (bat.getX() + bat.getWidth()) && (y + height) >= bat.getY()) {

            soundStore.playBatHit();
            y = bat.getTopSide().top - height - 1;

            int border;
            int directionX;
            float steps = (float) MAX_VELOCITY / (float) bat.getWidth();
            int newMovement_X = DEFAULT_VELOCITY;
            int newMovement_Y = DEFAULT_VELOCITY;

            // Von welcher Richtung kam der Ball
            if (movementY > 0) {  // Von links nach rechts
                border = bat.getLeftSide().left - width; // Linker Rand des Schlägers, rechter Rand des Balls
                directionX = 1;  // Richtung von x merken
            } else { // Von Rechts nach links
                border = bat.getRightSide().right; // Rechter Rand des Schlägers
                directionX = -1; // Richtung von x merken
            }

            // Abstand zum Rand der Ursprungsbewegungsrichtung (Rechter Rand wenn von rechts kommend, linker Rand wenn von links kommend
            int diff = Math.abs(getX() - border);

            // Abstand mal Geschwindigkeit je Abstand ergibt die neue movement_x
            float nMovX = diff * steps;
            nMovX -= Math.abs(movementY - movementX) / 2;
            newMovement_X = (int) nMovX;
            newMovement_X = (newMovement_X == 0) ? 1 : newMovement_X; // muss aber mindestens 1 sein

            newMovement_Y = MAX_VELOCITY - newMovement_X; // Y fällt im selben Verhältnis zur Steigerung von x

            newMovement_X += bat.getMovementX() % MAX_VELOCITY;

            movementX = directionX * newMovement_X;
            movementY = -1 * newMovement_Y;
        }

    }

    /**
     * Veranlasst, dass die letzte ausgeführte Bewegung des Balls übernommen wird, sodass diese
     * für die neue Bewegung nach einem Treffer wieder verwendet werden kann. (Pseudo-Random-Startball)
     */
    public void useOldMovementForRespawn() {
        useOldMovement = true;
        old_movementX = movementX;
        old_movementY = movementY;
    }

    /**
     * Gibt zurück, ob der Ball oberhalb des Displays ist. (Bildschirmwechsel zu Gegenspieler)
     * @return boolean
     */
    public boolean isOutside() {
        return isOutside;
    }

    /**
     * Veranlasst die Löschung des boolean.
     * Wird aufgerufen, wenn sichergestellt ist, dass der Ball das Display des Gegenspielers wirklich erreicht hat.
     */
    public void resetIsOutSide() {
        isOutside = false;
    }

    /**
     * Gibt zurück, ob ein Tor erzielt wurde.
     * @return boolean
     */
    public boolean isGoal() {

        return goal;
    }

    /**
     * Löscht die goal-boolean und setzt den Ball auf eine Nicht-Goal-Position.
     */
    public void resetGoal() {
        respawn_y = (int) (Math.random() * (GamePanel.HEIGHT / 3));
        goal = false;
    }

    /**
     * Vorbereitung vor einem Ball-Respawn.
     */
    private void preRespawn() {
        respawn_x = x;
        respawn_y = y;
        respawn_mov_x = (useOldMovement && old_movementX != 0) ? old_movementX : (int) (Math.random() * DEFAULT_VELOCITY) + 1;
        respawn_mov_y = (useOldMovement && old_movementY != 0) ? old_movementY : DEFAULT_VELOCITY;
        movementX = 0;
        movementY = 0;
        useOldMovement = false;
    }

    /**
     * Veranlasst das Respawnen des Balls nach einem Treffer.
     */
    public void respawn() {

        x = respawn_x;
        y = respawn_y;

        Thread wait = new Thread() {

            public void run() {
                try {
                    Thread.sleep(respawnTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                movementX = (respawn_mov_x == 0) ? DEFAULT_VELOCITY : respawn_mov_x;
                movementY = (respawn_mov_y == 0) ? DEFAULT_VELOCITY : respawn_mov_y;
            }

        };
        wait.start();
    }

    /**
     * Gibt zurück, ob sich der Ball gerade im Blinken-Status befindet.
     * @return
     */
    public boolean isBlink() {
        return blink;
    }

    /**
     * Veranlasst den Beginn des Blinkens des Balls nach einem Treffer.
     * @param blinkTimeMilliSeconds
     * @param respawn
     */
    public void blinkOn(int blinkTimeMilliSeconds, boolean respawn) {
        if (respawn) this.preRespawn();
        this.blink = true;
        this.visible = true;
        this.blink(blinkTimeMilliSeconds, respawn);
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    /**
     * Lässt den Ball Blinken.
     * @param blinkTimeMilliSeconds
     * @param respawn
     */
    private void blink(final int blinkTimeMilliSeconds, final boolean respawn) {

        Thread blinkThread = new Thread() {

            public void run() {
                int timer = blinkTimeMilliSeconds;
                while (timer > 0) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        CmdgLog.e(e.getMessage());
                    }
                    timer -= 500;
                    visible = !visible;
                }
                try {
                    Thread.sleep(750);
                } catch (InterruptedException e) {
                    CmdgLog.e(e.getMessage());
                }
                if(paused) return;
                blinkOff(respawn);
            }
        };
        blinkThread.start();
    }

    /**
     * Veranlasst das Ende des Blinkens des Balls nach einem Treffer.
     * @param respawn
     */
    private void blinkOff(boolean respawn) {
        this.blink = false;
        this.visible = true;
        if (respawn) this.respawn();
    }
}
