package de.cmdg.cmdpong.model.DBObjects;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.HashMap;

import de.cmdg.cmdpong.helper.DBAdapter;

/**
 * Erstellt von Dominik Grüdl
 * am 26.07.16.
 * Enthält den Namen des Spielers
 */
public class User extends DBObject implements Parcelable {

    private String name;

    /**
     * Standardkonstruktor
     */
    public User() {
        super(Type.USER);
    }

    /**
     * Konstruktor für neue User
     * @param name Name des neuen Users
     */
    public User(String name) {
        super(Type.USER);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Zurückgabe der Objektattribute für Datenbankanbindung
     * @return HashMap
     */
    @Override
    public HashMap<String, String> getDTO() {
        HashMap<String, String> hm = new HashMap<>();

        hm.put(DBAdapter.KEY_USER_ID, (this.id != null) ? String.valueOf(this.id) : null);
        hm.put(DBAdapter.KEY_USER_NAME, name);

        return hm;
    }

    /*********** stuff for interface parcelable ***********/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeValue(this.id);
        dest.writeLong(this.createdOn != null ? this.createdOn.getTime() : -1);
        dest.writeLong(this.modifiedOn != null ? this.modifiedOn.getTime() : -1);
    }

    protected User(Parcel in) {
        this.name = in.readString();
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : Type.values()[tmpType];
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        long tmpCreatedOn = in.readLong();
        this.createdOn = tmpCreatedOn == -1 ? null : new Date(tmpCreatedOn);
        long tmpModifiedOn = in.readLong();
        this.modifiedOn = tmpModifiedOn == -1 ? null : new Date(tmpModifiedOn);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
