package de.cmdg.cmdpong.model.GameObjects;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import de.cmdg.cmdpong.viewController.GamePanel;

/**
 * Erstellt von Michael Lorenz
 * am 28.07.2016.
 * GameObjekt-Ableitung zur Darstellung des Spielerschlägers.
 * Hier ist die drag-Objekt-Funktionalität implementiert.
 */
public class Bat extends GameObject {

    private static final int OFFSET_TO_INFOLINE = 30;
    private int width = 350;
    private int height = 40;
    private Paint paint;
    private Rect bat;

    private Rect leftSide;
    private Rect rightSide;
    private Rect topSide;


    public Bat() {
        super();

        paint = new Paint();
        paint.setARGB(255, 255, 255, 255);

        bat = new Rect((GamePanel.WIDTH - width) / 2, GamePanel.HEIGHT - height - GamePanel.OFFSET_FROM_GROUND - OFFSET_TO_INFOLINE, width, GamePanel.HEIGHT - GamePanel.OFFSET_FROM_GROUND - OFFSET_TO_INFOLINE);
        x = bat.left;
        y = bat.top;

        leftSide = new Rect(bat.left - 1, bat.top, bat.left, bat.bottom);
        rightSide = new Rect(bat.right, bat.top, bat.right + 1, bat.bottom);
        topSide = new Rect(bat.left, bat.top - 1, bat.right, bat.top + height + (GamePanel.OFFSET_FROM_GROUND + OFFSET_TO_INFOLINE));
    }

    /**
     * Gibt die Breite des Schlägers zurück.
     * @return int
     */
    public int getWidth() {
        return bat.width();
    }

    /**
     * Gibt die Höhe des Schlägers zurück.
     * @return int
     */
    public int getHeight() {
        return bat.height();
    }

    /**
     * Gibt die linke Seite des Schlägers zurück.
     * @return Rect
     */
    public Rect getLeftSide() {
        return leftSide;
    }

    /**
     * Gibt die rechte Seite des Schlägers zurück.
     * @return Rect
     */
    public Rect getRightSide() {
        return rightSide;
    }

    /**
     * Gibt die obere Seite des Schlägers zurück.
     * @return Rect
     */
    public Rect getTopSide() {
        return topSide;
    }

    /**
     * Berechnet die neue Position für den nächsten zu zeichnenden Frame.
     */
    @Override
    public void update() {
        //super.update();

        bat.set(x, y, x + width, y + height);

        if (x < 0) {
            x = 0;
        }
        if (x + width > GamePanel.WIDTH) {
            x = GamePanel.WIDTH - width;
        }

        leftSide.set(bat.left - 1, bat.top, bat.left, bat.bottom);
        rightSide.set(bat.right, bat.top, bat.right + 1, bat.bottom);
        topSide.set(bat.left, bat.top - 1, bat.right, bat.bottom + (GamePanel.OFFSET_FROM_GROUND + OFFSET_TO_INFOLINE));

    }

    /**
     * Veranlasst das Zeichnen des Schlägerobjektes.
     * @param canvas
     */
    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawRect(bat, paint);
    }
}
