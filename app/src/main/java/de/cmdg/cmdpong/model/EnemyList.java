package de.cmdg.cmdpong.model;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.widget.ListView;

import java.util.ArrayList;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.helper.DBAdapter;
import de.cmdg.cmdpong.layout.layoutHelper.EnemyListAdapter;
import de.cmdg.cmdpong.model.DBObjects.Enemy;

/**
 * Erstellt von Torsten Panzer
 * am 30.07.2016.
 * Hält eine List mit potentielen Gegnern, die dem Spieler angezigt werden.
 * Erzeugt und verwaltet auch den notwendigen Adpater für den ListView.
 */
public class EnemyList {

    private ArrayList<Enemy> enemyList;
    private EnemyListAdapter enemyListAdapter;
    private Activity activity; // Activity in der sich der ListView befindet
    private DBAdapter dba; // Referenz auf den Datenbankadapter (ermöglicht Zugriff auf die Datenbank)

    /**
     * @param activity Activity
     */
    public EnemyList(Activity activity) {
        this.enemyList = new ArrayList<>();
        this.activity = activity;
        this.dba = new DBAdapter(activity.getApplicationContext());
        this.initAdapter();

    }

    /**
     * Erzeugt und setzt den Adapter für den ListView.
     */
    private void initAdapter() {
        ListView listView = (ListView) this.activity.findViewById(R.id.discoveredDevicesList);
        if(listView != null) {
            enemyList = new ArrayList<>();
            enemyListAdapter = new EnemyListAdapter(this.activity, enemyList);
            listView.setAdapter(enemyListAdapter);
        }
    }

    /**
     * @param device Eine Instanz der Klasse BluetoothDevice
     */
    public boolean addEnemyFromBluetoothDevice(BluetoothDevice device) {

        if(enemyList != null) {

            if (!device.getName().contains(activity.getString(R.string.bluetoothDeviceIdent))) {
                return false;
            }

            // Erzeugen des neuen Gegner und merken der Devicedaten
            Enemy e = dba.getEnemy(device.getName().replace(activity.getString(R.string.bluetoothDeviceIdent), ""), String.valueOf(device.getAddress()));
            e.setBluetoothDevice(device);

            for (Enemy en : enemyList) {
                if (en.getGuid().equals(e.getGuid())) { // Wenn Enemy bereits enthalten, dann keine Änderung an der Liste
                    return false;
                }
            }

            // Hinzufügen zur Liste
            enemyList.add(e);

            // Adapter über Änderungen informieren
            enemyListAdapter.notifyDataSetChanged();
            return true;

        }
        return false;
    }

}
