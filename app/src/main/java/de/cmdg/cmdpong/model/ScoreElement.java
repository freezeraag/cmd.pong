package de.cmdg.cmdpong.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.cmdg.cmdpong.model.DBObjects.Enemy;
import de.cmdg.cmdpong.model.DBObjects.Game;

/**
 * Erstellt von Dominik Grüdl
 * am 26.07.16.
 * Wird verwendet um die Spiele pro Enemy zu gruppieren sowie für jedes einzelne Spiel
 *
 */
public class ScoreElement {
    private Enemy enemy;

    // wins und defeats enthält die Anzahl der Siege und Niederlagen bei der Gruppierung nach Enemy
    // oder die Punkte und Gegenpunkte für die Detailansicht
    private int wins, defeats;
    private String duration;

    // Enthält die Spiele bei der Gruppierung nach Enemy
    // oder null für die Detailansicht
    // Ist listGames nicht null handelt es sich um eine Gruppierung und nicht um ein einzelnes Spiel
    private ArrayList<Game> listGames;

    /**
     * Konstruktor für ein neues ScoreElement
     * @param enemy Enemy
     * @param wins Anzahl der Siege gegen enemy
     * @param defeats Anzahl der Niederlage gegen enemy
     */
    public ScoreElement(Enemy enemy, int wins, int defeats) {
        this.enemy = enemy;
        this.wins = wins;
        this.defeats = defeats;
    }

    public Enemy getEnemy() {
        return this.enemy;
    }

    public int getWins() {
        return wins;
    }

    public int getDefeats() {
        return defeats;
    }

    public String getDuration() {
        return duration;
    }

    /**
     * Berechnen der Spieldauer im Format mm:ss
     * @param end End
     * @param start Start
     */
    public void setDuration(Date end, Date start) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(end.getTime() - start.getTime()));
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);

        duration = String.format(Locale.GERMAN, "%02d:%02d", minutes, seconds);
    }

    public void incrementDefeats() {
        this.defeats++;
    }

    public void incrementWins() {
        this.wins++;
    }

    /**
     * Hinzufügen eines Spiels für die Gruppierung der Enemies
     * @param g Game
     */
    public void addGame(Game g) {
        if(listGames == null) {
            listGames = new ArrayList<>();
        }
        listGames.add(g);
    }

    public ArrayList<Game> getListGames() {
        return listGames;
    }
}