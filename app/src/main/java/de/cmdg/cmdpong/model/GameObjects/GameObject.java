package de.cmdg.cmdpong.model.GameObjects;

import android.graphics.Canvas;

/**
 * Erstellt von Michael Lorenz
 * am 28.07.2016.
 * Super-Klasse aller darzustellenden Game-Objekte.
 * Jedes dieser Objekte besitzt eine Bitmap des Pixel-Objekts,
 * eine x- und y-Position im SurfaceView-Canvas und
 * ein dx und dy für die relative Positionsveränderung im nächsten Frame.
 */
public class GameObject {

    protected int x;
    protected int y;
    protected int movementX;
    protected int movementY;

    public GameObject() {
    }

    /**
     * Hier wird die Positionsveränderung im SurfaceView-Canvas für den nächsten Frame berechnet.
     */
    public void update() {
        x += movementX;
        y += movementY;
    }

    /**
     * Hier wird veranlasst, dass die Bitmap, an der in update() berechneten neuen Position im
     * SurfaceView-Canvas, dargestellt wird.
     *
     * @param canvas
     */
    public void draw(Canvas canvas) {
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setMovementX(int movementX) {
        this.movementX = movementX;
    }

    public void setMovementY(int movementY) {
        this.movementY = movementY;
    }

    public int getMovementX() {
        return movementX;
    }

    public int getMovementY() {
        return movementY;
    }
}
