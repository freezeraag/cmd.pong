package de.cmdg.cmdpong.model.DBObjects;

import java.util.Date;
import java.util.HashMap;

/**
 * Erstellt von Dominik Grüdl
 * am 26.07.16.
 * Parent class für alle Klassen die in der Datenbank hinterlegt werden
 */
public abstract class DBObject {

    // Alle Objekte in der Datenbank haben die folgenden Variablen
    public enum Type {USER, ENEMY, OPTIONS, GAME}
    protected Type type;
    protected Integer id; // Soll NULL bei Initialisierung erzeugen
    protected Date createdOn, modifiedOn;

    /**
     * Standardkonstruktor für Model-Klassen
     * @param type Type des Models
     */
    public DBObject(Type type) {
        this.type = type;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public int getId() {
        return this.id == null ? 0 : this.id;
    }

    public Type getType() {
        return this.type;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    /**
     * Zurückgabe der Objektattribute für Datenbankanbindung
     * @return HashMap
     */
    public abstract HashMap<String, String> getDTO();

    /*********** stuff for interface parcelable ***********/

    protected DBObject() {}
}
