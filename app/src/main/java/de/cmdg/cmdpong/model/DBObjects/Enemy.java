package de.cmdg.cmdpong.model.DBObjects;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.HashMap;

import de.cmdg.cmdpong.helper.DBAdapter;

/**
 * Erstellt von Dominik Grüdl
 * am 26.07.16.
 */
public class Enemy extends DBObject implements Parcelable {
    private String name;
    private String guid; // Global Unique Identifier - Bluetooth-MAC + User-ID
    private BluetoothDevice btDevice;

    /**
     * Standardkonstruktor
     */
    public Enemy() {
        super(Type.ENEMY);
    }

    /**
     * Konstruktor für neue Enemies
     * @param name Name des neuen Enemies
     * @param guid Bluetooth-MAC und lokaler ID
     */
    public Enemy(String name, String guid) {
        super(Type.ENEMY);
        this.name = name;
        this.guid = guid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public String getGuid() {
        return guid;
    }

    public BluetoothDevice getBluetoothDevice() {
        return btDevice;
    }

    public void setBluetoothDevice(BluetoothDevice btDevice) {
        this.btDevice = btDevice;
    }

    /**
     * Zurückgabe der Objektattribute für Datenbankanbindung
     * @return HashMap
     */
    @Override
    public HashMap<String, String> getDTO() {
        HashMap<String, String> hm = new HashMap<>();

        hm.put(DBAdapter.KEY_ENEMY_ID, (this.id != null) ? String.valueOf(this.id) : null);
        hm.put(DBAdapter.KEY_ENEMY_NAME, name);
        hm.put(DBAdapter.KEY_ENEMY_GUID, guid);

        return hm;
    }

    /*********** stuff for interface parcelable ***********/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.guid);
        dest.writeParcelable(this.btDevice, flags);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeValue(this.id);
        dest.writeLong(this.createdOn != null ? this.createdOn.getTime() : -1);
        dest.writeLong(this.modifiedOn != null ? this.modifiedOn.getTime() : -1);
    }

    protected Enemy(Parcel in) {
        this.name = in.readString();
        this.guid = in.readString();
        this.btDevice = in.readParcelable(BluetoothDevice.class.getClassLoader());
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : Type.values()[tmpType];
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        long tmpCreatedOn = in.readLong();
        this.createdOn = tmpCreatedOn == -1 ? null : new Date(tmpCreatedOn);
        long tmpModifiedOn = in.readLong();
        this.modifiedOn = tmpModifiedOn == -1 ? null : new Date(tmpModifiedOn);
    }

    public static final Parcelable.Creator<Enemy> CREATOR = new Parcelable.Creator<Enemy>() {
        @Override
        public Enemy createFromParcel(Parcel source) {
            return new Enemy(source);
        }

        @Override
        public Enemy[] newArray(int size) {
            return new Enemy[size];
        }
    };
}
