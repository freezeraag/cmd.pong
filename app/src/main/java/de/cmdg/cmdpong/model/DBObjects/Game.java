package de.cmdg.cmdpong.model.DBObjects;

import java.util.HashMap;

import de.cmdg.cmdpong.helper.DBAdapter;

/**
 * Erstellt von Dominik Grüdl
 * am 26.07.16.
 * Enthält die Daten zu einem Spiel
 */
public class Game extends DBObject {

    public int pointsToWin = 7;
    public static final int MIN_OFFSET_TO_WIN = 2;

    public static final int WINNER_WINNER_CHICKEN_DINNER = 1;
    public static final int WINNER_IS_ENEMY = 2;

    public static final int NO_WINNER = 0;

    private User user;
    private Enemy enemy;
    private int pointsUser;
    private int pointsEnemy;

    /**
     * Standardkonstruktor
     */
    public Game() {
        super(Type.GAME);
        this.pointsUser = 0;
        this.pointsEnemy = 0;
    }

    /**
     * Konstruktor für neue Games
     * @param user User-Objekt
     * @param enemy Enemy-Objekt
     */
    public Game(User user, Enemy enemy) {
        super(Type.GAME);
        this.user = user;
        this.enemy = enemy;
        this.pointsUser = 0;
        this.pointsEnemy = 0;
    }

    public void incrementUserPoints() {
        this.pointsUser++;
    }

    public void incrementEnemyPoints() {
        this.pointsEnemy++;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }

    public Enemy getEnemy() {
        return this.enemy;
    }

    public int getPointsEnemy() {
        return pointsEnemy;
    }

    public void setPointsEnemy(int pointsEnemy) {
        this.pointsEnemy = pointsEnemy;
    }

    public int getPointsUser() {
        return pointsUser;
    }

    public void setPointsUser(int pointsUser) {
        this.pointsUser = pointsUser;
    }

    public int getPointsToWin() {
        return pointsToWin;
    }

    public int isOver() {
        if(pointsUser == pointsToWin) {
            if (Math.abs(pointsUser - pointsEnemy) >= MIN_OFFSET_TO_WIN) {
                return WINNER_WINNER_CHICKEN_DINNER;
            } else {
                pointsToWin++;
            }
        } else if(pointsEnemy == pointsToWin ) {
            if (Math.abs(pointsUser - pointsEnemy) >= MIN_OFFSET_TO_WIN) {
                return WINNER_IS_ENEMY;
            } else {
                pointsToWin++;
            }
        }
        return NO_WINNER;
    }

    public int isMatchball() {
        if(Math.abs(pointsUser - pointsEnemy) >= MIN_OFFSET_TO_WIN-1) {
            if(pointsUser == pointsToWin -1) {
                return WINNER_WINNER_CHICKEN_DINNER;
            }
            if(pointsEnemy == pointsToWin -1) {
                return WINNER_IS_ENEMY;
            }
        }
        return NO_WINNER;
    }

    /**
     * Zurückgabe der Objektattribute für Datenbankanbindung
     * @return HashMap
     */
    @Override
    public HashMap<String, String> getDTO() {
        HashMap<String, String> hm = new HashMap<>();

        hm.put(DBAdapter.KEY_GAME_ID, (this.id != null) ? String.valueOf(this.id) : null);
        hm.put(DBAdapter.KEY_USER_ID, (user.id != null) ? String.valueOf(user.getId()) : null);
        hm.put(DBAdapter.KEY_ENEMY_ID, (enemy.id != null) ? String.valueOf(enemy.getId()) : null);
        hm.put(DBAdapter.KEY_GAME_POINTS_USER, String.valueOf(pointsUser));
        hm.put(DBAdapter.KEY_GAME_POINTS_ENEMY, String.valueOf(pointsEnemy));

        return hm;
    }
}