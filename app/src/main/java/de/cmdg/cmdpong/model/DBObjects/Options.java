package de.cmdg.cmdpong.model.DBObjects;

import java.util.HashMap;

import de.cmdg.cmdpong.helper.DBAdapter;

/**
 * Erstellt von Dominik Grüdl
 * am 26.07.16.
 * Enthält eine Option als Schlüssel-Wert-Paare
 */
public class Options extends DBObject {

    private String key;
    private String value;

    /**
     * Standardkonstruktor
     */
    public Options() {
        super(Type.OPTIONS);
    }

    public Options(String key, String value) {
        super(Type.OPTIONS);
        this.key = key;
        this.value = value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    /**
     * Zurückgabe der Objektattribute für Datenbankanbindung
     * @return HashMap
     */
    @Override
    public HashMap<String, String> getDTO() {

        HashMap<String, String> hm = new HashMap<>();

        hm.put(DBAdapter.KEY_OPTIONS_ID, (this.id != null) ? String.valueOf(this.id) : null);
        hm.put(DBAdapter.KEY_OPTIONS_KEY, key);
        hm.put(DBAdapter.KEY_OPTIONS_VALUE, value);

        return hm;
    }
}