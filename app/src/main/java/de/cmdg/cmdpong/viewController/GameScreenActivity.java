package de.cmdg.cmdpong.viewController;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Vibrator;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Method;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.communication.MessageService;
import de.cmdg.cmdpong.communication.bluetooth.BTClient;
import de.cmdg.cmdpong.communication.bluetooth.BTController;
import de.cmdg.cmdpong.communication.bluetooth.BTEventListener;
import de.cmdg.cmdpong.communication.bluetooth.BTServer;
import de.cmdg.cmdpong.communication.bluetooth.BTServiceConnector;
import de.cmdg.cmdpong.communication.bluetooth.NoBluetoothAdapterException;
import de.cmdg.cmdpong.helper.CmdgLog;
import de.cmdg.cmdpong.model.DBObjects.Game;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.viewController.fragments.gameScreen.CountDownFragment;
import de.cmdg.cmdpong.viewController.fragments.gameScreen.ReconnectFragment;
import de.cmdg.cmdpong.viewController.fragments.mainMenu.MenuGameFragment;
import de.cmdg.cmdpong.controller.GameController;
import de.cmdg.cmdpong.viewController.fragments.gameScreen.LoseFragment;
import de.cmdg.cmdpong.viewController.fragments.gameScreen.PauseGameReceiverFragment;
import de.cmdg.cmdpong.viewController.fragments.gameScreen.PauseGameSenderFragment;
import de.cmdg.cmdpong.viewController.fragments.gameScreen.WaitingForEnemyFragment;
import de.cmdg.cmdpong.viewController.fragments.gameScreen.WinFragment;

/**
 * Erstellt von Michael Lorenz
 * am 26.07.2016.
 * Die Activity, in der das Spiel abläuft. Die GamePanel-Klasse dient hierfür als SurfaceView, der in der Activity
 * angezeigt wird.
 */
public class GameScreenActivity extends AppCompatActivity {

    private WaitingForEnemyFragment waitingFragment;
    private PauseGameSenderFragment pauseSenderFragment;
    private PauseGameReceiverFragment pauseReceiverFragment;
    private WinFragment winFragment;
    private ReconnectFragment reconnectFragment;
    private LoseFragment loseFragment;
    private CmdgFragment mFragment;
    private CountDownFragment countDownFragment;
    private BTController btController;
    private BTServiceConnector btsc;
    private GameController gameController;
    private Activity self;
    private boolean paused;
    private boolean pausedSender;
    private boolean fragmentShown;
    private GamePanel gamePanel;
    private CmdgTextView userPoints;
    private CmdgTextView enemyPoints;
    private Vibrator vibrator;
    private CmdgTextView userName;
    private CmdgTextView enemyName;
    private TextView separator;
    private CmdgTextView matchball;
    private LinearLayout infoLine;
    private boolean countDownStarted = false;
    private boolean waitingForDiscoverable = false;

    private static final long[] WIN_VIBRATION_PATTERN = {0, 500, 300, 500, 300, 500};
    private static final int VIBRATE_DURATION_ENEMY_POINT = 750;

    BroadcastReceiver bCR = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case BTClient.CLIENT_CONNECTION_FAILED:
                case BTServer.SERVER_CONNECTION_FAILED:
                    Toast.makeText(GameScreenActivity.this, getString(R.string.connectionFailed), Toast.LENGTH_SHORT).show();
                    if (gameController != null) {
                        gameController.exitGame(false);
                    } else {
                        finish();
                    }
                    break;
                case GameController.STATE_CHANGED:
                    if (gameController.getCurrentState() == GameController.state.READY) {
                        startGame();
                    }
                    break;
                case MessageService.CONNECTION_LOST:
                    /*Toast.makeText(GameScreenActivity.this, getString(R.string.connectionLost), Toast.LENGTH_SHORT).show();
                    if (gameController != null) {
                        gameController.exitGame(false);
                    } else {
                        finish();
                    }*/
                    if (gameController != null) {
                        showReconnectFragment();
                        gameController.reconnectGame();
                    } else {
                        Toast.makeText(GameScreenActivity.this, getString(R.string.connectionFailed), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    break;
                case GamePanel.NO_CHANGE_ACK:
                case GamePanel.NO_NEWPOINT_ACK:
                    Toast.makeText(GameScreenActivity.this, getString(R.string.lostMessage), Toast.LENGTH_SHORT).show();
                    if (gameController != null) {
                        gameController.exitGame(false);
                    } else {
                        finish();
                    }
                    break;
                case BTEventListener.BROADCAST_NOT_DISCOVERABLE: // Discoverable changed
                    if(waitingForDiscoverable) {
                        finish();
                    }
                    break;
                case BTEventListener.BROADCAST_ACTIVATE_DISCOVERABLE_STARTED:
                    waitingForDiscoverable = true;
                    break;
                case BTEventListener.BROADCAST_ACTIVATE_DISCOVERABLE_FINISHED:
                    waitingForDiscoverable = false;
                    break;
                case CountDownFragment.BROADCAST_COUNTDOWN_READY:
                    if (paused) {
                        paused = false;
                    }
                    countDownStarted = false;
                    break;
                case CountDownFragment.BROADCAST_COUNTDOWN_STARTED:
                    countDownStarted = true;
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        paused = false;
        fragmentShown = false;
        pausedSender = false;
        waitingFragment = new WaitingForEnemyFragment();
        pauseSenderFragment = new PauseGameSenderFragment();
        pauseReceiverFragment = new PauseGameReceiverFragment();
        winFragment = new WinFragment();
        loseFragment = new LoseFragment();
        countDownFragment = new CountDownFragment();
        reconnectFragment = new ReconnectFragment();
        mFragment = null;

        // erzwinge Vollbild
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        setContentView(R.layout.activity_game_screen);
        gamePanel = (GamePanel) findViewById(R.id.gameSurface);

        Button pauseView = (Button) findViewById(R.id.pauseBtn);
        pauseView.setTypeface(Typeface.createFromAsset(getAssets(), getResources().getString(R.string.cmdgTypeface)));
        pauseView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseGame();
            }
        });

        self = this;

        registerReceiver(bCR, new IntentFilter(BTClient.CLIENT_CONNECTION_FAILED));
        registerReceiver(bCR, new IntentFilter(BTServer.SERVER_CONNECTION_FAILED));
        registerReceiver(bCR, new IntentFilter(MessageService.CONNECTION_LOST));
        registerReceiver(bCR, new IntentFilter(GameController.STATE_CHANGED));
        registerReceiver(bCR, new IntentFilter(BTEventListener.BROADCAST_NOT_DISCOVERABLE));
        registerReceiver(bCR, new IntentFilter(BTEventListener.BROADCAST_ACTIVATE_DISCOVERABLE_STARTED));
        registerReceiver(bCR, new IntentFilter(BTEventListener.BROADCAST_ACTIVATE_DISCOVERABLE_FINISHED));
        registerReceiver(bCR, new IntentFilter(CountDownFragment.BROADCAST_COUNTDOWN_STARTED));
        registerReceiver(bCR, new IntentFilter(CountDownFragment.BROADCAST_COUNTDOWN_READY));
        registerReceiver(bCR, new IntentFilter(CountDownFragment.BROADCAST_COUNTDOWN_STARTED));

        btsc = new BTServiceConnector(this, new BTServiceConnector.BTServiceConnectorCallback() {
            @Override
            public void onConnected(BTController btc) {
                btController = btc;
                gameController = new GameController(self, btc);
                gameController.setGamePanel(gamePanel);
                userPoints = (CmdgTextView) findViewById(R.id.userPoints);
                enemyPoints = (CmdgTextView) findViewById(R.id.enemyPoints);
                separator = (TextView) findViewById(R.id.separator);
                separator.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryLight));
                matchball = (CmdgTextView) findViewById(R.id.matchball);
                registerReceiver(gameController, new IntentFilter(GameController.INIT_SERVER));
                registerReceiver(gameController, new IntentFilter(GameController.INIT_CLIENT));
                registerReceiver(gameController, new IntentFilter(GamePanel.NEW_ENEMY_POINT));
                registerReceiver(gameController, new IntentFilter(GamePanel.NEW_USER_POINT));

                try {
                    btController.initBTAdapter();
                } catch (NoBluetoothAdapterException e) {
                    e.printStackTrace();
                }
            }
        }, new BTController.BTControllerCallbacks() {

            @Override
            public void onBTAdapterOn() {
                Intent intent = getIntent();
                Bundle extras = intent.getExtras();

                final int type = extras.getInt(getString(R.string.startType));
                if (type == MenuGameFragment.START_AS_CLIENT) {
                    startAsClient(extras);
                } else if (type == MenuGameFragment.START_AS_SERVER) {
                    startAsServer();
                }
            }

        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!paused) {
            showWaitForEnemyFragment();
        }
        pausedSender = false;
    }

    public void onRestart() {
        super.onRestart();
        if (paused) {
            showPauseFragment(pausedSender);
        }
        pausedSender = false;
    }

    private void startAsServer() {
        btController.startServer();
    }

    private void startAsClient(Bundle extras) {

        BluetoothDevice device = extras.getParcelable(BluetoothDevice.EXTRA_DEVICE);

        if (device != null) {
            if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    device.createBond();
                } else {
                    try {
                        Method createBondMethod = device.getClass().getMethod("createBond", (Class[]) null);
                        createBondMethod.invoke(device, (Object[]) null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                btController.startClientConnection(device);
            }
        } else {
            finish();
        }

    }

    public void showWaitForEnemyFragment() {
        showFragment(waitingFragment);
    }

    public void showReconnectFragment() {
        showFragment(reconnectFragment);
        paused = true;
    }

    public void showCountDownFragment(int start) {
        countDownFragment.setStart(start);
        showFragment(countDownFragment);
    }

    public void hideCountDownFragment() {
        hideFragment();
    }

    public void removeWaitForEnemyFragment() {
        hideFragment();
    }

    public void startGame() {
        removeWaitForEnemyFragment();
        gamePanel.setGameController(gameController);
        userName = (CmdgTextView) findViewById(R.id.userName);
        enemyName = (CmdgTextView) findViewById(R.id.enemyName);
        userName.setText(gameController.getCurrentUser().getName());
        enemyName.setText(gameController.getCurrentEnemy().getName());
        updatePoints();
        showCountDownFragment((GamePanel.PAUSE_BEFORE_START / 1000));
        gamePanel.startGameLoop();

    }

    public void updatePoints() {
        int playerMatchball = gameController.getCurrentGame().isMatchball();

        if (playerMatchball != Game.NO_WINNER) {
            matchball.setText(R.string.matchball);
            if (playerMatchball == Game.WINNER_WINNER_CHICKEN_DINNER) {
                matchball.setTextColor(ContextCompat.getColor(this, R.color.colorSuccessLight));
            } else if (playerMatchball == Game.WINNER_IS_ENEMY) {
                matchball.setTextColor(ContextCompat.getColor(this, R.color.colorDangerLight));
            }
        } else {
            matchball.setText("");
        }

        userPoints.setText(String.valueOf(gameController.getCurrentGame().getPointsUser()));
        enemyPoints.setText(String.valueOf(gameController.getCurrentGame().getPointsEnemy()));
    }


    public void notifyNewPoint(boolean userPoint) {
        if (!userPoint && vibrator != null && vibrator.hasVibrator()) {
            vibrator.vibrate(VIBRATE_DURATION_ENEMY_POINT);
        }
    }

    public void notifyExitGame(boolean won) {
        String toastText = getString(R.string.gameExitedNotify, ((won) ? gameController.getCurrentUser().getName() : gameController.getCurrentEnemy().getName()));
        Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    public void pauseGame() {
        if (!paused) {
            showPauseFragment(true);
            gameController.pauseGame(true);
            paused = pausedSender = true;
        }
    }

    public void showPauseFragment(boolean isSender) {
        showFragment((isSender) ? pauseSenderFragment : pauseReceiverFragment);
    }

    public void removePauseFragement() {
        hideFragment();
    }

    private void showFragment(CmdgFragment fragment) {
        // erzwinge Vollbild
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        fragmentShown = true;
        mFragment = fragment;
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.gameFrame, mFragment, mFragment.getFragmentTag())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commitAllowingStateLoss();


    }

    private void hideFragment() {
        // erzwinge Vollbild
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        FragmentManager fm = getSupportFragmentManager();
        if (mFragment != null) {
            fm.beginTransaction()
                    .remove(mFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commitAllowingStateLoss();
        }
        fragmentShown = false;
        mFragment = null;
    }

    public void exitGame() {
        gameController.getCurrentGame().setPointsEnemy(gameController.getCurrentGame().getPointsToWin());
        gameController.exitGame(true);
        notifyExitGame(false);
    }

    public void showEndFragment(boolean userHasWon, int userPoints, int enemyPoints) {

        gamePanel.setVisibility(View.GONE);
        infoLine = (LinearLayout) findViewById(R.id.infoLine);
        if (infoLine != null) {
            infoLine.setVisibility(View.GONE);
        }

        if (userHasWon) {
            winFragment.setPointsEnemy(enemyPoints);
            winFragment.setPointsUser(userPoints);
            if (vibrator != null && vibrator.hasVibrator()) {
                vibrator.vibrate(WIN_VIBRATION_PATTERN, -1);
            }
        } else {
            loseFragment.setPointsEnemy(enemyPoints);
            loseFragment.setPointsUser(userPoints);
        }
        showFragment((userHasWon) ? winFragment : loseFragment);
    }

    public void resumeGame() {
        pausedSender = false;
        gameController.resumeGame(true);
    }

    public GameController getGameController() {
        return gameController;
    }

    private void exit() {

        try {
            unregisterReceiver(bCR);
        } catch (Exception ex) {
            CmdgLog.e(ex.getMessage());
        }

        if (gameController != null) {
            gameController.finish();
        }
        if (btController != null) {
            btController.stopServer();
            btController.closeConnection();
        }
        if (btsc != null) {
            btsc.disconnect();
        }
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }


    public void onPause() {
        super.onPause();
        if (!fragmentShown) {
            pauseGame();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        exit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BTController.REQUEST_ENABLE_DISCOVERABLE) {
            if (resultCode <= 0) {
                sendBroadcast(new Intent(BTEventListener.BROADCAST_NOT_DISCOVERABLE));
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (countDownStarted) {
                CountDownFragment cf = (CountDownFragment) mFragment;
                cf.exit();
            }
            if (gameController.getCurrentState() == GameController.state.READY) {
                pauseGame();
                return true;
            } else {
                if (btController != null) {
                    // Zurücksetzen des Bluetooth-Anzeigenamens. Wir müssen nun nicht mehr gefiltert werden.
                    btController.resetBluetoothName();
                }
                exit();
                finish();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
