package de.cmdg.cmdpong.viewController;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import de.cmdg.cmdpong.communication.messages.AcknowledgeMessage;
import de.cmdg.cmdpong.controller.GameController;
import de.cmdg.cmdpong.controller.MainGameThread;
import de.cmdg.cmdpong.helper.SoundStore;
import de.cmdg.cmdpong.model.GameObjects.Ball;
import de.cmdg.cmdpong.model.GameObjects.Bat;

/**
 * Erstellt von Michael Lorenz
 * am 28.07.2016.
 *
 * Diese Klasse ist der Game-Objekt-Container. Sie instanziiert alle darzustellenden Objekte und
 * stellt diese auf dem Display dar. Weiterhin wird von dieser Klasse der Haupt-Thread instanziiert und gestartet,
 * welcher den Game-Loop am Leben hält.
 */
public class GamePanel extends SurfaceView implements SurfaceHolder.Callback {

    public static final String NEW_USER_POINT = "de.cmdg.cmdpong.newUserPoint";
    public static final String NEW_ENEMY_POINT = "de.cmdg.cmdpong.newEnemyPoint";
    public static final String NO_CHANGE_ACK = "de.cmdg.cmdpong.noChangeAck";
    public static final String NO_NEWPOINT_ACK = "de.cmdg.cmdpong.noNewPointAck";

    public static final int OFFSET_FROM_GROUND = 100;

    public static final int PAUSE_BEFORE_RESUME = 3000; // in Sekunden
    public static final int PAUSE_BEFORE_START = 3000; // in Sekunden

    public static final int WIDTH = 1920;
    public static final int HEIGHT = 1080;
    private MainGameThread mainGameThread;
    private GameController gc;

    // Game-Objects
    private Bat bat;
    private Ball ball;

    // Bat-Bewegungsvariablen
    private float startX = 0;
    private int originXBat;
    private int lastXBat;

    private boolean changeAck = true;
    private boolean newPointAck = true;

    private int changeAckCounter = 0;
    private int newPointAckCounter = 0;

    private static int MAX_LOST_ACK = 15;

    // zum Abspielen von Sound-Ressourcen
    private SoundStore soundStore;

    // gibt an, welcher Spieler gerade den Ball auf dem Screen hat
    private boolean visibleBall;

    private long updateIndex;

    public GamePanel(Context context) {
        super(context);
        init();
    }

    public GamePanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * Vorbereitende Maßnahmen.
     * Aufruf durch Konstruktor.
     */
    public void init() {
        soundStore = new SoundStore(getContext());
        updateIndex = 0;

        // callback hinzufügen, um Events abfangen zu können
        getHolder().addCallback(this);

        // mache gamePanel focusable, damit events gehandlet werden können
        setFocusable(true);
    }

    public void setGameController(GameController gameController) {
        this.gc = gameController;
        this.visibleBall = gc.isHost();
    }

    /**
     * Hier werden alle darzustellenden Objekte instanziiert und dabei deren Position im holder pixelgetreu angegeben.
     * Zudem kann hier bereits der Haupt-Thread gestartet werden.
     *
     * @param holder
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        bat = new Bat();
        ball = new Ball();
        ball.setContext(getContext());
        if (visibleBall) {
            ball.respawn();
        }

    }

    /**
     * Veranlasst den Start des Game-Loops im Main-Game-Thread.
     */
    public void startGameLoop() {

        ball.blinkOn(PAUSE_BEFORE_RESUME, true);

        mainGameThread = new MainGameThread(getHolder(), this);
        mainGameThread.setRunning(true);
        mainGameThread.start();

    }

    /**
     * Veranlasst den Stop des Game-Loops im Main-Game-Thread.
     */
    public void stopGameLoop() {
        if (mainGameThread != null) {
            mainGameThread.setRunning(false);
            mainGameThread = null;
        }
    }

    /**
     * Veranlasst die Wiedereinkehr in den Game-Loop des Main-Game-Threads.
     */
    public void resumeGameLoop() {
        if (mainGameThread != null) {

            ball.useOldMovementForRespawn();
            ball.setPaused(false);
            ball.blinkOn(PAUSE_BEFORE_RESUME, true);
            mainGameThread.setPaused(false);

        }
    }

    /**
     * Veranlasst die Pausierung des Game-Loops im Main-Game-Thread.
     */
    public void pauseGameLoop() {
        if (mainGameThread != null) {
            ball.setPaused(true);
            mainGameThread.setPaused(true);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    /**
     * Routine beim Zerstören des SurfaceViews.
     *
     * @param holder
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                if (mainGameThread != null) {
                    mainGameThread.setRunning(false);
                    mainGameThread.join();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            retry = false;
        }
    }

    /**
     * Handler für Touch-Routinen auf dem Display.
     * Lässt den Schläger bewegen.
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            startX = event.getX();
            lastXBat = (int) startX;
            originXBat = bat.getX();
            return true;
        } else if (event.getActionMasked() == MotionEvent.ACTION_MOVE) {
            int newPosition = originXBat + ((int) (event.getX() - startX));
            if (newPosition >= 0 && (newPosition + bat.getWidth()) < WIDTH) {

                bat.setX(newPosition);
                bat.setMovementX(newPosition - lastXBat); // Speichern der Geschwindigkeit mit der der Schläger bewegt wurde
                bat.update();
                lastXBat = newPosition;

            }
            return true;
        } else if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            bat.setMovementX(0);
            return true;
        }
        return super.onTouchEvent(event);
    }

    /**
     * Diese Methode, vom Haupt-Thread aufgerufen, veranlasst, dass von allen im SurfaceViews befindlichen Objekten
     * die update()-Methode aufrufen wird, sodass diese ihre neue Position im SurfaceView erhalten.
     */
    public void update() {
        bat.update();
        if (visibleBall) {

            ball.update(bat);

            if (ball.isGoal()) {

                newPointAck = false;
                gc.sendBallPosition(ball, updateIndex);
                ball.setY(GamePanel.HEIGHT - GamePanel.OFFSET_FROM_GROUND - ball.getHeight() - 1);
                ball.blinkOn(2000, true);
                ball.resetGoal();
                getContext().sendBroadcast(new Intent(NEW_ENEMY_POINT));

            } else if (ball.isOutside()) {

                gc.sendBallPosition(ball, updateIndex);
                visibleBall = false;
                changeAck = false;

            }

            if(!newPointAck) {
                if (updateIndex % (MainGameThread.FPS / 10) == 0) {
                    gc.sendBallPosition(ball, updateIndex);
                    newPointAckCounter++;
                }
                if(newPointAckCounter >= MAX_LOST_ACK) {
                    getContext().sendBroadcast(new Intent(NO_NEWPOINT_ACK));
                }
            }

        } else {

            if(!changeAck) {
                if (updateIndex % (MainGameThread.FPS / 10) == 0) {
                    gc.sendBallPosition(ball, updateIndex);
                    changeAckCounter++;
                }
                if(changeAckCounter >= MAX_LOST_ACK) {
                    getContext().sendBroadcast(new Intent(NO_CHANGE_ACK));
                }
            }

        }

        updateIndex++;

    }

    /**
     * Diese Methode, vom Haupt-Thread aufgerufen, veranlasst, dass von allen im SurfaceViews befindlichen Objekten
     * die draw()-Methode aufrufen wird, sodass diese sich im übergeben SurfaceView-Canvas darstellen.
     *
     * @param canvas
     */
    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        final float scaleFactorX = (float) getWidth() / WIDTH;
        final float scaleFactorY = (float) getHeight() / HEIGHT;
        if (canvas != null) {
            final int savedState = canvas.save();
            canvas.scale(scaleFactorX, scaleFactorY);
            bat.draw(canvas);
            if (visibleBall) {
                ball.draw(canvas);
            }
            canvas.restoreToCount(savedState);
        }
    }

    public void changeAcknowledgeRetrieved() {
        changeAck = true;
        changeAckCounter = 0;
    }

    public void newPointAcknowledgeRetrieved() {
        newPointAck = true;
        newPointAckCounter = 0;
    }

    /**
     * Veranlsst die Verarbeitung der Ballposition auf dem Display des Gegenspielers.
     * Hier erfährt man, ob man selbst einen Treffer auf dem anderen Display erzielt hat, um sich einen Punkt gutzuschreiben.
     * @param x
     * @param y
     * @param movX
     * @param movY
     */
    public void setEnemyBallPosition(int x, int y, int movX, int movY) {

        if(changeAck && !visibleBall) {

            if (y < 0 && movY < 0) {
                visibleBall = true;
                ball.resetIsOutSide();
                ball.setX(GamePanel.WIDTH - x);
                ball.setY(0);
                ball.setMovementX((-1) * movX);
                ball.setMovementY(Math.abs(movY));
                gc.sendAcknowledge(AcknowledgeMessage.ackknowledgeType.CHANGE);
            }

            if (y + ball.getHeight() >= (GamePanel.HEIGHT - GamePanel.OFFSET_FROM_GROUND) && movY > 0) {
                soundStore.playGoalUser();
                getContext().sendBroadcast(new Intent(NEW_USER_POINT));
                gc.sendAcknowledge(AcknowledgeMessage.ackknowledgeType.NEW_POINT);
            }

        }

    }

}
