package de.cmdg.cmdpong.viewController.fragments.mainMenu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.helper.ConditionBuilder;
import de.cmdg.cmdpong.helper.DBAdapter;
import de.cmdg.cmdpong.layout.layoutHelper.UserAdapter;
import de.cmdg.cmdpong.model.DBObjects.Options;
import de.cmdg.cmdpong.model.DBObjects.User;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.viewController.MainMenuActivity;

/**
 * Erstellt von Dominik Grüdl
 * am 02.08.16.
 * Anzeigen der Entwickler in Form eines Bildes sowie deren Namen
 * Der Name kann angeklickt werden und öffnet das Xing-Profil des jeweiligen Entwicklers im Browser
 */
public class ChangeUserFragment extends CmdgFragment {

    private View view;
    private DBAdapter dba;
    private UserAdapter userAdapter;
    private ArrayList<User> userList;

    // Zum Benachrichtigen des UserAdapters
    public static final String NEW_USER_CREATED = "de.cmdg.cmdpong.userCreated";

    // Empfangen von Ereignissen aus anderen Fragments
    BroadcastReceiver bcr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case MenuScoreFragment.SCORE_FRAGMENT_OPENED:
                    closeFragment();
                    break;
                case UserAdapter.USER_MODIFIED:
                    updateView();
                    break;
            }
        }
    };

    /**
     * Erzeugen des Views
     *
     * @param inflater           Inflater
     * @param container          Container
     * @param savedInstanceState SavedInstanceState
     * @return View
     */
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_change_user, container, false);

        ((MainMenuActivity) getActivity()).resetRoot();

        // Empfangen von Ereignissen anderer Fragments registrieren
        getActivity().registerReceiver(bcr, new IntentFilter(UserAdapter.USER_MODIFIED));
        getActivity().registerReceiver(bcr, new IntentFilter(MenuScoreFragment.SCORE_FRAGMENT_OPENED));

        dba = new DBAdapter(getContext());

        userList = new ArrayList<>();

        // Neuen Spieler anlegen
        createNewUserLayout();

        // Liste aller User erzeugen
        setupListView(view);

        return view;
    }

    /**
     * Layout für das Anlegen eines neuen Spielers anlegen
     */
    public void createNewUserLayout() {

        // Layout eines Listenelements holen
        View vKeyValueElement = getActivity().getLayoutInflater().inflate(R.layout.item_options_list, (ViewGroup) view, false);
        vKeyValueElement.setClickable(true);

        // TextViews des Layouts füllen
        CmdgTextView tvKey = (CmdgTextView) vKeyValueElement.findViewById(R.id.tVOptionKey);
        tvKey.setText(R.string.createNewUser);
        CmdgTextView tvValue = (CmdgTextView) vKeyValueElement.findViewById(R.id.tVOptionValue);
        tvValue.setText("+");

        // Namen des aktuell angemeldeten Spielers auslesen
        Options lastUser = dba.getOption(DBAdapter.KEYS_OPTIONS_ID_LAST_USER);
        final String lastUserName = dba.getUserById(Long.parseLong(lastUser.getValue())).getName();

        // Wird auf "Neuen Spieler anlegen" geklickt wird der neue Benutzername abgefragt
        vKeyValueElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askForUserName(lastUserName);
                userAdapter.notifyDataSetChanged();
            }
        });

        // Hole das LinearLayout aus fragment_menu_options.xml
        ViewGroup vgKeyValue = (ViewGroup) view.findViewById(R.id.newUser);

        // Füge erstelltes Layout ein
        vgKeyValue.addView(vKeyValueElement);
    }

    /**
     * Dialog zum Eingeben eines neuen Benutzernamens
     *
     * @param lastUserName Name des aktuell angemeldeten Spielers
     */
    public void askForUserName(final String lastUserName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        // Erstellen eines TextViews für den Title des Alerts
        CmdgTextView tVUserNameAlertHeader = new CmdgTextView(getContext());
        tVUserNameAlertHeader.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        tVUserNameAlertHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, Integer.parseInt(getString(R.string.gameChallengeTitle)));
        tVUserNameAlertHeader.setGravity(Gravity.CENTER);
        tVUserNameAlertHeader.setText(getContext().getString(R.string.nameRequestDialogTitle));
        builder.setCustomTitle(tVUserNameAlertHeader);

        // Anzeigen eines Textfeldes
        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        builder.setView(input);

        // Beim auswählen von "OK" werden die Eingaben validiert und ein neuer Benutzer angelegt
        builder.setPositiveButton(R.string.acceptButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                validateInput(input.getText().toString(), lastUserName);
                updateUserList();
                getContext().sendBroadcast(new Intent(NEW_USER_CREATED));
                closeFragment();
            }
        });
        builder.setNegativeButton(R.string.cancelButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getContext(), R.string.nameAlertAbbruchmeldung, Toast.LENGTH_LONG).show();
            }
        });
        builder.show();
    }

    /**
     * Eingaben im Dialog validieren und ggf. neuen Benutzer anlegen
     *
     * @param name         Name des neuen Spielers
     * @param lastUserName Name des aktuell angemeldeten Spielers
     */
    public void validateInput(String name, String lastUserName) {

        // Prüfen ob ein gültiger Name eingegeben wurde
        if (name.trim().equals("") || name.trim().length() < 2 || name.trim().length() > 10) {
            askForUserName(lastUserName);
            Toast.makeText(getContext(), getString(R.string.invalidUsernameLength, getString(R.string.minLength), getString(R.string.maxLength)), Toast.LENGTH_LONG).show();

            // Neuen User anlegen wenn der eingegebene Username nicht mit dem bisherigen übereinstimmt
        } else if (!name.trim().equals(lastUserName)) {

            // Neuen Benutzer anlegen wenn zum neuen Benutzernamen nicht bereits ein User angelegt ist
            if (dba.getUserByCondition(new ConditionBuilder().whereAnd(DBAdapter.KEY_USER_NAME, name.trim()).build()) == null) {
                dba.insert(new User(name.trim()));
            }

            // Neuen oder bereits vorhandenen User aus Datenbank holen
            ConditionBuilder cb = new ConditionBuilder();
            cb.whereAnd(DBAdapter.KEY_USER_NAME, name.trim());
            ArrayList<User> alu = dba.getUserByCondition(cb.build());

            // Option mit dem alten angemeldeten Benutzer holen und ID des neuen Benutzers einsetzen
            Options o = dba.getOption(DBAdapter.KEYS_OPTIONS_ID_LAST_USER);
            o.setValue(String.valueOf(alu.get(0).getId()));

            // Option aktualisieren
            dba.update(o);

            Toast.makeText(getContext(), getContext().getString(R.string.loggedIn, name.trim()), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Liste aller User erzeugen
     *
     * @param view View
     */
    public void setupListView(View view) {

        ListView lvUser = (ListView) view.findViewById(R.id.userList);

        // Adapter erzeugen
        userAdapter = new UserAdapter(getActivity(), userList);

        // Adapter an ListView hängen
        lvUser.setAdapter(userAdapter);

        updateUserList();
    }

    /**
     * Aktualisieren der Anzeige
     */
    public void updateUserList() {

        // Bisherige Liste leeren oder ggf. neu erzeugen
        if (userList != null) {
            userList.clear();
        } else {
            userList = new ArrayList<>();
        }

        // User aus Datenbank in ArrayList schreiben
        ConditionBuilder cb = new ConditionBuilder();
        cb.orderBy(DBAdapter.KEY_MODIFIEDON, ConditionBuilder.ORDERTYPES.DESC);
        userList.addAll(dba.getUserByCondition(cb.build()));
        userAdapter.notifyDataSetChanged();

    }

    /**
     * Fragment schließen
     */
    public void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    /**
     * Wird immer beim Anzeigen des Fragments aufgerufen
     */
    @Override
    public void updateView() {
        updateUserList();
        ((MainMenuActivity) getActivity()).resetRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainMenuActivity) getActivity()).setRoot();

        // Empfangen von Ereignissen anderer Fragments beenden
        getActivity().unregisterReceiver(bcr);
    }
}