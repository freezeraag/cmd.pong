package de.cmdg.cmdpong.viewController.fragments.mainMenu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.helper.CmdgLog;
import de.cmdg.cmdpong.helper.DBAdapter;
import de.cmdg.cmdpong.layout.layoutHelper.OptionAdapter;
import de.cmdg.cmdpong.layout.layoutHelper.UserAdapter;
import de.cmdg.cmdpong.model.DBObjects.Options;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.viewController.MainMenuActivity;

/**
 * Erstellt von Michael Lorenz
 * am 26.07.2016.
 * Dieses Fragment beinhaltet alle Funktionalitäten des Optionsbildschirms
 */

public class MenuOptionsFragment extends CmdgFragment {

    private ArrayList<Options> optionList;
    private DBAdapter dba;
    private View view;
    private OptionAdapter optionAdapter;

    // Signalisiert geöffneten Fragments, dass sie sich schließen sollen
    public static final String OPTIONS_FRAGMENT_OPENED = "de.cmdg.cmdpong.optionFragmentOpened";

    // Empfangen von Ereignissen aus anderen Fragments
    BroadcastReceiver bcr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {

                // Optionen neu laden, da ein anderer angemeldeter Benutzer angezeigt werden muss
                case UserAdapter.USER_MODIFIED:
                case ChangeUserFragment.NEW_USER_CREATED:
                case OptionAdapter.OPTION_CHANGED:
                    updateView();
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_menu_options, container, false);

        // Empfangen von Ereignissen anderer Fragments registrieren
        getActivity().registerReceiver(bcr, new IntentFilter(UserAdapter.USER_MODIFIED));
        getActivity().registerReceiver(bcr, new IntentFilter(ChangeUserFragment.NEW_USER_CREATED));
        getActivity().registerReceiver(bcr, new IntentFilter(OptionAdapter.OPTION_CHANGED));

        return view;
    }

    /**
     * Instanzvariablen instanziieren und setupListView aufrufen
     */
    @Override
    public void onStart() {
        super.onStart();

        // Datenquelle initialisieren
        optionList = new ArrayList<>();

        // Datenbankadapter initialisieren
        dba = new DBAdapter(getActivity());

        setupListView(view);
        updateView();
    }

    /**
     * Neuen OptionAdapter erzeugen und an vorgesehenen ListView hängen
     *
     * @param view View
     */
    public void setupListView(View view) {

        ListView lvOptions = (ListView) view.findViewById(R.id.optionList);

        // Adapter erzeugen
        optionAdapter = new OptionAdapter(getActivity(), optionList);

        // Adapter an ListView hängen
        lvOptions.setAdapter(optionAdapter);
    }

    /**
     * Aktualisieren der Anzeige
     */
    public void updateOptionList() {

        // Bisherige Liste leeren oder ggf. neu erzeugen
        if (optionList != null) {
            optionList.clear();
        } else {
            optionList = new ArrayList<>();
        }

        // Optionen aus Datenbank in ArrayList schreiben
        optionList.addAll(dba.getAllOptions());
        optionAdapter.notifyDataSetChanged();
        showCredits();

    }

    /**
     * View zum öffnen der Credits anzeigen
     */
    public void showCredits() {
        // Layout eines Listenelements holen
        View vKeyValueElement = getActivity().getLayoutInflater().inflate(R.layout.item_options_list, (ViewGroup) view, false);

        // TextViews des Layouts füllen
        CmdgTextView tvKey = (CmdgTextView) vKeyValueElement.findViewById(R.id.tVOptionKey);
        tvKey.setText(R.string.creditsKeyInOptions);
        tvKey.setClickable(true);

        // Listener implementieren
        // Beim Klick auf "Anzeigen" wird ein neues Fragment geladen, dass die Credits enthält
        tvKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.menuFragmentOptions, new CreditsFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        // Hole das LinearLayout aus fragment_menu_options.xml
        ViewGroup vgKeyValue = (ViewGroup) view.findViewById(R.id.optionShowCredits);

        // Füge erstelltes Layout ein
        vgKeyValue.addView(vKeyValueElement);
    }

    /**
     * Wird immer beim Anzeigen des Fragments aufgerufen
     */
    @Override
    public void updateView() {
        updateOptionList();
        ((MainMenuActivity) getActivity()).setRoot();

        // Benachrichtigen der anderen Fragments sich zu schließen
        getContext().sendBroadcast(new Intent(OPTIONS_FRAGMENT_OPENED));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Empfangen von Ereignissen anderer Fragments beenden
        getActivity().unregisterReceiver(bcr);
    }

}