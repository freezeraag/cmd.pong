package de.cmdg.cmdpong.viewController.fragments.gameScreen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.layout.CmdgButton;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.viewController.GameScreenActivity;

/**
 * Erstellt von Michael Lorenz
 * am 05.08.2016.
 */
public class PauseGameSenderFragment extends CmdgFragment {
    View view;
    GameScreenActivity activity;

    private final String fragmentTag = "PauseSenderFragment";

    /**
     * Pause Fragment.
     * Aufruf, wenn man selbst das Spiel pausiert hat.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return View
     */
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_pause_game_sender, null);
        activity = (GameScreenActivity) getActivity();

        AdView mAdView = (AdView) view.findViewById(R.id.adViewPauseSender);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        CmdgButton exitBtn = (CmdgButton) view.findViewById(R.id.exitBtn);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.exitGame();
            }
        });
        CmdgButton resumeBtn = (CmdgButton) view.findViewById(R.id.resumeBtn);
        resumeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.resumeGame();
            }
        });

        return view;
    }

    @Override
    public void updateView() {

    }
}
