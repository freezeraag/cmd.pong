package de.cmdg.cmdpong.viewController.fragments.mainMenu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.layout.layoutHelper.UserAdapter;
import de.cmdg.cmdpong.viewController.MainMenuActivity;

/**
 * Erstellt von Dominik Grüdl
 * am 02.08.16.
 * Anzeigen der Entwickler in Form eines Bildes sowie deren Namen
 * Der Name kann angeklickt werden und öffnet das Xing-Profil des jeweiligen Entwicklers im Browser
 */
public class CreditsFragment extends CmdgFragment {

    private View view;
    private LinearLayout llKeyValue;

    // Empfangen von Ereignissen aus anderen Fragments
    BroadcastReceiver bcr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case MenuScoreFragment.SCORE_FRAGMENT_OPENED:
                    closeFragment();
                    break;
            }
        }
    };

    /**
     * Credits für die Entwickler im Fragment anzeigen
     *
     * @param inflater           Inflater
     * @param container          Container
     * @param savedInstanceState SavedInstanceState
     * @return View
     */
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // Empfangen von Ereignissen anderer Fragments registrieren
        getActivity().registerReceiver(bcr, new IntentFilter(MenuScoreFragment.SCORE_FRAGMENT_OPENED));

        view = inflater.inflate(R.layout.fragment_credits, container, false);
        ((MainMenuActivity) getActivity()).resetRoot();

        // Hole das LinearLayout aus fragment_credits.xml
        llKeyValue = (LinearLayout) view.findViewById(R.id.creditsList);

        // Für jeden Entwickler einen Eintrag erstellen
        insertCredit(R.drawable.pato, "Torsten Panzer", "https://www.xing.com/profile/Torsten_Panzer2");
        insertCredit(R.drawable.lomi, "Michael Lorenz", "https://www.xing.com/profile/Michael_Lorenz115");
        insertCredit(R.drawable.grdo, "Dominik Grüdl", "https://www.xing.com/profile/Dominik_Gruedl");

        return view;
    }

    /**
     * Für jeden Entwickler neues Listenelement anlegen und dem View des Fragments hinzufügen
     *
     * @param pic  Bild des Entwicklers
     * @param name Name des Entwicklers
     * @param web  Webseite des Entwicklers
     */
    private void insertCredit(int pic, String name, final String web) {

        // Hole das Layout eines Listenelements aus item_options_list.xml
        View vKeyValueElement = getActivity().getLayoutInflater().inflate(R.layout.item_options_list, (ViewGroup) view, false);

        // Fülle die TextViews des Layouts
        CmdgTextView tvKey = (CmdgTextView) vKeyValueElement.findViewById(R.id.tVOptionKey);

        // Bild des Entwicklers am linken Rand des TextViews ablegen
        tvKey.setCompoundDrawablesWithIntrinsicBounds(pic, 0, 0, 0);

        // wrap_content festlegen damit der Name des Entwicklers an das Bild gerückt wird
        tvKey.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        // Namen des Entwicklers setzen und unterstreichen
        CmdgTextView tvValue = (CmdgTextView) vKeyValueElement.findViewById(R.id.tVOptionValue);
        tvValue.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvValue.setText(name);
        tvValue.setClickable(true);

        // Link auf Xing-Profil des Entwicklers ablegen
        tvValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(web));
                startActivity(i);
            }
        });

        // Füge erstelltes Layout ein
        llKeyValue.addView(vKeyValueElement);

    }

    @Override
    public void updateView() {
    }

    public void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainMenuActivity) getActivity()).setRoot();

        // Empfangen von Ereignissen anderer Fragments beenden
        getActivity().unregisterReceiver(bcr);
    }
}
