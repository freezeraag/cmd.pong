package de.cmdg.cmdpong.viewController.fragments.mainMenu;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.helper.DBAdapter;
import de.cmdg.cmdpong.layout.CmdgButton;
import de.cmdg.cmdpong.model.EnemyList;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.viewController.MainMenuActivity;
import de.cmdg.cmdpong.viewController.GameScreenActivity;

/**
 * Erstellt von Michael Lorenz
 * am 26.07.2016.
 *
 * Dieses Fragment beinhaltet alles was für den Verbindungsaufbau benötigt wird.
 * - Eröffnen eines Spiels
 * - Suchen nach Spielern in der Umgebung
 * - Spielaufbau mit gefundenen Spielern
 */

public class MenuGameFragment extends CmdgFragment {

    MainMenuActivity parent;
    ImageView ivProgessSquare;
    DBAdapter dba;
    public final static int START_AS_SERVER = 1;
    public final static int START_AS_CLIENT = 2;
    private EnemyList enemyList;

    private BroadcastReceiver bcr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            switch (action) {
                case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                    ivProgessSquare.setVisibility(View.VISIBLE);
                    break;
                case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                    ivProgessSquare.setVisibility(View.GONE);
                    break;
                case BluetoothDevice.ACTION_FOUND:
                    BluetoothDevice bdt = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (bdt != null) {
                        if(enemyList.addEnemyFromBluetoothDevice(bdt)) {
                            ivProgessSquare.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        parent = (MainMenuActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_menu_game, container, false);
        dba = new DBAdapter(getContext());

        CmdgButton refreshButton = (CmdgButton) view.findViewById(R.id.refreshBtn);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.getBTController().discover();

            }
        });
        CmdgButton openGameButton = (CmdgButton) view.findViewById(R.id.discoverableBtn);
        openGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newGameAsServer = new Intent(getActivity(), GameScreenActivity.class);
                newGameAsServer.putExtra(getString(R.string.startType), START_AS_SERVER);
                startActivity(newGameAsServer);
            }
        });

        // Erzeugen des Ladezeichens
        ivProgessSquare = (ImageView) view.findViewById(R.id.loadingSquareEnemyList);

        // Ladezeichen wie in Optionen gewählt anzeigen
        if(dba.getOption("loadingSymbol").getValue().equals("a")) {
            ivProgessSquare.setBackgroundResource(R.drawable.spinner_a_animation);
        } else {
            ivProgessSquare.setBackgroundResource(R.drawable.spinner_b_animation);
        }

        // Animation des Ladezeichens generieren und starten
        ivProgessSquare.post(new Runnable() {

            @Override
            public void run() {
                AnimationDrawable frameAnimation = (AnimationDrawable) ivProgessSquare.getBackground();
                if (frameAnimation != null) {
                    frameAnimation.start();
                }
            }
        });

        getActivity().registerReceiver(bcr, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED));
        getActivity().registerReceiver(bcr, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
        getActivity().registerReceiver(bcr, new IntentFilter(BluetoothDevice.ACTION_FOUND));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        enemyList = new EnemyList(getActivity());
        updateView();
    }

    @Override
    public void updateView() {
        ivProgessSquare.setVisibility(View.GONE);
        ((MainMenuActivity) getActivity()).setRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(bcr);
    }
}
