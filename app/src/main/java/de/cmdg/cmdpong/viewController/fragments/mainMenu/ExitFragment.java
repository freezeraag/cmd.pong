package de.cmdg.cmdpong.viewController.fragments.mainMenu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.layout.CmdgButton;
import de.cmdg.cmdpong.layout.CmdgFragment;

/**
 * Erstellt von Michael Lorenz
 * am 02.08.16.
 * Anzeigen eines Fragments, welches auf das Beenden der Anwendung hinweißt
 */
public class ExitFragment extends CmdgFragment {

    /**
     * Exit Fragment
     * Auufruf, wenn im Hauptmenu versucht wird, das Spiel zu beenden.
     *
     * @param inflater           Inflater
     * @param container          Container
     * @param savedInstanceState SavedInstanceState
     * @return View
     */
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_exit, container, false);

        AdView mAdView = (AdView) view.findViewById(R.id.adViewExit);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        CmdgButton exitBtn = (CmdgButton) view.findViewById(R.id.exitBtn);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        final ExitFragment self = this;

        CmdgButton cancelBtn = (CmdgButton) view.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction()
                        .remove(self)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commitAllowingStateLoss();
            }
        });
        return view;
    }

    @Override
    public void updateView() {
    }
}
