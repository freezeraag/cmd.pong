package de.cmdg.cmdpong.viewController.fragments.gameScreen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.helper.SoundStore;
import de.cmdg.cmdpong.layout.CmdgButton;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.viewController.GameScreenActivity;

/**
 * Erstellt von Michael Lorenz
 * am 05.08.2016.
 */
public class WinFragment extends CmdgFragment {
    View view;
    GameScreenActivity activity;
    SoundStore soundStore;

    private final String fragmentTag = "WinFragment";

    private CmdgTextView subHeader;
    private int pointsEnemy = 0;
    private int pointsUser = 0;

    InterstitialAd mInterstitialAd;

    /**
     * Win Fragment.
     * Aufruf nachdem ein Spiel gewonnen wurde.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return View
     */
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_win_lose, null);
        activity = (GameScreenActivity) getActivity();

        CmdgTextView header = (CmdgTextView) view.findViewById(R.id.header);
        header.setText(getString(R.string.youWinHeader));

        subHeader = (CmdgTextView) view.findViewById(R.id.subHeader);

        CmdgButton exitBtn = (CmdgButton) view.findViewById(R.id.mainMenuBtn);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });

        soundStore = new SoundStore(getContext());

        mInterstitialAd = new InterstitialAd(activity);
        mInterstitialAd.setAdUnitId(activity.getResources().getString(R.string.afterGameInterstitial));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                activity.getGameController().exitGame(false);
            }
        });
        requestNewInterstitial();

        return view;
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("17B5EB062F86969A53B98703D28E7F52")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
        subHeader.setText(getString(R.string.finalPoints, pointsUser, pointsEnemy));
        soundStore.playWinGame();
    }

    public void setPointsUser(int pointsUser) {
        this.pointsUser = pointsUser;
    }

    public void setPointsEnemy(int pointsEnemy) {
        this.pointsEnemy = pointsEnemy;
    }

    @Override
    public void updateView() {

    }
}
