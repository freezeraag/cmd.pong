package de.cmdg.cmdpong.viewController.fragments.gameScreen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.layout.CmdgButton;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.viewController.GameScreenActivity;

/**
 * Erstellt von Michael Lorenz
 * am 05.08.2016.
 */
public class ReconnectFragment extends CmdgFragment {
    View view;
    GameScreenActivity activity;

    private final String fragmentTag = "ReconnectFragment";

    /**
     * Reconnect Fragment.
     * Aufruf, wenn während des Spiels die Bluetooth-Verbindung zum Gegenspieler abbricht und nun
     * wieder aufgebaut wird.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return View
     */
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_reconnect, null);
        activity = (GameScreenActivity) getActivity();

        CmdgButton exitBtn = (CmdgButton) view.findViewById(R.id.exitBtn);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getGameController().exitGame(false);
            }
        });


        return view;
    }

    @Override
    public void updateView() {

    }
}
