package de.cmdg.cmdpong.viewController.fragments.gameScreen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.viewController.GameScreenActivity;

/**
 * Erstellt von Michael Lorenz
 * am 05.08.2016.
 */
public class CountDownFragment extends CmdgFragment {
    View view;
    GameScreenActivity activity;

    private final String fragmentTag = "CountDownFragment";

    public static final String BROADCAST_UPDATE_COUNTDOWN = "de.cmdg.cdmpong.update_counter";
    public static final String BROADCAST_COUNTDOWN_READY = "de.cmdg.cdmpong.counter_ready";
    public static final String BROADCAST_COUNTDOWN_STARTED = "de.cmdg.cdmpong.counter_started";

    private CmdgTextView countDown;
    private int counter;
    private BroadcastReceiver bcr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(BROADCAST_UPDATE_COUNTDOWN)) {
                if(counter > 0) {
                    countDown.setText(String.valueOf(counter));
                } else {
                    countDown.setText(R.string.startText);
                }
            }
        }
    };




    /**
     * CountDown Fragment.
     * Aufruf nach einer beendeten Pause.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return View
     */
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_countdown, null);
        activity = (GameScreenActivity) getActivity();
        countDown = (CmdgTextView) view.findViewById(R.id.countDown);



        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(bcr, new IntentFilter(BROADCAST_UPDATE_COUNTDOWN));
    }

    @Override
    public void onResume() {
        super.onResume();

        Thread wait = new Thread() {
            public void run() {
                activity.sendBroadcast(new Intent(BROADCAST_COUNTDOWN_STARTED));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                while(counter >= 0) {
                    activity.sendBroadcast(new Intent(BROADCAST_UPDATE_COUNTDOWN));
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    counter--;
                }
                activity.hideCountDownFragment();
                activity.sendBroadcast(new Intent(BROADCAST_COUNTDOWN_READY));
            }
        };

        wait.start();
    }

    public void setStart(int start) {
        this.counter = start;
    }

    @Override
    public void updateView() {

    }

    public void exit() {
        counter = -1;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(bcr);
    }
}
