package de.cmdg.cmdpong.viewController;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import de.cmdg.cmdpong.communication.bluetooth.BTController;
import de.cmdg.cmdpong.communication.bluetooth.BTServiceConnector;
import de.cmdg.cmdpong.communication.bluetooth.NoBluetoothAdapterException;
import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.helper.CmdgLog;
import de.cmdg.cmdpong.helper.ConditionBuilder;
import de.cmdg.cmdpong.helper.DBAdapter;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.model.DBObjects.Options;
import de.cmdg.cmdpong.model.DBObjects.User;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.layout.layoutHelper.PagerAdapterMenu;
import de.cmdg.cmdpong.viewController.fragments.mainMenu.ExitFragment;

/**
 * Erstellt von Michael Lorenz
 * am 26.07.2016.
 * Einstiegsactivity beim Start der App
 */

public class MainMenuActivity extends AppCompatActivity {

    private DBAdapter dba;
    private User currentUser;
    private BTController btController;
    private BTServiceConnector btsc;
    private boolean root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_menu);

        root = true;
        dba = new DBAdapter(this);

        btsc = new BTServiceConnector(this, new BTServiceConnector.BTServiceConnectorCallback() {
            @Override
            public void onConnected(BTController btc) {
                btController = btc;
                initUser();
                initBT();
            }
        });

        // erzwinge Vollbild
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        // pagerAdapter instanziieren für den Aufbau des Menüs
        final PagerAdapterMenu pagerAdapter = new PagerAdapterMenu(getSupportFragmentManager(), this);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        // Durch Menü iterieren um die Schriftart der Bestandteile anpassen
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.cmdgTypeface)));
                }
            }
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ((CmdgFragment) pagerAdapter.getItem(position)).updateView();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    /**
     * Veranlasst das Anschalten des Bluetooth-Adapters
     */
    public void initBT() {
        try {
            btController.initBTAdapter();
        } catch (NoBluetoothAdapterException e) {
            CmdgLog.e(e.getMessage());
            Thread sleepThread = new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                        if (CmdgLog.STAGING == CmdgLog.STAGINGTYPES.PRODUCTION) {
                            finish();
                        }
                    } catch (Exception threadException) {
                        threadException.printStackTrace();
                    }
                }
            };
            Toast.makeText(this, getString(R.string.noBluetoothAdapterFoundMessage),
                    Toast.LENGTH_SHORT).show();
            sleepThread.start();
        }
    }

    public BTController getBTController() {
        return btController;
    }

    public DBAdapter getDBAdapter() {
        return dba;
    }

    /**
     * Beim ersten Programmstart muss ein Benutzername eingegeben werden
     */
    public void initUser() {

        // Prüfen ob bereits ein User vorhanden ist
        ConditionBuilder cb = new ConditionBuilder();
        cb.whereAnd(DBAdapter.KEY_OPTIONS_KEY, DBAdapter.KEYS_OPTIONS_ID_LAST_USER);
        ArrayList<Options> lastUser = dba.getOptionsByCondition(cb.build());

        // Neuen User anlegen wenn keiner in DB gefunden wurde
        if (lastUser == null) {
            askForUserName();
            setupOptions();
        } else {
            currentUser = dba.getUserById(Long.parseLong(lastUser.get(0).getValue()));
        }
    }

    /**
     * Benutzernamen mithilfe eines Alerts erfragen
     */
    public void askForUserName() {

        // AlertDialog erstellen
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Erstellen eines TextViews für den Title des Alerts
        CmdgTextView tVUserNameAlertHeader = new CmdgTextView(this);
        tVUserNameAlertHeader.setPadding(0, getResources().getDimensionPixelSize(R.dimen.padding_medium), 0, getResources().getDimensionPixelSize(R.dimen.padding_medium));
        tVUserNameAlertHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, Integer.parseInt(getString(R.string.gameChallengeTitle)));
        tVUserNameAlertHeader.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        tVUserNameAlertHeader.setGravity(Gravity.CENTER);
        tVUserNameAlertHeader.setText(getString(R.string.nameRequestDialogTitle));
        builder.setCustomTitle(tVUserNameAlertHeader);

        // Alert kann nicht "weggeklickt" werden
        builder.setCancelable(false);

        // EditText für die Eingabe des Namens erstellen und in Alert einfügen
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        builder.setView(input);

        // Beim Klick auf "Ok" wird der eingegebene Benutzername validiert und in DB gespeichert
        builder.setPositiveButton(getString(R.string.acceptButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = input.getText().toString();

                // Benutzernamen auf korrekte Länge prüfen
                if (name.trim().equals("") || name.trim().length() < Integer.parseInt(getString(R.string.minLength)) || name.trim().length() > Integer.parseInt(getString(R.string.maxLength))) {
                    askForUserName();
                    String text = getString(R.string.invalidUsernameLength, getString(R.string.minLength), getString(R.string.maxLength));
                    Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();

                } else {

                    // Benutzername darf nicht gewissen Schlüsselwörtern entsprechen
                    String[] badNames = getString(R.string.badUsernames).split(",");

                    // Ungültige Namen mit Benutzernamen vergleichen
                    boolean badName = false;
                    for (String s : badNames) {
                        if (name.trim().equals(s)) {
                            badName = true;
                        }
                    }

                    // Wurde ein ungültiger Name eingegeben wird ein Toast angezeigt
                    if (badName) {
                        Toast.makeText(getApplicationContext(), getString(R.string.invalidName), Toast.LENGTH_LONG).show();

                        // Benutzer anlegen wenn alles ok ist
                    } else {
                        currentUser = new User(name.trim());
                        dba.insert(currentUser);

                        Options o = new Options(DBAdapter.KEYS_OPTIONS_ID_LAST_USER, String.valueOf(currentUser.getId()));
                        dba.insert(o);
                    }
                }
            }
        });

        builder.show();
    }

    /**
     * Bei erstem Programmstart müssen einmalig die Optionen voreingestellt werden
     */
    public void setupOptions() {
        setupLoadingSymbol();
        setupSound();
    }

    /**
     * Option für Soundeinstellungen anlegen
     * Allerdings nur wenn sie nicht bereits angelegt sind
     */
    public void setupSound() {
        Options o = dba.getOption(DBAdapter.KEYS_OPTIONS_ID_SOUND_ON_OFF);

        // Wird das Element nicht in der DB gefunden wird trotzdem ein Options-Object zurückgegeben,
        // allerdings mit leerem Value
        if(o.getValue().equals("")) {
            o.setValue("true");
            dba.insert(o);
        }
    }

    /**
     * Option für Ladesymbol anlegen
     * Allerdings nur wenn sie nicht bereits angelegt sind
     */
    public void setupLoadingSymbol() {
        Options o = dba.getOption(DBAdapter.KEYS_OPTIONS_ID_LOADING_SYMBOL);

        // Wird das Element nicht in der DB gefunden wird trotzdem ein Options-Object zurückgegeben,
        // allerdings mit leerem Value
        if(o.getValue().equals("")) {
            o.setValue("b");
            dba.insert(o);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (btController != null) {
            btController.finish();
            if (btsc != null) {
                btsc.disconnect();
            }
        }
        if (dba != null) {
            dba.close();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && isRoot()) {
            FragmentManager fm = getSupportFragmentManager();
            ExitFragment exitFragment = new ExitFragment();
            fm.beginTransaction()
                    .replace(R.id.mainScreen, exitFragment, exitFragment.getFragmentTag())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commitAllowingStateLoss();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean isRoot() {
        return root;
    }

    public void resetRoot() {
        root = false;
    }

    public void setRoot() {
        root = true;
    }
}