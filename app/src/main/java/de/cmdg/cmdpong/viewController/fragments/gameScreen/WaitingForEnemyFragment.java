package de.cmdg.cmdpong.viewController.fragments.gameScreen;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.helper.CmdgLog;
import de.cmdg.cmdpong.helper.DBAdapter;
import de.cmdg.cmdpong.layout.CmdgFragment;

/**
 * Erstellt von Michael Lorenz
 * am 26.07.2016.
 */

public class WaitingForEnemyFragment extends CmdgFragment {

    ImageView ivProgessSquare;
    DBAdapter dba;
    private final String fragmentTag = "WaitingForEnemyFragment";

    public WaitingForEnemyFragment() {
    }

    public static WaitingForEnemyFragment newInstance() {
        return new WaitingForEnemyFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Waiting for Enemy Fragment.
     * Aufruf, wenn Spiel als Host gestartet wurde, während des Wartens auf den Gegenspieler.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_waiting_for_enemy, null);
        dba = new DBAdapter(getContext());

        // Erzeugen des Ladezeichens
        ivProgessSquare = (ImageView) view.findViewById(R.id.loadingSquareEnemy);
        // Ladezeichen wie in Optionen gewählt anzeigen
        if(dba.getOption("loadingSymbol").getValue().equals("a")) {
            ivProgessSquare.setBackgroundResource(R.drawable.spinner_a_animation);
        } else {
            ivProgessSquare.setBackgroundResource(R.drawable.spinner_b_animation);
        }

        // Animation des Ladezeichens erzeugen
        ivProgessSquare.post(new Runnable() {

            @Override
            public void run() {
                AnimationDrawable frameAnimation = (AnimationDrawable) ivProgessSquare.getBackground();
                if (frameAnimation != null) {
                    frameAnimation.start();
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void updateView() {

    }
}
