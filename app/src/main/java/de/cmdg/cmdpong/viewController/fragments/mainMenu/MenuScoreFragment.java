package de.cmdg.cmdpong.viewController.fragments.mainMenu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.helper.ConditionBuilder;
import de.cmdg.cmdpong.helper.DBAdapter;
import de.cmdg.cmdpong.layout.layoutHelper.ScoreAdapter;
import de.cmdg.cmdpong.model.ScoreElement;
import de.cmdg.cmdpong.layout.layoutHelper.UserAdapter;
import de.cmdg.cmdpong.model.DBObjects.Game;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.viewController.MainMenuActivity;

/**
 * Erstellt von Michael Lorenz
 * am 26.07.2016.
 * Dieses Fragment beinhaltet alle Views die den Highscore-Bildschirm bilden
 */

public class MenuScoreFragment extends CmdgFragment {

    private ArrayList<ScoreElement> scoreList;
    private ScoreAdapter scoreAdapter;
    private DBAdapter dba;
    private View view;
    private ScoreElement se;

    private CmdgTextView tVNumberContext;
    private CmdgTextView tVNumberWins;
    private CmdgTextView tVNumberDefeats;

    // Signalisiert geöffneten Fragments, dass sie sich schließen sollen
    public static final String SCORE_FRAGMENT_OPENED = "de.cmdg.cmdpong.scoreFragmentOpened";

    // Empfangen von Ereignissen aus anderen Fragments
    BroadcastReceiver bcr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case UserAdapter.USER_MODIFIED:
                    updateView();
                    break;
                case MenuOptionsFragment.OPTIONS_FRAGMENT_OPENED:
                    closeFragment();
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        // Empfangen von Ereignissen anderer Fragments registrieren
        getActivity().registerReceiver(bcr, new IntentFilter(UserAdapter.USER_MODIFIED));

        view = inflater.inflate(R.layout.fragment_menu_score, container, false);

        // Datenquelle initialisieren
        scoreList = new ArrayList<>();

        // Datenbankadapter initialisieren
        dba = new DBAdapter(getActivity());

        setupListViewByEnemies(view);

        // Header für Summe der Spiele und für die Überschriften anzeigen
        setupAllGamesHeader();
        setupHeaderHeadlines();

        // Ist se nicht gesetzt werden alle Ergebnisse angezeigt
        if (se == null) {
            showGamesAgainstAllEnemies();

            // Ist se gesetzt werden die Ergebnisse zu einem Gegner angezeigt
        } else {
            showGamesAgainstOneEnemy(se);
        }

        return view;
    }

    /**
     * ListView initialisieren
     *
     * @param view View
     */
    public void setupListViewByEnemies(View view) {

        // ListView erzeugen
        ListView lvEnemies = (ListView) view.findViewById(R.id.scoreListEnemies);

        // Adapter erzeugen
        scoreAdapter = new ScoreAdapter(getActivity(), scoreList);

        // Adapter an ListView hängen
        lvEnemies.setAdapter(scoreAdapter);
    }

    /**
     * ScoreList gegen alle Gegner anzeigen
     */
    public void showGamesAgainstAllEnemies() {
        if (dba != null) {
            if (scoreList != null) {
                scoreList.clear();
            } else {
                scoreList = new ArrayList<>();
            }

            // Alle Spiele aus Datenbank in ArrayList selektieren
            ConditionBuilder cb = new ConditionBuilder();
            cb.orderBy(DBAdapter.KEY_MODIFIEDON, ConditionBuilder.ORDERTYPES.DESC);
            ArrayList<Game> listAllGames = dba.getGamesByCondition(cb.build());

            //ArrayList<Game> listAllGames = dba.getAllGames();

            // HashMap zum Gruppieren der Spiele nach Enemy
            // Die HashMap enthält die ID des Enemies als Key und die Wins und Defeats als ScoreElement im Value
            HashMap<Integer, ScoreElement> hm = new HashMap<>();

            // Summen der Siege und Niederlagen für die Übersicht über alle Spiele
            int wins = 0;
            int defeats = 0;

            // Aktuell angemeldeten Spieler ermitteln
            String currentUserId = dba.getOption(DBAdapter.KEYS_OPTIONS_ID_LAST_USER).getValue();

            int currentUser = 0;
            try {
                currentUser = dba.getUserById(Long.parseLong(currentUserId)).getId();
            } catch (Exception ignored) {
            }

            if (listAllGames != null) {
                // Durch Spiele iterieren und Wins und Defeats nach Enemy summieren
                for (Game g : listAllGames) {
                    // Nur Spiele des aktuell angemeldeten Spielers anzeigen
                    if (g.getUser().getId() == currentUser) {

                        ScoreElement se;

                        // ScoreElement aus HashMap holen wenn gegen diesen Gegner bereits ein Spiel gespielt wurde
                        if (hm.containsKey(g.getEnemy().getId())) {
                            se = hm.get(g.getEnemy().getId());
                        } else {
                            se = new ScoreElement(g.getEnemy(), 0, 0);
                            hm.put(g.getEnemy().getId(), se);
                        }
                        se.addGame(g);

                        // Anzahl der Wins/Defeats erhöhen, je nachdem wer gewonnen hat
                        if (g.getPointsEnemy() > g.getPointsUser()) {
                            se.incrementDefeats();
                        } else {
                            se.incrementWins();
                        }
                    }
                }

                // Summen der Siege und Niederlagen über alle Spiele
                for (ScoreElement e : hm.values()) {
                    wins += e.getWins();
                    defeats += e.getDefeats();
                }

                // Liste für ScoreAdapter mit ScoreElements bestücken
                for (ScoreElement se : hm.values()) {
                    this.scoreList.add(se);
                }
            }

            // Header der die Summen über alle Spiele anzeigt initialisieren
            updateAllGamesValues(wins, defeats);

            scoreAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Wird aufgerufen wenn die Spiele zu einem einzelnen Enemy angezeigt werden sollen
     *
     * @param scoreElement Daten zu einem einzelnen Spiel
     */
    public void showGamesAgainstOneEnemy(ScoreElement scoreElement) {

        // Wird von der Detailansicht in das Options-Fragment gewechselt soll sich die Detailansicht schließen
        getActivity().registerReceiver(bcr, new IntentFilter(MenuOptionsFragment.OPTIONS_FRAGMENT_OPENED));

        // Spiele aus scoreElement des Enemies holen
        ArrayList<Game> listAllGamesForEnemy = scoreElement.getListGames();

        if (scoreList != null) {
            scoreList.clear();
        } else {
            scoreList = new ArrayList<>();
        }

        // Für jedes Game wird ein neues ScoreElement erzeugt
        for (Game g : listAllGamesForEnemy) {
            ScoreElement se = new ScoreElement(g.getEnemy(), g.getPointsUser(), g.getPointsEnemy());
            se.setDuration(g.getModifiedOn(), g.getCreatedOn());
            scoreList.add(se);
        }

        // Header der die Summen über alle Spiele anzeigt initialisieren
        updateAllGamesValues(scoreElement.getWins(), scoreElement.getDefeats());

        scoreAdapter.notifyDataSetChanged();

        Toast.makeText(getActivity(), getString(R.string.gamesAgainst, scoreElement.getEnemy().getName()), Toast.LENGTH_LONG).show();
    }

    /**
     * Header der die Summen über alle Spiele anzeigt initialisieren
     */
    public void setupAllGamesHeader() {
        // hole das Layout eines Listenelements aus item_list_scorere.xml
        View viewMainHeaderValues = getActivity().getLayoutInflater().inflate(R.layout.item_list_score_header, (ViewGroup) view, false);

        // TextViews des Layouts holen
        tVNumberContext = (CmdgTextView) viewMainHeaderValues.findViewById(R.id.tVNumberPlayed);
        tVNumberWins = (CmdgTextView) viewMainHeaderValues.findViewById(R.id.tVNumberWins);
        tVNumberDefeats = (CmdgTextView) viewMainHeaderValues.findViewById(R.id.tVNumberDefeats);

        // hole das LinearLayout aus fragment_menu_score.xml
        ViewGroup vGMainHeaderValues = (ViewGroup) view.findViewById(R.id.scoreMainHeaderValues);

        // füge erstelltes Layout als Header ein
        vGMainHeaderValues.addView(viewMainHeaderValues);
    }

    /**
     * Werte der Headline füllen
     *
     * @param wins    Anzahl der Siege
     * @param defeats Anzahl der Niederlagen
     */
    public void updateAllGamesValues(int wins, int defeats) {
        tVNumberContext.setText(String.valueOf(wins + defeats));
        tVNumberWins.setText(String.valueOf(wins));
        tVNumberDefeats.setText(String.valueOf(defeats));
    }

    /**
     * Header der die Überschriften anzeigt initialisieren
     */
    public void setupHeaderHeadlines() {
        // hole das Layout eines Listenelements aus item_list_score.xmlml
        View viewMainHeaderTitle = getActivity().getLayoutInflater().inflate(R.layout.item_list_score_sub_header, (ViewGroup) view, false);

        // fülle die TextViews des Layouts
        // Die TextColor muss manuell gesetzt werden (Bug)
        CmdgTextView tVMainTitlePlayed = (CmdgTextView) viewMainHeaderTitle.findViewById(R.id.tVTitleLeft);
        tVMainTitlePlayed.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

        CmdgTextView tVMainTitleWins = (CmdgTextView) viewMainHeaderTitle.findViewById(R.id.tVTitleMiddle);
        tVMainTitleWins.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

        CmdgTextView tVMainTitleDefeats = (CmdgTextView) viewMainHeaderTitle.findViewById(R.id.tVTitleRight);
        tVMainTitleDefeats.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

        // hole das LinearLayout aus fragment_menu_score.xml
        ViewGroup vGMainHeaderTitle = (ViewGroup) view.findViewById(R.id.scoreMainHeaderTitle);

        // füge erstelltes Layout als Header ein
        vGMainHeaderTitle.addView(viewMainHeaderTitle);
    }

    /**
     * @param se ScoreElement des Gegners
     */
    public void setScoreElement(ScoreElement se) {
        this.se = se;
    }

    /**
     * Fragment schließen
     */
    public void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    /**
     * Wird immer beim Anzeigen des Fragments aufgerufen
     */
    @Override
    public void updateView() {
        showGamesAgainstAllEnemies();

        // Benachrichtigen der anderen Fragments sich zu schließen
        getContext().sendBroadcast(new Intent(SCORE_FRAGMENT_OPENED));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainMenuActivity) getActivity()).setRoot();
        // Empfangen von Ereignissen anderer Fragments beenden
        getActivity().unregisterReceiver(bcr);
    }
}