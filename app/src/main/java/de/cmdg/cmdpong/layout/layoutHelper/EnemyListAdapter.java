package de.cmdg.cmdpong.layout.layoutHelper;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.model.DBObjects.Enemy;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.viewController.GameScreenActivity;
import de.cmdg.cmdpong.viewController.fragments.mainMenu.MenuGameFragment;

/**
 * Erstellt von Michael Lorenz
 * am 28.07.2016.
 */
public class EnemyListAdapter extends ArrayAdapter<Enemy> {

    private ArrayList<Enemy> enemyList;
    private Activity activity;

    /**
     * Konstruktor
     * @param activity Activity
     * @param enemyList EnemyList
     */
    public EnemyListAdapter(Activity activity, ArrayList<Enemy> enemyList) {
        super(activity.getApplicationContext(), R.layout.item_list_device, enemyList);

        this.activity =  activity;
        this.enemyList = enemyList;

    }

    /**
     * Erzeugt für jeden Gegner ein neues Element in der Liste
     * @param position Position
     * @param convertView ConvertView
     * @param Parent Parent
     * @return View
     */
    public View getView(int position, View convertView, ViewGroup Parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_list_device, Parent, false);
        }

        final Enemy enemy = enemyList.get(position);

        // Beim Klick auf einen Gegner wird ein Dialog angezeigt
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);

                // Titel des Dialogs definieren
                CmdgTextView tvTitle = new CmdgTextView(activity);
                tvTitle.setPadding(0, activity.getResources().getDimensionPixelSize(R.dimen.padding_medium), 0, activity.getResources().getDimensionPixelSize(R.dimen.padding_medium));
                tvTitle.setText(activity.getString(R.string.enterGame, enemy.getName()));
                tvTitle.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
                tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, Integer.parseInt(activity.getString(R.string.gameChallengeTitle)));
                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                builder.setCustomTitle(tvTitle);

                // "OK"-Button startet ein Spiel
                builder.setPositiveButton(getContext().getResources().getString(R.string.attack), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent newGame = new Intent(activity, GameScreenActivity.class);
                        newGame.putExtra(activity.getString(R.string.startType), MenuGameFragment.START_AS_CLIENT);
                        newGame.putExtra(BluetoothDevice.EXTRA_DEVICE, enemy.getBluetoothDevice());
                        activity.startActivity(newGame);

                        dialog.dismiss();
                    }
                });

                // "Abbrechen"-Button schließt Dialog
                builder.setNegativeButton(getContext().getResources().getString(R.string.escape), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.show();

            }
        });

        // Für jeden Gegner wird der Gerätenamen in der Liste ausgegeben
        if (enemy != null) {
            CmdgTextView tVDevice = (CmdgTextView) v.findViewById(R.id.device);
            tVDevice.setText(enemy.getName());
        }
        return v;
    }
}
