package de.cmdg.cmdpong.layout;


import android.support.v4.app.Fragment;

/**
 * Erstellt von Dominik Grüdl
 * am 27.07.16.
 * Jedes Fragment muss dadurch die Methode udpateView implementieren
 */
public abstract class CmdgFragment extends Fragment {

    public abstract void updateView();

    public String getFragmentTag() {
        return "CmdgFrament";
    }
}
