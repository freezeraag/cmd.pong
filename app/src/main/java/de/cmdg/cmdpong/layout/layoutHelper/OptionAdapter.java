package de.cmdg.cmdpong.layout.layoutHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.helper.DBAdapter;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.model.DBObjects.Options;
import de.cmdg.cmdpong.viewController.MainMenuActivity;
import de.cmdg.cmdpong.viewController.fragments.mainMenu.ChangeUserFragment;

/**
 * Erstellt von Dominik Grüdl
 * am 02.08.16.
 * Adapter, der die Optionen aus der Datenbank in ein vorgegebenes Layout ausgibt
 */
public class OptionAdapter extends ArrayAdapter<Options> {

    private ArrayList<Options> optionList;
    private DBAdapter dba;
    private Activity activity;

    public static final String OPTION_CHANGED = "de.cmdg.cmdpong.optionChanged";

    /**
     * Konstruktor für einen neuen OptionAdapter
     * @param activity Activity
     * @param optionList ArrayList welche die Option-Objekte enthält
     */
    public OptionAdapter(Activity activity, ArrayList<Options> optionList) {

        super(activity.getApplicationContext(), R.layout.item_options_list, optionList);

        this.activity = activity;

        // Datenbankadapter um detailliertere Informationen auslesen zu können
        dba = new DBAdapter(activity);

        this.optionList = optionList;
    }

    /**
     * Erzeugt für jede Option ein neues Element in der Liste
     * @param position Position
     * @param convertView ConvertView
     * @param Parent Parent
     * @return View
     */
    public View getView(int position, final View convertView, ViewGroup Parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_options_list, Parent, false);
        }

        final Options option = optionList.get(position);

        // Ausgabe der Optionen
        if (option != null) {

            final CmdgTextView tvKey = (CmdgTextView) v.findViewById(R.id.tVOptionKey);
            final CmdgTextView tvValue = (CmdgTextView) v.findViewById(R.id.tVOptionValue);
            tvValue.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

            // Die Optionen werden "aufgehübscht" ausgegeben
            switch (option.getKey()) {
                case DBAdapter.KEYS_OPTIONS_ID_LAST_USER:
                    tvKey.setText(R.string.optionLastUserKey);
                    try {
                        tvValue.setText(activity.getString(R.string.changeUser, dba.getUserById(Integer.parseInt(option.getValue())).getName()));
                    } catch (Exception e) {
                        tvValue.setText(option.getValue());
                    }

                    v.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ((MainMenuActivity) activity).getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.menuFragmentOptions, new ChangeUserFragment())
                                    .addToBackStack(null)
                                    .commit();
                        }
                    });
                    break;
                case DBAdapter.KEYS_OPTIONS_ID_LAST_BLUETOOTH_STATE:
                    // Für den Bluetoothstate wird immer "An" ausgegeben, da sich im ausgeschalteten Zustand die App beendet
                    tvKey.setText(R.string.optionLastBluetoothStateKey);
                    tvValue.setText(R.string.optionAn);

                    v.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(activity, R.string.optionLastBluetoothStateChange, Toast.LENGTH_LONG).show();
                        }
                    });
                    break;
                case DBAdapter.KEYS_OPTIONS_ID_SOUND_ON_OFF:
                    tvKey.setText(R.string.sound);
                    tvValue.setText(option.getValue().equals("true") ? "An" : "Aus");

                    v.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (option.getValue().equals("true")) {
                                option.setValue("false");
                            } else {
                                option.setValue("true");
                            }
                            dba.update(option);
                            getContext().sendBroadcast(new Intent(OPTION_CHANGED));
                        }
                    });
                    break;
                case "loadingSymbol":
                    tvKey.setText(R.string.loadingSymbol);
                    tvValue.setText("");
                    if(option.getValue().equals("a")) {
                        tvValue.setCompoundDrawablesWithIntrinsicBounds(R.drawable.spinner_a_logo, 0, 0, 0);
                    } else {
                        tvValue.setCompoundDrawablesWithIntrinsicBounds(R.drawable.spinner_b_logo, 0, 0, 0);
                    }

                    v.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (option.getValue().equals("a")) {
                                option.setValue("b");
                            } else {
                                option.setValue("a");
                            }
                            dba.update(option);
                            getContext().sendBroadcast(new Intent(OPTION_CHANGED));
                        }
                    });
                    break;
                default:
                    tvKey.setText(option.getKey());
                    tvValue.setText(option.getValue());
                    break;
            }
        }

        return v;
    }
}