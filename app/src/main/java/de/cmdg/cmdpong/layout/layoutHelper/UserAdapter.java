package de.cmdg.cmdpong.layout.layoutHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.helper.DBAdapter;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.model.DBObjects.Game;
import de.cmdg.cmdpong.model.DBObjects.Options;
import de.cmdg.cmdpong.model.DBObjects.User;

/**
 * Erstellt von Dominik Grüdl
 * am 02.08.16.
 * Adapter, der die Optionen aus der Datenbank in ein vorgegebenes Layout ausgibt
 */
public class UserAdapter extends ArrayAdapter<User> {

    private ArrayList<User> userList;
    private DBAdapter dba;

    public static final String USER_MODIFIED = "de.cmdg.cmdpong.userModified";

    /**
     * Konstruktor für einen neuen UserAdapter
     *
     * @param activity Activity
     * @param userList ArrayList welche die Option-Objekte enthält
     */
    public UserAdapter(Activity activity, ArrayList<User> userList) {

        super(activity.getApplicationContext(), R.layout.item_options_list, userList);

        // Datenbankadapter um detailliertere Informationen auslesen zu können
        dba = new DBAdapter(activity);

        this.userList = userList;
    }

    /**
     * Erzeugt für jeden Benutzer ein neues Element in der Liste
     *
     * @param position    Position
     * @param convertView ConvertView
     * @param Parent      Parent
     * @return View
     */
    public View getView(int position, final View convertView, ViewGroup Parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_options_list, Parent, false);
        }

        final User user = userList.get(position);

        // Ausgabe der Benutzer
        if (user != null) {

            CmdgTextView tvKey = (CmdgTextView) v.findViewById(R.id.tVOptionKey);
            CmdgTextView tvValue = (CmdgTextView) v.findViewById(R.id.tVOptionValue);

            tvKey.setText(user.getName());

            // Summe über alle Spiele eines Spielers ermitteln und ausgeben
            int cntGames = 0;
            if (dba.getAllGames() != null) {
                for (Game g : dba.getAllGames()) {
                    if (g.getUser().getId() == user.getId()) {
                        cntGames++;
                    }
                }
            }
            tvValue.setText(getContext().getString(R.string.numberOfGames, cntGames));

            // Hintergrundfarbe des aktuell ausgewählten Spielers grün färben
            final Options o = dba.getOption(DBAdapter.KEYS_OPTIONS_ID_LAST_USER);
            if (o.getValue().equals(String.valueOf(user.getId()))) {
                v.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorSuccess));
            } else {
                v.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
            }

            // Akuteller Nutzer kann durch anklicken gewechselt werden
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // Broadcast zur Aktualisierung des Score-Fragments und des Option-Fragments senden
                    getContext().sendBroadcast(new Intent(USER_MODIFIED));

                    // Option mit dem alten angemeldeten Benutzer holen und ID des neuen Benutzers einsetzen
                    o.setValue(String.valueOf(user.getId()));

                    // Option aktualisieren
                    dba.update(o);

                    Toast.makeText(getContext(), getContext().getString(R.string.loggedIn, user.getName()), Toast.LENGTH_LONG).show();
                }
            });

            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    // Zuerst prüfen ob der angemeldete Benutzer noch angemeldet ist
                    if (user.getId() != Integer.parseInt(o.getValue())) {

                        ArrayList<Game> allGames = dba.getAllGames();

                        if(allGames != null) {
                            // Spiele des Benutzers löschen
                            for (Game g : allGames) {
                                if (g.getUser().getId() == user.getId()) {
                                    dba.delete(g);
                                }
                            }
                        }

                        // Spieler löschen
                        dba.delete(user);

                        Toast.makeText(getContext(), getContext().getString(R.string.deleted, user.getName()), Toast.LENGTH_LONG).show();
                        getContext().sendBroadcast(new Intent(USER_MODIFIED));
                    } else {
                        // Spieler darf nicht gelöscht werden wenn er noch angemeldet ist
                        Toast.makeText(getContext(), getContext().getString(R.string.noDeletePossible, user.getName()), Toast.LENGTH_LONG).show();
                    }
                    return true;
                }
            });
        }
        return v;
    }
}