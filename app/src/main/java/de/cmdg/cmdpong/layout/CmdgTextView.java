package de.cmdg.cmdpong.layout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.TextView;

import de.cmdg.cmdpong.R;

/**
 * Erstellt von Dominik Grüdl
 * am 28.07.16.
 * Eigene TextView-Klasse um eigenes Typeface standardmäßig setzen zu können
 */
public class CmdgTextView extends TextView {

    // Festlegen des Typeface für alle TextViews
    private Typeface cmdgTypeface = Typeface.createFromAsset(getContext().getAssets(), getResources().getString(R.string.cmdgTypeface));

    public CmdgTextView(Context context) {
        super(context);
        setCmdgTypeface();
    }

    public CmdgTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCmdgTypeface();
    }

    public CmdgTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCmdgTypeface();
    }

    @Override
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }

    public Typeface getCmdgTypeface() {
        return cmdgTypeface;
    }

    public void setCmdgTypeface(Typeface cmdgTypeface) {
        this.cmdgTypeface = cmdgTypeface;
    }

    /**
     * Setzen des Typeface für jeden TextView
     */
    public void setCmdgTypeface() {
        this.setTypeface(cmdgTypeface);
        this.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryLight));
    }

}
