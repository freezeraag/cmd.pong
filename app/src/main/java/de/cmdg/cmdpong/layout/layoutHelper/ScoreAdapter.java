package de.cmdg.cmdpong.layout.layoutHelper;

import android.app.Activity;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.layout.CmdgTextView;
import de.cmdg.cmdpong.model.ScoreElement;
import de.cmdg.cmdpong.viewController.MainMenuActivity;
import de.cmdg.cmdpong.viewController.fragments.mainMenu.MenuScoreFragment;

/**
 * Erstellt von Dominik Grüdl
 * am 26.07.16.
 * Adapter, der die Ergebnisse aller Spiele in ein vorgegebenes Layout ausgibt
 */
public class ScoreAdapter extends ArrayAdapter<ScoreElement> {

    private ArrayList<ScoreElement> scoreList;
    private Activity activity;

    /**
     * Konstruktor für einen neuen ScoreAdapter
     *
     * @param activity  Activity
     * @param scoreList ArrayList welche die ScoreElements enthält
     */
    public ScoreAdapter(Activity activity, ArrayList<ScoreElement> scoreList) {

        super(activity.getApplicationContext(), R.layout.item_list_score_element, scoreList);

        this.scoreList = scoreList;
        this.activity = activity;
    }

    /**
     * Erzeugt für jedes ScoreElement ein neues Element in der Liste
     *
     * @param position    Position
     * @param convertView ConvertView
     * @param Parent      Parent
     * @return View
     */
    public View getView(int position, final View convertView, ViewGroup Parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_list_score_element, Parent, false);
        }

        final ScoreElement scoreElement = scoreList.get(position);

        // Am scoreElement wird unterschieden ob die Ansicht zu allen Enemies oder nur zu einem Enemy angezeigt werden soll
        if (scoreElement != null) {
            CmdgTextView tVNumberContext = (CmdgTextView) v.findViewById(R.id.tVNumberContext);
            CmdgTextView tVNumberWins = (CmdgTextView) v.findViewById(R.id.tVNumberWins);
            CmdgTextView tVNumberDefeats = (CmdgTextView) v.findViewById(R.id.tVNumberDefeats);

            // Anzeigen der Summen zu allen Enemies
            if (scoreElement.getListGames() != null) {

                // Ausgabe der Ergebnisse nach Enemy gruppiert
                tVNumberContext.setText(scoreElement.getEnemy().getName());
                tVNumberWins.setText(String.valueOf(scoreElement.getWins()));
                tVNumberDefeats.setText(String.valueOf(scoreElement.getDefeats()));

                // Für ganze Zeile OnClickListener implementieren um Detailansicht öffnen zu können
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Fragment das die Einzelansicht anzeigt erstellen
                        MenuScoreFragment mfs = new MenuScoreFragment();
                        ((MainMenuActivity) activity).resetRoot();
                        mfs.setScoreElement(scoreElement);

                        // FragmentManager zum Wechseln des Fragments zur Einzelansicht
                        ((MainMenuActivity) activity).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.scoreFragment, mfs)
                                .addToBackStack(null)
                                .commit();
                    }
                });

                // Ausgabe der Ergebnisse zu einem Enemy
            } else {
                tVNumberContext.setText(activity.getString(R.string.min, scoreElement.getDuration()));
                tVNumberWins.setText(String.valueOf("Spieler: " + scoreElement.getWins()));
                tVNumberDefeats.setText(String.valueOf("Gegner: " + scoreElement.getDefeats()));

                // Hintergrundfarbe je nach Spielergebnis anzeigen
                if (scoreElement.getWins() >= scoreElement.getDefeats()) {
                    v.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorSuccess));
                } else {
                    v.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorDanger));
                }
            }
        }

        return v;
    }

}