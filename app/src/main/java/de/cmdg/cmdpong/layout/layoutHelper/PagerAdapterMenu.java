package de.cmdg.cmdpong.layout.layoutHelper;

import android.content.Context;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.layout.CmdgFragment;
import de.cmdg.cmdpong.viewController.fragments.mainMenu.MenuGameFragment;
import de.cmdg.cmdpong.viewController.fragments.mainMenu.MenuOptionsFragment;
import de.cmdg.cmdpong.viewController.fragments.mainMenu.MenuScoreFragment;

/**
 * Erstellt von Michael Lorenz
 * am 26.07.2016.
 *
 * PagerAdapter für das Hauptmenü.
 */

public class PagerAdapterMenu extends FragmentPagerAdapter {

    private Context context;
    private ArrayList<CmdgFragment> fragmentList;

    public PagerAdapterMenu(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        fragmentList = new ArrayList<>();
        fragmentList.add(new MenuGameFragment());
        fragmentList.add(new MenuScoreFragment());
        fragmentList.add(new MenuOptionsFragment());
    }

    /**
     * Gibt das Fragment anhand eines Index zurück.
     * @param i
     * @return Fragment
     */
    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return fragmentList.get(i);
            case 1:
                return fragmentList.get(i);
            case 2:
                return fragmentList.get(i);
            default:
                return null;
        }
    }

    /**
     * Gibt die Anzahl aller darzustellenden Fragments zurück.
     * @return int
     */
    @Override
    public int getCount() {
        return 3;
    }

    /**
     * Gibt die Titelüberschriften jedes Fragments zurück.
     * @param position
     * @return CharSequence
     */
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getResources().getString(R.string.gameMenuTitle);
            case 1:
                return context.getResources().getString(R.string.scoreMenuTitle);
            case 2:
                return context.getResources().getString(R.string.optionsMenuTitle);
        }
        return null;
    }
}
