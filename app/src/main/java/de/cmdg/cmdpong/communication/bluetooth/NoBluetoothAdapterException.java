package de.cmdg.cmdpong.communication.bluetooth;

/**
 * Exception die geworfen wird, wenn es keinen Bluetoothadpter auf dem Gerät gibt.
 *
 * Erstellt von Torsten Panzer
 * am 30.07.2016.
 */
public class NoBluetoothAdapterException extends Exception {
    public NoBluetoothAdapterException(String message) {
        super(message);
    }
}
