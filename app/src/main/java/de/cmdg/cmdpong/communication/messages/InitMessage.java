package de.cmdg.cmdpong.communication.messages;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Diese Klasse stellt eine Init-Message dar.
 *
 * Erstellt von Torsten Panzer
 * am 02.08.2016.
 */
public class InitMessage extends Message {

    private String guid; // GUID des Senders --> BT-MAC plus lokale User-ID
    private String senderName; // Name des Sender

    // KEYS für die JSON-Objekte
    public static final String KEY_SENDER_NAME = "senderName";
    public static final String KEY_GUID = "guid";

    /**
     * Konstruktor zum Erzeugen einer Nachricht die versendet werden soll.
     *
     * @param senderName
     * @param guid
     */
    public InitMessage(String senderName, String guid) {
        super(messageTypes.INIT, argsToJSONObject(senderName, guid));
        this.guid = guid;
        this.senderName = senderName;
    }

    /**
     * Konstruktor zum Casten von einer Message zu einer Init-Message. Wird beim Empfangen einer
     * Nachricht in der Elternklasse aufgerufen.
     *
     * @param message
     */
    protected InitMessage(Message message) {
        super(message); // Setzt den Typ und den Body
        try { // Setzen der restlichen Werte aus dem Body der Message
            this.senderName = (String) body.get(KEY_SENDER_NAME);
            this.guid = (String) body.get(KEY_GUID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Statische Methode die im "Senden"-Konstruktor benötigt wird,
     * um aus den Properties eine JSON-Objekt zu erzeugen.
     *
     * @param senderName
     * @param guid
     * @return JSONObject
     */
    private static JSONObject argsToJSONObject(String senderName, String guid) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put(KEY_SENDER_NAME, senderName);
            jsonObject.put(KEY_GUID, guid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /**
     *
     * @return String
     */
    public String getGuid() {
        return guid;
    }

    /**
     *
     * @return String
     */
    public String getSenderName() {
        return senderName;
    }
}
