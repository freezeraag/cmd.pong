package de.cmdg.cmdpong.communication.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;

import java.io.IOException;
import java.util.UUID;

import de.cmdg.cmdpong.helper.CmdgLog;
import de.cmdg.cmdpong.controller.GameController;

/**
 * Erstellt von Torsten Panzer
 * am 01.08.2016.
 */
public class BTClient extends Thread {

    private BluetoothSocket bTSocket; // Socket-Endpunkt für die Kommunikation
    private BluetoothDevice bTDevice; // Referenz auf das Bluetoothdevice mit dem Kommuniziert werden soll
    private BTController btc; // Referenz auf den BTCOntroller
    private Activity activity; // Referenz auf ein beliebiges Activity-Objekt (wird nur für sendBroadCast benötigt)
    public static final String CLIENT_CONNECTION_FAILED = "de.cmdg.cmdgpong.client_connection_failed"; // Event das gesendet wird wenn der Verbindungsaufbau fehlschlägt
    public static final String KEY_BLUETOOTHDEVICE = "bluetoothdevice"; // Extra-Key für BroadCast Init-Client um das entsprechnde BTDevice zu versenden

    /**
     *
     * @param bTDevice
     * @param btc
     * @param uuid Die UUID des BTServers, mit dem eine Verbindung aufgebaut werden soll
     * @param activity
     */
    public BTClient(BluetoothDevice bTDevice, BTController btc, UUID uuid, Activity activity) {
        this.bTDevice = bTDevice;
        this.btc = btc;
        this.activity = activity;

        // Erzeugen des Sockets (Verbindungsendpunkt auf der Clientseite)
        BluetoothSocket temp = null; // temp, da btSocket final ist
        try {
            temp = bTDevice.createInsecureRfcommSocketToServiceRecord(uuid);
        } catch (IOException e) {
            CmdgLog.e("CONNECTTHREAD", "Could not create RFCOMM socket:" + e.toString());
            return;
        }
        bTSocket = temp;
    }

    /**
     * Wird durch BTClient.start() aufgerufen
     */
    @Override
    public void run() {

        // Aufbau der Verbindung
        try {
            bTSocket.connect(); // Blockierend bis erfolgreich oder Exception durch z.B. Timeout
        } catch (IOException e) {
            activity.sendBroadcast(new Intent(CLIENT_CONNECTION_FAILED)); // Propagieren, dass Verbindung fehlgeschlagen ist
            cancel(); // schliessen des Sockets
        }

        /**
         * Verbindung wurde aufgebaut
         * Kommunikationsteil Client
         */
        btc.setSocket(bTSocket); // Übergeben des Sockets an den BTController, somit ist er überall Verfügbar
        Intent i = new Intent(GameController.INIT_CLIENT);
        i.putExtra(KEY_BLUETOOTHDEVICE, this.bTDevice);
        activity.sendBroadcast(i); // Senden des Events, dass Verbindung erfolgreich. Der Client kann nun in den State Init wechseln

        return; // Beenden des Threads
    }

    /**
     * Schliesst den Socket und beendet die Verbindung.
     * @return
     */
    public void cancel() {
        try {
            bTSocket.close();
        } catch (Exception e) {
            CmdgLog.e("CONNECTTHREAD", "Could not close connection:" + e.toString());
            return;
        }
        return;
    }
}

