package de.cmdg.cmdpong.communication.bluetooth;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import de.cmdg.cmdpong.helper.CmdgLog;

/**
 * Klasse die zur Bindung and den BTController-Service verwendet wird.
 *
 * Erstellt von Torsten Panzer
 * am 02.08.2016.
 */
public class BTServiceConnector implements ServiceConnection {

    BTController btc;
    Activity activity;
    BTController.BTControllerCallbacks ccb; // Callbacks die vom BTCOntroller in bestimmten Phasen seine LifeCycles ausgeführt werden sollen
    BTServiceConnectorCallback scb; // Callbacks die von dieser Klasse in bestimmten Phasen seine LifeCycles ausgeführt werden sollen

    /**
     *
     * @param activity
     * @param scb
     */
    public BTServiceConnector(Activity activity, BTServiceConnectorCallback scb) {
        this(activity, scb, null);
    }

    /**
     *
     * @param activity
     * @param scb
     * @param ccb
     */
    public BTServiceConnector(Activity activity, BTServiceConnectorCallback scb, BTController.BTControllerCallbacks ccb) {
        this.activity = activity;
        this.ccb = ccb;
        this.scb = scb;

        // Controller bindet die aktuelle Activity an den Service. Diese hat so z.B. selbst Zugriff auf alle Sockets
        Intent btServiceBoundIntent = new Intent(activity, BTController.class);
        activity.bindService(btServiceBoundIntent, this, Context.BIND_AUTO_CREATE); // Context.BIND_AUTO_CREATE: Service wird gestartet, wenn er noch nicht läuft

    }

    /**
     *
     * @param name
     * @param service
     */
    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        // Wir konnten den BTService binden. IBinder wird gecastet und dann holen wir den Service
        BTController.BTServiceBinder binder = (BTController.BTServiceBinder) service;
        btc = binder.getService(activity, ccb); // Übergeben der Activity und der Callbacks an den Service
        btc.initBTController(); // Initieren des Services --> ToDo in BTController verschieben (ggf. Timing Problem wegen activity und ccb).
        if(scb != null) {
            scb.onConnected(btc); // Ausführen des OnConnected-Callbacks, sofern einer übergeben wurde
        }
    }

    /**
     *
     * @param name name
     */
    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    /**
     * Callbacks für BTController die in bestimmten Stadien seines LifeCycles aufgerufen werden.
     */
    public interface BTServiceConnectorCallback {
        void onConnected(BTController btc);
    }

    /**
     *  Diese Methode löst die Bindung zum Service.
     */
    public void disconnect() {
        try {
            activity.unbindService(this);
        } catch(Exception ex) {
            CmdgLog.e(ex.getMessage());
        }
    }
}
