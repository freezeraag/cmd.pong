package de.cmdg.cmdpong.communication.messages;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Diese Klasse stellt eine Interrupt-Message dar.
 *
 * Erstellt von Torsten Panzer
 * am 02.08.2016.
 */
public class InterruptMessage extends Message {

    private INTERRUPTSTATE state;

    public enum INTERRUPTSTATE {
        PAUSE, EXIT, RESUME
    }

    // KEYS für die JSON-Objekte
    public static final String KEY_STATE = "state";

    /**
     * Konstruktor zum Erzeugen einer Nachricht die versendet werden soll.
     *
     * @param state
     */
    public InterruptMessage(INTERRUPTSTATE state) {
        super(messageTypes.INTERRUPT, argsToJSONObject(state));
        this.state = state;
    }

    /**
     * Konstruktor zum Casten von einer Message zu einer Init-Message. Wird beim Empfangen einer
     * Nachricht in der Elternklasse aufgerufen.
     *
     * @param message
     */
    protected InterruptMessage(Message message) {
        super(message); // Setzt den Typ und den Body
        try { // Setzen der restlichen Werte aus dem Body der Message
            this.state = INTERRUPTSTATE.valueOf((String) body.get(KEY_STATE));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Statische Methode die im "Senden"-Konstruktor benötigt wird,
     * um aus den Properties eine JSON-Objekt zu erzeugen.
     *
     * @param state
     * @return JSONObject
     */
    private static JSONObject argsToJSONObject(INTERRUPTSTATE state) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put(KEY_STATE, state);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /**
     *
     * @return INTERRUPTSTATE
     */
    public INTERRUPTSTATE getState() {
        return state;
    }

}
