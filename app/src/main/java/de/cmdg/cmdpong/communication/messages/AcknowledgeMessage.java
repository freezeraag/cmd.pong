package de.cmdg.cmdpong.communication.messages;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Diese Klasse stellt eine Acknowledge-Message dar.
 *
 * Erstellt von Torsten Panzer
 * am 02.08.2016.
 */
public class AcknowledgeMessage extends Message {

    public enum ackknowledgeType { // Mögliche Typen von Acknowledges
        CHANGE, NEW_POINT
    }

    private ackknowledgeType ackType; // Typ dieser Nachricht

    // KEYS für die JSON-Objekte
    public static final String KEY_TYPE = "type";

    /**
     * Konstruktor zum Erzeugen einer Nachricht die versendet werden soll.
     *
     */
    public AcknowledgeMessage(ackknowledgeType type) {
        super(messageTypes.ACKNOWLEDGE, argsToJSONObject(type));
    }

    /**
     * Konstruktor zum Casten von einer Message zu einer Init-Message. Wird beim Empfangen einer
     * Nachricht in der Elternklasse aufgerufen.
     *
     * @param message
     */
    protected AcknowledgeMessage(Message message) {
        super(message); // Setzt den Typ und den Body
        try { // Setzen der restlichen Werte aus dem Body der Message
            this.ackType = ackknowledgeType.valueOf((String) body.get(KEY_TYPE));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Statische Methode die im "Senden"-Konstruktor benötigt wird,
     * um aus den Properties eine JSON-Objekt zu erzeugen.
     *
     * @return JSONObject
     */
    private static JSONObject argsToJSONObject(ackknowledgeType type) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put(KEY_TYPE, type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /**
     *
     * @return ackknowledgeType
     */
    public ackknowledgeType getAckType() {
        return ackType;
    }
}
