package de.cmdg.cmdpong.communication.bluetooth;

import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.UUID;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.communication.MessageService;
import de.cmdg.cmdpong.helper.CmdgLog;
import de.cmdg.cmdpong.helper.DBAdapter;
import de.cmdg.cmdpong.model.DBObjects.Options;

/**
 * Erstellt von Torsten Panzer
 * am 01.08.2016.
 */
public class BTController extends Service {

    // static variablen werden vom binder gesetzt. So hat der Service immer eine
    // Referenz auf die aktuell gebundene Activity und die entsprechenden Callbacks
    private static Activity activity; // Die Activity von der der Controller aufgerufen wurde
    private static BTControllerCallbacks cb; // Callback, die, wenn gesetzt, in verschiedenen Stadien des LifeCylces des BTControllers aufgerufen werden

    private BluetoothAdapter ba; // Referenz auf den Bluetoothadapter des Devices
    private DBAdapter dba; // Referenz auf den Datenbankadapter (ermöglicht Zugriff auf die Datenbank)
    private BTServer serverThread; // Referenz auf den Thread der auf eingehende Verbindungen wartet
    private boolean serverIsRunning = false; // flag das prüft ob der Spieler ein Spiel eröffnet hat
    private BTEventListener btel; // Referenz auf den BroadCastListener
    private String originName; // Name den der Spieler in seinem BluetoothAdapter festgelegt hat
    private BluetoothSocket socket; // Eine Referenz auf die Socketverbindung
    private MessageService messageService; // Instanz auf den MessageService der das Senden und Empfangen von Nachrichten steuert

    private final IBinder btsb = new BTServiceBinder(); // Binder, wird benötigt um eine Activity an den Service zu binden

    // Staitsche Klassenvariablen
    public static final int REQUEST_ENABLE_BLUETOOTH = 1; // Requestcode für Intent zum Aktivieren des Bluetoothadapters
    public static final int REQUEST_ENABLE_DISCOVERABLE = 2; // Request für Intent zum Aktivieren der Sichtbarkeit

    // APP_NAME und APP_UUID werden zum Verbindungsaufbau zweier BTDevices benötigt.
    public static final String APP_NAME = "CMDG.Pong";
    public static final UUID APP_UUID = UUID.fromString("2498e364-8189-46de-8602-d1082e58b908");

    // Wert in s für die Dauer der Sichtbarkeit für andere Geräte
    public static final int DURATION_DISCOVERABLE = 300;

    /**
     * Default-Konstruktor
     */
    public BTController() {
        super();
    }

    /**
     * Initialisiert den Datenbankadapter und registriert die Events.
     * Wird vom Connecter in onServiceConnect aufgerufen.
     */
    public void initBTController() {

        // Erzeugen eines Datenbankadpters
        if (dba == null) { // soll nur einmal passieren
            this.dba = new DBAdapter(activity.getApplicationContext());
        }

        // Für Events Registrieren, auf die die Klassen reagieren soll
        if (btel == null) { // nur einmal registrieren
            btel = new BTEventListener(this);
        }

        // Erzeugen des MessageServices
        if(messageService == null) { // soll nur einmal passieren
            messageService = new MessageService(activity, this);
        }

    }

    /**
     * @param intent intent
     * @param flags flags
     * @param startId startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY; // Nach Beendigung durch System wieder Starten
    }

    /**
     * @param intent intent
     * @return den Binder, wird benötigt um einen Activity an den Service zu binden
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return btsb;
    }

    /**
     * @return true, wenn eine Instanz von BTServer erzeugt ist und einen Socket im Status accept hält, sonst false
     */
    public boolean isServerRunning() {
        return serverIsRunning;
    }

    /**
     *
     * @return Einen Referenz auf den MessageService.
     */
    public MessageService getMessageService() {
        return messageService;
    }

    /**
     * @return Den Namen den der Nutzer bei seinem BTAdapter eingestellt hat. Der Name wird durch CMD.Pong kurzfristig
     * geändert, daher müssen wir uns den originalen Namen merken.
     */
    public String getOriginName() {
        return originName;
    }

    /**
     * @param originName originName
     */
    public void setOriginName(String originName) {
        this.originName = originName;
    }

    /**
     * @return Eine Referenze auf den aktuellen Socket, die aktuelle Verbindung.
     */
    public BluetoothSocket getSocket() {
        return socket;
    }

    /**
     * Methode zum Speichern des Sockets im BTController. Sollte bereits einer vorhanden sein,
     * wird zunächst versucht diesen zu schliessen.
     *
     * @param btSocket
     */
    public void setSocket(BluetoothSocket btSocket) {
        if (this.socket != null) {
            try {
                this.socket.close();
            } catch (Exception e) {
                CmdgLog.e(e.getMessage());
            }
        }
        this.socket = btSocket;
    }

    /**
     * @return Eine Referenz auf den Bluetoothadapter des Devices.
     */
    public BluetoothAdapter getBluetoothAdapter() {
        return this.ba;
    }

    /**
     * @return Eine Referenz auf die Activity, von der der BTController erzeugt wurde.
     */
    public Activity getActivity() {
        return BTController.activity;
    }

    /**
     * Initialisiert den Bluetoothadapter. Prüft ob es einen BTAdapter gibt und ob dieser ein-
     * geschaltet ist. Sollte er aus sein, wird der Adapter eingeschaltet (ein Intent gestartet).
     * Der Status vor Start der App wird gespeichert um diesen nach Beendigung wieder herzustellen.
     *
     * @throws NoBluetoothAdapterException
     */
    public void initBTAdapter() throws NoBluetoothAdapterException {

        // Holen des Bluetoothadapters
        ba = BluetoothAdapter.getDefaultAdapter();
        if (ba == null) { // Wenn es keinen gibt, wird eine Exception geworfen
            throw new NoBluetoothAdapterException(activity.getString(R.string.noBluetoothAdapterFoundMessage));
        }

        // Merken des Status vor Start der App und speichern in der Datenbank
        Options lastBluetoothState = dba.getOption(DBAdapter.KEYS_OPTIONS_ID_LAST_BLUETOOTH_STATE);
        lastBluetoothState.setValue(String.valueOf(ba.isEnabled()));
        dba.update(lastBluetoothState);

        // Prüfen ob BTAdapter eingeschaltet ist
        if (!ba.isEnabled()) { // Nicht eingeschaltet --> Intent zum Einschalten losschicken --> Reaktion in this.onReceive()
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(intent, REQUEST_ENABLE_BLUETOOTH);
        } else { // Eingeschaltet, dann wird der BTServer gestartet. App wartet nun auf eingehende Verbindungsanfragen von Gegnern
            onBTAdapterOn();
        }
    }

    /**
     * Started die Discoverphase des Bluetoothadapters --> Suche nach neuen Gegnern.
     *
     * @return true, wenn Discover gestartet werden konnte, sonst false
     */
    public boolean discover() {

        if (ba != null) {

            this.stopServer(); // Sollte ein Server laufen wird dieser gestoppt. Wir sind entweder Server oder Client.

            try {
                return ba.startDiscovery();
            } catch (Exception e) {
                CmdgLog.e(e.getMessage());
            }

        }

        return false;
    }

    /**
     * Beendet die Discoverphase, wenn diese läuft. Ansonsten kein Effekt.
     */
    public void cancelDiscover() {
        if (ba != null) {
            ba.cancelDiscovery();
        }
    }


    /**
     * Prüft ob das Device sichtbar für andere ist. Wenn nicht, wird ein Intent gesendet,
     * dass die Sichtbarkeit aktivieren soll. Die Änderung der Sichtbarkeit wird dann über einen
     * Broadcast vom BS gemeldet, der dann in der onReceive() des BCR-Objektes ausgewertet wird.
     */
    private void ensureDiscoverableState() {
        if (ba.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) { // Nicht Sichtbar?

            // Nicht Sichtbar! Intent für Aktivierung der Sichtbarkeit für <<DURATION_DISCOVERABLE>> s
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, DURATION_DISCOVERABLE);
            activity.startActivityForResult(discoverableIntent, REQUEST_ENABLE_DISCOVERABLE);

            // Ein Event wird ausgelöst, dass der Request gestartet wurde. Wer eventuell auf eine Statusänderung
            // wartet kann sich nun in den Status warten setzen (beugt z.B. einem Timeout-Fehler der Sichtbarkeit in der GameScreenActivity vor)
            activity.sendBroadcast(new Intent(BTEventListener.BROADCAST_ACTIVATE_DISCOVERABLE_STARTED));

        }
    }

    /**
     * Erzeugt einen BTServer-Thread, der auf eingehende Verbindungen von Gegnern wartet.
     */
    public void startServer() {
        if (ba != null) {
            if (ba.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
                ensureDiscoverableState(); // Es muss sicher gestellt sein, dass wir Sichtbar für andere sind bevor wir auf eingehende Verbindungen warten können
            } else {
                this.stopServer(); // Wenn der ServerThread läuft, wird er beendet und dann ein neuer gestartet.
                this.ba.cancelDiscovery(); // Sollte discoverphase laufen, wird diese gestoppt

                // ändere den Bluetooth-Namen damit wir herausgefiltert werden können
                setOriginName(ba.getName().replace(activity.getString(R.string.bluetoothDeviceIdent), "")); // merken des alten Namen, vergessene Token werden entfernt
                ba.setName(getOriginName() + activity.getString(R.string.bluetoothDeviceIdent));

                this.serverIsRunning = true; // Server wird auf gestartet gesetzt
                this.serverThread = new BTServer(this, APP_UUID, activity); // Server wird erzeugt (und erzeugt Socket)
                this.serverThread.start(); // Server gestartet und setzt den Socket in accept
            }
        }
    }

    /**
     * Stoppt den laufen Server-Thread (Spiel eröffen-Thread).
     */
    public void stopServer() {
        if (this.serverThread != null) {
            this.serverThread.cancel();
        }
        this.serverIsRunning = false; // Server läuft nun nicht mehr
    }

    /**
     * Erzeugt einen neue BTClient-Thread, der veruscht sich mit dem übergebenen BluetoothDevice zu verbinden.
     *
     * @param device
     */
    public void startClientConnection(BluetoothDevice device) {
        if (ba != null) {
            this.stopServer(); // Wenn der ServerThread läuft, wird er beendet. Da nur entweder Client oder Server möglich ist.

            // Erzeugen und starten des Client Threads und setzen des erzeugten Sockets in connect
            new BTClient(device, this, APP_UUID, activity).start();
        }
    }


    /**
     * Dient zum Aufräumen und Beenden der BT-Verbindung.
     * Setzt den Status des BTAdapters auf den Status vor Start der App zurück.
     */
    public void finish() {

        if (ba != null) {

            // Holen des Status des BTAdapters vor Start der App
            Options lastBluetoothState = dba.getOption(DBAdapter.KEYS_OPTIONS_ID_LAST_BLUETOOTH_STATE);
            if (!Boolean.parseBoolean(lastBluetoothState.getValue())) {
                ba.disable(); // Ausschalten des BT-Adapters, wenn er vorher aus war
            }

            if (btel != null) { // Austragen für alle registrierten Events. Wir wollen jetzt nicht mehr gestört werden.
                btel.unregisterReceiver();
            }

            closeConnection(); // Schliessen des Sockets, falls noch nicht passiert

            // Wir (Service) beenden uns selbst, bye bye
            this.stopSelf();

        }

    }


    /**
     * Callbacks für BTController die in bestimmten Stadien seines LifeCycles aufgerufen.
     */
    public interface BTControllerCallbacks {
        void onBTAdapterOn(); // Wird aufgerufen sobald der BTAdapter eingeschaltet ist
    }

    /**
     * Callback die aufgerufen wird, wenn der Bluetoothadapter an ist. Ruft registrierte Callbacks auf.
     */
    public void onBTAdapterOn() {

        if (cb != null) {
            cb.onBTAdapterOn();
        }
    }

    /**
     * Beendet eine offene Verbindung.
     */
    public void closeConnection() {
        if(socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket = null;
        }
        serverThread = null; // Server gelöscht
        serverIsRunning = false; // Server läuft nun nicht mehr
    }

    /**
     * Klasse die benötigt wird um einen Activity an diesen Service zu binden
     */
    public class BTServiceBinder extends Binder {

        BTController getService(Activity activity, BTControllerCallbacks cb) {
            BTController.cb = cb; // Speichern aller Callbacks
            BTController.activity = activity; // Welche Activity will sich binden
            return BTController.this; // Zurückgeben der Service-Instanz
        }
    }

    /**
     * Setzt den angezeigten Bluetoothnamen wieder auf den ursprünglichen Namen zurück.
     * Wurde geändert, damit der Gegner nur BTDevices mit der entsprechenden App angezeigt
     * bekommt.
     */
    public void resetBluetoothName() {
        if (originName != null) {
            ba.setName(originName);
        }
    }

}
