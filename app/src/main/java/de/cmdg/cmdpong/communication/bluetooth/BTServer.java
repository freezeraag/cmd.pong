package de.cmdg.cmdpong.communication.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;

import java.io.IOException;
import java.util.UUID;

import de.cmdg.cmdpong.helper.CmdgLog;
import de.cmdg.cmdpong.controller.GameController;

/**
 * Erstellt von Torsten Panzer
 * am 01.08.2016.
 */
public class BTServer extends Thread {

    private final BluetoothServerSocket serverSocket; // Socket-Endpunkt für die Kommunikation
    private BTController btc; // Referenz auf den BTCOntroller
    private Activity activity; // Referenz auf ein beliebiges Activity-Objekt (wird nur für sendBroadCast benötigt)
    public static final String SERVER_CONNECTION_FAILED = "de.cmdg.cmdgpong.server_connection_failed"; // Event das gesendet wird wenn der Verbindungsaufbau fehlschlägt

    /**
     *
     * @param btc btc
     * @param uuid Die UUID auf die der BTServer auf eingehende Anfragen lauscht.
     * @param activity activity
     */
    public BTServer(BTController btc, UUID uuid, Activity activity) {

        this.btc = btc;
        this.btc.cancelDiscover(); // Ein möglicher laufender Discoveryprozess wird beendet. Wir sind entweder Client oder Server.

        this.activity = activity;

        // Erzeugen des Sockets
        BluetoothServerSocket tmp = null; // tmo, weil serverSocket final ist
        try {
            // Erzeuge einen Socket der auf eingehende RFCOMM Signale wartet. Als Identifikation wird uuid.toString() (als name) und uuid verwendet.
            tmp = btc.getBluetoothAdapter().listenUsingInsecureRfcommWithServiceRecord(uuid.toString(), uuid);
        } catch (IOException e) {
            CmdgLog.e(e.getMessage());
        }
        serverSocket = tmp;

    }

    /**
     * Startet den eigentlichen Listen-Prozess.
     */
    public void run() {

        BluetoothSocket socket = null;

        // Warten bis eine Exception auftritt oder ein Client sich verbinden will
        while (true) {
            try {
                // Nur solange wir sichtbar sind wird auf eingehende Verbindungen gelauscht. Spart Strom, rettet die Welt ;)
                socket = serverSocket.accept(BTController.DURATION_DISCOVERABLE * 1000);
            } catch (IOException e) {
                CmdgLog.e(e.getMessage());
                activity.sendBroadcast(new Intent(SERVER_CONNECTION_FAILED)); // Propagieren, dass Verbindung fehlgeschlagen z.B. durch Timeout
                this.cancel(); // Schließen des Sockets
                break;
            }

            /**
             * Verbindung wurde aufgebaut
             * Kommunikationsteil Server
             */
            if (socket != null) {
                btc.setSocket(socket); // Übergeben des Sockets an den BTController, so ist er überall Verfügbar
                activity.sendBroadcast(new Intent(GameController.INIT_SERVER)); // Propagieren der erfolgreichen Verbindung und das der Server in den State Init wechseln kann
                return;
            }
        }
    }

    /**
     * Beendet die Verbindung und damit auch den Thread.
     */
    public void cancel() {

        try {
            serverSocket.close();
        } catch (Exception e) {
            CmdgLog.e(e.getMessage());
        }

    }

}
