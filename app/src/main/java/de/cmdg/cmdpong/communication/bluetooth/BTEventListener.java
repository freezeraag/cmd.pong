package de.cmdg.cmdpong.communication.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import de.cmdg.cmdpong.helper.CmdgLog;

/**
 * Klasse die auf BT-bezogene Events lauscht und reagiert.
 *
 * Erstellt von Torsten Panzer
 * am 02.08.2016.
 */
public class BTEventListener extends BroadcastReceiver {

    BTController btc;

    // Event das gefeuert wird wenn Discovering beendet werden soll (z.B. nach Timeout). Das Event wird in einem separten Thread ausgelöst.
    public static final String BROADCAST_CANCEL_DISCOVERING = "de.cmdg.cmdgpong.cancel_discovering";

    // Event das gefeuert wenn der Benutzer bei Sichtbarkeit nein gewählt hat
    public static final String BROADCAST_NOT_DISCOVERABLE = "de.cmdg.cmdgpong.not_discoverable";

    // Event das gefeuert wird wenn der Prozess der den Nutzer fragt ob Sichtbarkeit aktiviert werden darf, gestartet wurde
    public static final String BROADCAST_ACTIVATE_DISCOVERABLE_STARTED = "de.cmdg.cmdpong.activate_discoverable_started";

    // Event das gefeuert wird wenn der Prozess der den Nutzer fragt ob Sichtbarkeit aktiviert werden darf, beendet wurde
    public static final String BROADCAST_ACTIVATE_DISCOVERABLE_FINISHED = "de.cmdg.cmdpong.activate_discoverable_finished";

    /**
     *
     * @param btc
     */
    public BTEventListener(BTController btc) {
        this.btc = btc;

        // Event, das vom Bs ausgelöst wird, wenn der Status des Bluetoothadapters
        // geändert wurde: ON oder OFF
        btc.getActivity().registerReceiver(this, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

        // Custom Event, das von einem Kind-Thread ausgelöst wird, wenn die DiscoveringPhase beendet werden soll.
        btc.getActivity().registerReceiver(this, new IntentFilter(BTEventListener.BROADCAST_CANCEL_DISCOVERING));

        // Event, das vom BS ausgelöst wird, wenn discoverable geändert wurde
        btc.getActivity().registerReceiver(this, new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));

        // Event, das vom Bs ausgelöst wird wenn sich der Pairing-State ändert
        btc.getActivity().registerReceiver(this, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));

    }

    /**
     * Methode, die aufgerufen wird sobald ein neues Event gefeuert wird, für
     * das sich die Klasse registriert hat.
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction(); // Holen des Eventtypes

        switch (action) {
            case BluetoothAdapter.ACTION_STATE_CHANGED: // Status des BTAdapters geändert

                int extraState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (extraState) {
                    case BluetoothAdapter.STATE_ON: // BTAdapter wurde eingeschaltet
                        btc.onBTAdapterOn(); // Aufrufen der Callback
                }

                break;

            case BTEventListener.BROADCAST_CANCEL_DISCOVERING: // Discoveringphase soll beendet werden
                btc.cancelDiscover();
                break;

            case BluetoothAdapter.ACTION_SCAN_MODE_CHANGED: // Discoverable changed
                if (btc.getBluetoothAdapter().getScanMode() == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) { // Wir sind Sichtbar
                    btc.getActivity().sendBroadcast(new Intent(BROADCAST_ACTIVATE_DISCOVERABLE_FINISHED)); // Event auslösen, dass Request jetzt beendet ist (beugt z.B. einem Timeout-Fehler der Sichtbarkeit in der GameScreenActivity vor)
                    btc.startServer(); // Starten der BTServerInstanz
                } else {
                    btc.getActivity().sendBroadcast(new Intent(BROADCAST_NOT_DISCOVERABLE)); // Event auslösen, dass der Nutzer vermutlich Nein gewählt hat oder der Timeout erreicht ist
                }
                break;

            case BluetoothDevice.ACTION_BOND_STATE_CHANGED: // Pairing Status zwischen zwei Devices hat sich geändert
                if (!btc.isServerRunning()) { // Wir reagieren nur als Client darauf
                    BluetoothDevice bd = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    int bond_state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                    if (bond_state == BluetoothDevice.BOND_BONDED) { // Wenn der Status Paired ist, kann der CLient versuchen sich zu verbinden
                        btc.startClientConnection(bd); // Starten einer Client-Verbindung
                    }

                }
                break;
        }

    }

    /**
     * Alle Events wieder austragen.
     */
    public void unregisterReceiver() {

        // Für alle Events wieder austragen. Wir wollen jetzt in Ruhe gelassen werden ;)
        try {
            btc.getActivity().unregisterReceiver(this);
        } catch (Exception ex) {
            CmdgLog.e(ex.getMessage());
        }

    }

}
