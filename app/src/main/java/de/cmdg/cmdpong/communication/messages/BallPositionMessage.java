package de.cmdg.cmdpong.communication.messages;

import org.json.JSONException;
import org.json.JSONObject;

import de.cmdg.cmdpong.model.GameObjects.Ball;

/**
 * Erstellt von Michael Lorenz
 * am 04.08.2016.
 */
public class BallPositionMessage extends Message {

    private int x;
    private int y;
    private int movX;
    private int movY;
    private long index;

    // KEYS für die JSON-Objekte
    public static final String BALL_X = "x";
    public static final String BALL_Y = "y";
    public static final String BALL_MOV_X = "mov_x";
    public static final String BALL_MOV_Y = "mov_y";
    public static final String MESSAGE_INDEX = "index";

    /**
     * Konstruktor zum Erzeugen einer Nachricht die versendet werden soll.
     *
     * @param ball
     * @param index
     */
    public BallPositionMessage(Ball ball, long index) {
        super(messageTypes.BALLPOSITION, argsToJSONObject(ball, index));
        this.x = ball.getX();
        this.y = ball.getY();
        this.movX = ball.getMovementX();
        this.movY = ball.getMovementY();
        this.index = index;
    }

    /**
     * Konstruktor zum Casten von einer Message zu einer Init-Message. Wird beim Empfangen einer
     * Nachricht in der Elternklasse aufgerufen.
     *
     * @param message
     */
    protected BallPositionMessage(Message message) {
        super(message); // Setzt den Typ und den Body
        try { // Setzen der restlichen Werte aus dem Body der Message
            this.x = (int) body.get(BALL_X);
            this.y = (int) body.get(BALL_Y);
            this.movX = (int) body.get(BALL_MOV_X);
            this.movY = (int) body.get(BALL_MOV_Y);
            this.index = (int) body.get(MESSAGE_INDEX);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Statische Methode die im "Senden"-Konstruktor benötigt wird,
     * um aus den Properties eine JSON-Objekt zu erzeugen.
     *
     * @param ball
     * @param index
     * @return JSONObject
     */
    private static JSONObject argsToJSONObject(Ball ball, long index) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put(BALL_X, ball.getX());
            jsonObject.put(BALL_Y, ball.getY());
            jsonObject.put(BALL_MOV_X, ball.getMovementX());
            jsonObject.put(BALL_MOV_Y, ball.getMovementY());
            jsonObject.put(MESSAGE_INDEX, index);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getMovementX() {
        return movX;
    }

    public int getMovementY() {
        return movY;
    }

    public long getIndex() {
        return index;
    }
}
