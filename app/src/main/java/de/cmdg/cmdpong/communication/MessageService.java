package de.cmdg.cmdpong.communication;

import android.app.Activity;
import android.content.Intent;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import de.cmdg.cmdpong.communication.bluetooth.BTController;
import de.cmdg.cmdpong.communication.messages.Message;
import de.cmdg.cmdpong.helper.CmdgLog;

/**
 * Klasse die das Senden und Empfangen von Messages handelt.
 *
 * Erstellt von Torsten Panzer
 * am 02.08.2016.
 */
public class MessageService {

    private Activity activity; // Referenz auf eine beliebige Activity für Broadcasts
    private BTController btc; // Referenu auf den BTController um Zugriff auf die Sockets zu erhalten
    private ArrayList<Message> messageQueue; // Schlange in der alle empfangenen Nachrichten gepuffert werden, bis jemand Sie abholt (verbrauchendes Lesen)

    private static final int MAX_MESSAGE_BYTES = 1024; // Maximale länge der Nachricht
    // --> ToDo längere Nachrichten in mehrere Nachrichten zerlegen {splittedMessage: 1, content: content1},  {splittedMessage: 2, content: content2} ...
    // --> ToDo Beim Empfangen: content1 + content2 = message

    // Event das gesendet wird, wenn eine neue Nachricht empfangen wurde
    public static final String NEW_MESSAGE_RECEIVED = "de.cmdg.cmdpong.new_message_received";

    // Event das gesendet wird, wenn die Verbindung unterbrochen wurden
    public static final String CONNECTION_LOST = "de.cmdg.cmdpong.connection_lost";

    // Token die zur Überprüfung einer Fehlerfreien Übertragung am Anfang bzw. am Ende der Nachricht eingefügt werden.
    public static final String MESSAGE_BEGIN_TOKEN = "---START---";
    public static final String MESSAGE_END_TOKEN = "---END---";

    private InputStream inputStream; // Referenz auf den aktuellen InputStream, wird zum Schliessen von außen benötigt
    public readThread reader; // Referenz auf den ReaderThread, der dauerhaft läuft und ggf. von außen beendet werden soll

    /**
     * @param activity activity
     * @param btc btc
     */
    public MessageService(Activity activity, BTController btc) {

        this.activity = activity;
        this.btc = btc;
        this.messageQueue = new ArrayList<>(); // Initialisieren des Puffers

    }

    /**
     * Holen der nächsten Nahricht im Puffer.
     *
     * @return Message
     */
    public Message getNextMessage() {
        if (!this.messageQueue.isEmpty()) { // Ist noch eine Nachricht vorhanden?
            return this.messageQueue.remove(0); // Erste Nachricht holen und entfernen aus dem Puffer
        }
        return null;
    }

    /**
     * @param message
     * @return true, wenn die Nachricht losgeschickt wurde. False wenn nicht.
     */
    public boolean sendMessage(Message message) {
        String mStr = MESSAGE_BEGIN_TOKEN + message.getJSONString() + MESSAGE_END_TOKEN; // Hinzufügen der Token
        byte[] m = mStr.getBytes(); // Umwandeln in Byte-Array
        if (btc.getSocket() != null && m.length <= MAX_MESSAGE_BYTES) { // Prüfen der Länge
            new writeThread(m).start(); // Starten eine Threads zum Senden der Nachricht.
            return true;
        }
        return false;
    }

    /**
     * Startet einen Thread der auf eingehende Nachrichten wartet
     * und meldet wenn eine Neue da ist.
     */
    public void listenForMessages() {
        if (btc.getSocket() != null) {
            reader = new readThread();
            reader.start();
        }
    }

    /**
     * Ermöglicht das Beenden des ReaderThread von außen. Falls er zum Beispiel in der
     * blockierenden Read-Funktion hängt und nicht merkt, dass der Socket abgerissen ist.
     *
     */
    public void stopListenForMessages() {
        if (reader != null) {
            reader.cancel();
            reader = null;
        }
    }

    /**
     * Ermöglicht, dass schliessen des InputStreams von außen.
     */
    public void closeInputStream() {
        if(inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Thread der auf eingehende Nachrichten wartet
     * und meldet wenn eine Neue da ist. Kann nur vom
     * MessageService erzeugt werden.
     */
    private class readThread extends Thread {

        private final InputStream in; // Der Stream von dem gelesen werden soll
        private boolean keepAlive = true; // Flag das Angbit ob weiter gelesen werden soll

        /**
         *
         */
        public readThread() {
            InputStream tmp = null; // tmp, da in final ist
            try {
                tmp = btc.getSocket().getInputStream(); // Holen des InputStreams vom Socket aus dem gelesen werden soll.
            } catch (Exception e) {
                CmdgLog.e(e.getMessage());
                activity.sendBroadcast(new Intent(CONNECTION_LOST)); // Propagieren, dass die Verbindung verloren gegangen ist.
                closeSocket();
            }
            in = inputStream = tmp;
        }

        /**
         *
         */
        @Override
        public void run() {

            byte[] buffer = new byte[MAX_MESSAGE_BYTES]; // Puffer in den das gelesene geschrieben wird
            int bytes; // Anzahl der Bytes die gelesen wurden

            while (keepAlive) { // "Dauerdienst" --> Nach dem lesen einer Nachrcht wird auf die nächste gewartet. Bis von außen gestoppt.

                try {
                    if (in.available() > 0) { // Wenn es was zu lesen gibt
                        bytes = in.read(buffer); // Lesen der Daten vom Stream
                        String message = new String(buffer, 0, bytes); // Umwandeln in einen String --> JSON
                        if (message.indexOf(MESSAGE_BEGIN_TOKEN) == 0 && message.indexOf(MESSAGE_END_TOKEN) > MESSAGE_BEGIN_TOKEN.length()) { // sind unsere Token enthalten?
                            message = message.substring(MESSAGE_BEGIN_TOKEN.length(), message.indexOf(MESSAGE_END_TOKEN)); // Rausfiltern der eigentlichen Nachricht --> Abschneiden der Token
                            messageQueue.add(new Message(message)); // Hinzufügen einer Neuen Message zum Puffer
                            activity.sendBroadcast(new Intent(NEW_MESSAGE_RECEIVED)); // Propgieren das Neue Nachricht eingetroffen ist
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CmdgLog.d(e.getMessage());
                    if (keepAlive) {
                        activity.sendBroadcast(new Intent(CONNECTION_LOST));  // Propagieren, dass die Verbindung verloren gegangen ist.
                    }
                    closeSocket();
                    break;
                }

            }

            return;
        }

        /**
         * Beendet den Thread nach dem Lesen der nächsten Nachricht.
         */
        public void cancel() {
            this.keepAlive = false;
        }

    }

    /**
     * Klasse die zum Senden einer Nachricht verwendet wird. Kann nur vom MessageService erzeugt werden.
     */
    private class writeThread extends Thread {

        private final OutputStream out; // Stream in den geschrieben werden soll
        private final byte[] bytesToWrite; // Daten die geschrieben werden sollen

        public writeThread(byte[] bytesToWrite) {

            this.bytesToWrite = bytesToWrite;

            // Holen des Streams in den geschrieben werden soll aus dem Socket
            OutputStream tmp = null; // tmp, das out final ist
            try {
                tmp = btc.getSocket().getOutputStream();
            } catch (Exception e) {
                CmdgLog.e(e.getMessage());
                activity.sendBroadcast(new Intent(CONNECTION_LOST));  // Propagieren, dass die Verbindung verloren gegangen ist.
                closeSocket();
            }
            out = tmp;
        }

        /**
         *
         */
        @Override
        public void run() {

            try { // Schreiben der byte-Nachricht in den Stream, danach beendet sich der Thread
                out.write(bytesToWrite);
            } catch (Exception e) {
                e.printStackTrace();
                activity.sendBroadcast(new Intent(CONNECTION_LOST));  // Propagieren, dass die Verbindung verloren gegangen ist.
                closeSocket();
            }

            return;
        }


    }

    /**
     * Schliesst die Socketverbindung.
     */
    private void closeSocket() {
        if(btc != null && btc.getSocket() != null && btc.getSocket().isConnected()) {
            try {
                btc.getSocket().close();
            } catch (Exception ex) {
                CmdgLog.e(ex.getMessage());
            }
        }
    }

}
