package de.cmdg.cmdpong.communication.messages;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Diese Klasse stellt eine Message dar.
 *
 * Erstellt von Michael Lorenz
 * am 02.08.2016.
 */
public class Message {

    messageTypes type; // Typ der Message
    JSONObject body; // Body (eigentlicher Inhalt) der Message

    public enum messageTypes {INIT, BALLPOSITION, INTERRUPT, ACKNOWLEDGE} // Alle Nachrichtentypen

    // KEYS für die JSON-Objekte
    public static final String KEY_TYPE = "type";
    public static final String KEY_BODY = "body";

    /**
     * Konstrukor der beim Empfangen einer Message zum
     * Casten in einen speziellen Messagetyp verwendet wird. Siehe convertMessage().
     *
     * @param message
     */
    protected Message(Message message) {
        this.type = message.type;
        this.body = message.body;
    }

    /**
     * Konstrukor der beim Empfangen einer Message zum Parsen des
     * empfangenen JSON-Strings verwendet wird.
     *
     * @param jsonString
     */
    public Message(String jsonString) {
        try { // Holen des Types und des Bodys
            JSONObject jsonObject = new JSONObject(jsonString);
            this.type = messageTypes.valueOf((String) jsonObject.get(KEY_TYPE));
            this.body = new JSONObject((String) jsonObject.get(KEY_BODY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Konstruktor der beim Senden einer Nachricht verwendet wird. Kann nicht direkt aufgerufen werden,
     * sondern nur implizit über einen abgeleiteten speziellen Message-Typ.
     *
     * @param type
     * @param body
     */
    protected Message(messageTypes type, JSONObject body) {
        this.type = type;
        this.body = body;
    }

    /**
     * @return messageTypes
     */
    public messageTypes getType() {
        return type;
    }

    /**
     * Wird nur intern verwendet. Nicht nach außen sichtbar nur in Kind-Klassen.
     *
     * @return JSONObject
     */
    protected JSONObject getBody() {
        return body;
    }

    /**
     * Gibt den Type und den Body als JSON-String zurück. Wird zum Versenden benötigt.
     *
     * @return String
     */
    public String getJSONString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_TYPE, type);
            jsonObject.put(KEY_BODY, body.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    /**
     * Konvertiert die Message in einen speziellen Message-Type.
     *
     * @return Message
     */
    public Message convertMessage() {
        switch (type) {
            case INIT:
                return new InitMessage(this);
            case BALLPOSITION:
                return new BallPositionMessage(this);
            case INTERRUPT:
                return new InterruptMessage(this);
            case ACKNOWLEDGE:
                return new AcknowledgeMessage(this);
        }
        return null;
    }

}
