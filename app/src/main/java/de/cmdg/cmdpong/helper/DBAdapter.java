package de.cmdg.cmdpong.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import de.cmdg.cmdpong.model.DBObjects.DBObject;
import de.cmdg.cmdpong.model.DBObjects.Enemy;
import de.cmdg.cmdpong.model.DBObjects.Game;
import de.cmdg.cmdpong.model.DBObjects.Options;
import de.cmdg.cmdpong.model.DBObjects.User;

/**
 * Erstellt von Torsten Panzer
 * am 26.07.2016.
 */

public class DBAdapter {

    /* Statische Daten zur Verwendung der Datenbank */

    // Name und Version der Datenbank
    private static final String DATABASE_NAME = "cmdg.db";
    private static final int DATABASE_VERSION = 1;

    // Tabellennamen
    private static final String DATABASE_TABLE_PREFIX = "cmdg_";
    private static final String DATABASE_TABLE_USER = "user";
    private static final String DATABASE_TABLE_GAMES = "games";
    private static final String DATABASE_TABLE_ENEMIES = "enemies";
    private static final String DATABASE_TABLE_OPTIONS = "options";
    private static final String DATABASE_TABLE_CREDITS = "credits";

    // Allgemeine Columns
    public static final String KEY_CREATEDON = "createdOn";
    public static final String KEY_MODIFIEDON = "modifiedOn";

    // Indiezes Allgemeine Columns
    public static final int COLUMN_CREATEDON_INDEX = 1;
    public static final int COLUMN_MODIFIEDON_INDEX = 2;

    // Columns Tabelle: User
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_USER_NAME = "name";
    public static final String[] KEYS_USER = {
            KEY_USER_ID, KEY_CREATEDON, KEY_MODIFIEDON, KEY_USER_NAME
    };

    // Indiezes Columns Tabelle: User
    public static final int COLUMN_USER_ID_INDEX = 0;
    public static final int COLUMN_USER_NAME_INDEX = 3;

    // Columns Tabelle: Enemy
    public static final String KEY_ENEMY_ID = "enemyId";
    public static final String KEY_ENEMY_GUID = "guid";
    public static final String KEY_ENEMY_NAME = "name";
    public static final String[] KEYS_ENEMY = {
            KEY_ENEMY_ID, KEY_CREATEDON, KEY_MODIFIEDON, KEY_ENEMY_GUID, KEY_ENEMY_NAME
    };

    // Indiezes Columns Tabelle: Enemy
    public static final int COLUMN_ENEMY_ID_INDEX = 0;
    public static final int COLUMN_ENEMY_GUID_INDEX = 3;
    public static final int COLUMN_ENEMY_NAME_INDEX = 4;

    // Columns Tabelle: Games
    public static final String KEY_GAME_ID = "gameId";
    public static final String KEY_GAME_POINTS_USER = "pointsUser";
    public static final String KEY_GAME_POINTS_ENEMY = "pointsEnemy";
    public static final String[] KEYS_GAME = {
            KEY_GAME_ID, KEY_CREATEDON, KEY_MODIFIEDON, KEY_USER_ID, KEY_ENEMY_ID, KEY_GAME_POINTS_USER, KEY_GAME_POINTS_ENEMY
    };

    // Indiezes Columns Tabelle: Games
    public static final int COLUMN_GAME_ID_INDEX = 0;
    public static final int COLUMN_GAME_USER_ID_INDEX = 3;
    public static final int COLUMN_GAME_ENEMY_ID_INDEX = 4;
    public static final int COLUMN_GAME_POINTS_USER_INDEX = 5;
    public static final int COLUMN_GAME_POINTS_ENEMY_INDEX = 6;

    // Columns Tabelle: Options
    public static final String KEY_OPTIONS_ID = "optionsId";
    public static final String KEY_OPTIONS_KEY = "optionsKey";
    public static final String KEY_OPTIONS_VALUE = "optionsValue";
    public static final String[] KEYS_OPTIONS = {
            KEY_OPTIONS_ID, KEY_CREATEDON, KEY_MODIFIEDON, KEY_OPTIONS_KEY, KEY_OPTIONS_VALUE
    };
    public static final String KEYS_OPTIONS_ID_LOADING_SYMBOL = "loadingSymbol";
    public static final String KEYS_OPTIONS_ID_SOUND_ON_OFF = "soundOnOff";
    public static final String KEYS_OPTIONS_ID_LAST_BLUETOOTH_STATE = "lastBluetoothState";
    public static final String KEYS_OPTIONS_ID_LAST_USER = "lastUser";


    // Indiezes Columns Tabelle: Options
    public static final int COLUMN_OPTIONS_ID_INDEX = 0;
    public static final int COLUMN_OPTIONS_KEY_INDEX = 3;
    public static final int COLUMN_OPTIONS_VALUE_INDEX = 4;

    /* ENDE Statische Daten  zur Verwendung der Datenbank */

    private DBOpenHelper dbHelper;
    private SQLiteDatabase db;

    // Wird vor jeder Operation (insert, update, delete, select) gesetzt und dann zur internen
    // Verarbeitung verwendet.
    private String currentTableName = null;
    private String currentPrimaryKey = null;
    private String[] currentKeyList = null;

    /**
     *
     * @param context Wird zu Ermittlung der richtigen Datenbank benötigt.
     */
    public DBAdapter(Context context) {
        dbHelper = new DBOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     *
     * @throws SQLException
     */
    public void open() throws SQLException {
        if(db == null || !db.isOpen()) {
            try {
                db = dbHelper.getWritableDatabase();
            } catch (SQLException e) {
                db = dbHelper.getReadableDatabase();
            }
        }
    }

    /**
     * Prüft ob eine Option in der Datenbank vorhanden ist und liefert dieses zurück.
     *
     * @param key
     * @return Options-Objekt, wenn es einen passenden Eintrag in der Datenbank gibt. Ansonsten neues Objekt mit leeren value.
     */
    public Options getOption(String key) {

        if(this.initDBOperation(DBObject.Type.OPTIONS)) {

            ConditionBuilder cb = new ConditionBuilder();
            cb.whereAnd(KEY_OPTIONS_KEY, key);
            ArrayList<Options> ol = this.getOptionsByCondition(cb.build());

            if(ol != null && ol.size() > 0) {
                return ol.get(0);
            }

        }

        return new Options(key, "");

    }

    /**
     * Prüft ob eine Enemy in der Datenbank vorhanden ist und liefert dieses zurück.
     *
     * @param name
     * @param guid
     * @return Enemy-Objekt, wenn es einen passenden Eintrag in der Datenbank gibt. Ansonsten neues Objekt mit übergebenen value.
     */
    public Enemy getEnemy(String name, String guid) {

        if(this.initDBOperation(DBObject.Type.OPTIONS)) {

            ConditionBuilder cb = new ConditionBuilder();
            cb.whereAnd(KEY_ENEMY_GUID, guid);
            ArrayList<Enemy> el = this.getEnemiesByCondition(cb.build());

            if(el != null && el.size() > 0) {
                return el.get(0);
            }

        }

        return new Enemy(name, guid);

    }

    /**
     *
     */
    public void close() {
        if(db != null && db.isOpen()) {
            db.close();
        }
    }

    /**
     * Aktualisiert einen Eintrag in der Datenbank.
     *
     * @param dbo Objekt vom Typ DBObject
     * @return id des Objekts.
     */
    public long update(DBObject dbo) {

        // Operation anhand des Typs initialisieren. Setzt: tableName, primaryKeyName und ColumnList
        if(this.initDBOperation(dbo.getType())) {

            if(dbo.getId() > 0) { // Id vorhanden, dann update ansonsten insert

                // Erzeugen der Condition und der Condition Args
                ConditionBuilder cb = new ConditionBuilder();
                cb.whereAnd(this.currentPrimaryKey, String.valueOf(dbo.getId()));
                ConditionBuilder.ConditionResult cr = cb.build();

                ContentValues cv = this.createContentValues(dbo);
                cv.put(KEY_MODIFIEDON, new Date().getTime());

                this.open(); // Öffnen der Datenbankverbindung

                if(db.update(this.currentTableName, cv, cr.condition, cr.conditionArgs) == 0 ) {
                    this.close(); // Schliessen der Datenbankverbindung
                    return -1; // No Rows affected --> Fehler beim updaten
                }

                this.close();  // Schliessen der Datenbankverbindung

            } else { // Keine Id vorhanden, dann insert

                this.insert(dbo);

            }

            return dbo.getId();

        }

        return -1; // No Rows affected --> Fehler beim updaten
    }

    /**
     * Fügt einen neuen Eintrag in der Datenbank ein.
     *
     * @param dbo Objekt vom Typ DBObject
     * @return id des Objekts.
     */
    public long insert(DBObject dbo) {

        // Operation anhand des Typs initialisieren. Setzt: tableName, primaryKeyName und ColumnList
        if(this.initDBOperation(dbo.getType())) {

            if(dbo.getId() > 0) { // Id vorhanden, dann update ansonsten insert

                this.update(dbo);

            } else { // Keine Id vorhanden, dann insert

                // Erzeugen der ConditionValues
                ContentValues cv = this.createContentValues(dbo);
                cv.put(KEY_CREATEDON, new Date().getTime());
                cv.put(KEY_MODIFIEDON, new Date().getTime());

                this.open(); // Öffnen der Datenbankverbindung

                long id = db.insert(this.currentTableName, null, cv);

                this.close(); // Schliessen der Datenbankverbindung

                // Setzen der Id im Objekt
                dbo.setId((int) id);

            }

            return dbo.getId();

        }

        return -1; // No Rows affected --> Fehler beim updaten
    }


    /**
     * Löscht einen Eintrag aus der Datenbank.
     *
     * @param dbo Ein Objekt vom Typ DBObject
     * @return affectedRows
     */
    public int delete(DBObject dbo) {


        // Operation anhand des Typs initialisieren. Setzt: tableName, primaryKeyName und ColumnList
        if(this.initDBOperation(dbo.getType())) {

            // Erzeugen der Condition und der Condition Args
            ConditionBuilder cb = new ConditionBuilder();
            cb.whereAnd(this.currentPrimaryKey, String.valueOf(dbo.getId()));
            ConditionBuilder.ConditionResult cr = cb.build();

            this.open(); // Öffnen der Datenbankverbindung

            int affectedRows = db.delete(this.currentTableName, cr.condition, cr.conditionArgs);

            this.close(); // Schliessen der Datenbankverbindung

            return affectedRows;

        }

        return 0;
    }

    /**
     *
     * @param id
     * @return User-Objekt oder null
     */
    public User getUserById(long id) {
        return (User) this.getByPrimaryKey(DBObject.Type.USER, id);
    }

    /**
     *
     * @return Liste von User-Objekten oder null
     */
    public ArrayList<User> getAllUser() {
        return (ArrayList<User>) this.getByCondition(DBObject.Type.USER, null); }

    /**
     *
     * @param cr ConditionResult erzeugt. Wird über ConditionBuilder .build() erzeugt.
     * @return Liste von User-Objekten oder null
     */
    public ArrayList<User> getUserByCondition(ConditionBuilder.ConditionResult cr) {
        return (ArrayList<User>) this.getByCondition(DBObject.Type.USER, cr);
    }

    /**
     *
     * @param id
     * @return Enemy-Objekt oder null
     */
    public Enemy getEnemyById(long id) {
        return (Enemy) this.getByPrimaryKey(DBObject.Type.ENEMY, id);
    }

    /**
     *
     * @return Liste von Enemy-Objekten oder null
     */
    public ArrayList<Enemy> getAllEnemies() { return (ArrayList<Enemy>) this.getByCondition(DBObject.Type.ENEMY, null); }

    /**
     *
     * @param cr ConditionResult erzeugt. Wird über ConditionBuilder .build() erzeugt.
     * @return Liste von Enemy-Objekten oder null
     */
    public ArrayList<Enemy> getEnemiesByCondition(ConditionBuilder.ConditionResult cr) {
        return (ArrayList<Enemy>) this.getByCondition(DBObject.Type.ENEMY, cr);
    }

    /**
     *
     * @param id
     * @return Game-Objekt oder null
     */
    public Game getGameById(long id) {
        return (Game) this.getByPrimaryKey(DBObject.Type.GAME, id);
    }

    /**
     *
     * @return Liste von Game-Objekten oder null
     */
    public ArrayList<Game> getAllGames() { return (ArrayList<Game>) this.getByCondition(DBObject.Type.GAME, null); }

    /**
     *
     * @param cr ConditionResult erzeugt. Wird über ConditionBuilder .build() erzeugt.
     * @return Liste von Game-Objekten oder null
     */
    public ArrayList<Game> getGamesByCondition(ConditionBuilder.ConditionResult cr) {
        return (ArrayList<Game>) this.getByCondition(DBObject.Type.GAME, cr);
    }

    /**
     *
     * @param id
     * @return Options-Objekt oder null
     */
    public Options getOptionById(long id) { return (Options) this.getByPrimaryKey(DBObject.Type.OPTIONS, id); }

    /**
     *
     * @return Liste von Options-Objekten oder null
     */
    public ArrayList<Options> getAllOptions() { return (ArrayList<Options>) this.getByCondition(DBObject.Type.OPTIONS, null); }

    /**
     *
     * @param cr ConditionResult erzeugt. Wird über ConditionBuilder .build() erzeugt.
     * @return Liste von Options-Objekten oder null
     */
    public ArrayList<Options> getOptionsByCondition(ConditionBuilder.ConditionResult cr) {
        return (ArrayList<Options>) this.getByCondition(DBObject.Type.OPTIONS, cr);
    }

    /**
     * Holt Einträge eines DBObjects aus der Datenbank anhander der Condition oder alle wenn Condition == null.
     *
     * @param type Type des DBObject-Objektes.
     * @param cr ConditionResult-Objekt. Zu erzeugen über ConditionBuiler .build()
     * @return ArrayList vom Typ User, Game, Enemy, Options
     */
    private Object getByCondition(DBObject.Type type, ConditionBuilder.ConditionResult cr) {

        if(this.initDBOperation(type)) {

            Cursor cursor = null;

            try {
                this.open(); // Öffnen der Datenbankverbindung

                if (cr == null) {  // Keine Condition mitgegeben, hole alle Einträge

                    cursor = db.query(this.currentTableName, this.currentKeyList, null, null, null, null, null);

                } else {

                    cursor = db.query(this.currentTableName, this.currentKeyList, cr.condition, cr.conditionArgs, null, null, cr.orderBy);

                }

                if (cursor.moveToFirst()) {

                    switch (type) {
                        case USER:
                            ArrayList<User> userList = new ArrayList<>();
                            do {
                                userList.add(this.createUserObject(cursor));
                            } while (cursor.moveToNext());
                            return userList;
                        case ENEMY:
                            ArrayList<Enemy> enemyList = new ArrayList<>();
                            do {
                                enemyList.add(this.createEnemyObject(cursor));
                            } while (cursor.moveToNext());
                            return enemyList;
                        case GAME:
                            ArrayList<Game> gameList = new ArrayList<>();
                            do {
                                gameList.add(this.createGameObject(cursor));
                            } while (cursor.moveToNext());
                            return gameList;
                        case OPTIONS:
                            ArrayList<Options> optionsList = new ArrayList<>();
                            do {
                                optionsList.add(this.createOptionsObject(cursor));
                            } while (cursor.moveToNext());
                            return optionsList;
                        default: // ungültiger Typ
                            return null;
                    }
                }

            } finally {
                if(cursor != null) {
                    cursor.close(); // Schliessen des Cursors
                }
            }

            this.close(); // Schliessen der Datenbankverbindung

        }

        return null;

    }

    /**
     * Holt einen Eintrag aus der Datenbank, anhand der übergebenen ID
     *
     * @param type DBObject-Type
     * @param id id des Objektes
     * @return Ein DBObject-Objekt (User, Game, Option, Enemy)
     */
    private DBObject getByPrimaryKey(DBObject.Type type, long id) {

        // Operation anhand des Typs initialisieren. Setzt: tableName, primaryKeyName und ColumnList
        if(this.initDBOperation(type)) {

            // Erzeugen der Condition und der Condition Args
            ConditionBuilder cb = new ConditionBuilder();
            cb.whereAnd(this.currentPrimaryKey, String.valueOf(id));

            Object list = this.getByCondition(type, cb.build());

            switch (type) {
                case USER:
                    if(list != null) {
                        ArrayList<User> userList = (ArrayList<User>) list;
                        if (userList.size() > 0) {
                            return userList.get(0);
                        }
                    }
                    break;
                case ENEMY:
                    if(list != null) {
                        ArrayList<Enemy> enemyList = (ArrayList<Enemy>) list;
                        if (enemyList.size() > 0) {
                            return enemyList.get(0);
                        }
                    }
                    break;
                case GAME:
                    if(list != null) {
                        ArrayList<Game> gamesList = (ArrayList<Game>) list;
                        if (gamesList.size() > 0) {
                            return gamesList.get(0);
                        }
                    }
                    break;
                case OPTIONS:
                    if(list != null) {
                        ArrayList<Options> optionsList = (ArrayList<Options>) list;
                        if (optionsList.size() > 0) {
                            return optionsList.get(0);
                        }
                    }
                    break;
                default: // ungültiger Typ
                    return null;
            }

        }

        return null;

    }

    /**
     *
     * @param cursor
     * @return User
     */
    private User createUserObject(Cursor cursor) {

        User obj = new User();
        obj.setId(cursor.getInt(COLUMN_USER_ID_INDEX));
        obj.setCreatedOn(new Date(cursor.getInt(COLUMN_CREATEDON_INDEX)));
        obj.setModifiedOn(new Date(cursor.getInt(COLUMN_MODIFIEDON_INDEX)));
        obj.setName(cursor.getString(COLUMN_USER_NAME_INDEX));

        return obj;

    }

    /**
     *
     * @param cursor
     * @return Game
     */
    private Game createGameObject(Cursor cursor) {

        Game obj = new Game();
        obj.setId(cursor.getInt(COLUMN_GAME_ID_INDEX));
        obj.setCreatedOn(new Date(cursor.getInt(COLUMN_CREATEDON_INDEX)));
        obj.setModifiedOn(new Date(cursor.getInt(COLUMN_MODIFIEDON_INDEX)));
        obj.setUser(this.getUserById(cursor.getInt(COLUMN_GAME_USER_ID_INDEX)));
        obj.setEnemy(this.getEnemyById(cursor.getInt(COLUMN_GAME_ENEMY_ID_INDEX)));
        obj.setPointsUser(cursor.getInt(COLUMN_GAME_POINTS_USER_INDEX));
        obj.setPointsEnemy(cursor.getInt(COLUMN_GAME_POINTS_ENEMY_INDEX));

        return obj;

    }

    /**
     *
     * @param cursor
     * @return Enemy
     */
    private Enemy createEnemyObject(Cursor cursor) {

        Enemy obj = new Enemy();
        obj.setId(cursor.getInt(COLUMN_ENEMY_ID_INDEX));
        obj.setCreatedOn(new Date(cursor.getInt(COLUMN_CREATEDON_INDEX)));
        obj.setModifiedOn(new Date(cursor.getInt(COLUMN_MODIFIEDON_INDEX)));
        obj.setName(cursor.getString(COLUMN_ENEMY_NAME_INDEX));
        obj.setGuid(cursor.getString(COLUMN_ENEMY_GUID_INDEX));

        return obj;

    }

    /**
     *
     * @param cursor
     * @return Enemy
     */
    private Options createOptionsObject(Cursor cursor) {

        Options obj = new Options();
        obj.setId(cursor.getInt(COLUMN_OPTIONS_ID_INDEX));
        obj.setCreatedOn(new Date(cursor.getInt(COLUMN_CREATEDON_INDEX)));
        obj.setModifiedOn(new Date(cursor.getInt(COLUMN_MODIFIEDON_INDEX)));
        obj.setKey(cursor.getString(COLUMN_OPTIONS_KEY_INDEX));
        obj.setValue(cursor.getString(COLUMN_OPTIONS_VALUE_INDEX));

        return obj;

    }

    /**
     *
     * @param dbo
     * @retur Valide ContentValues aus einem DBOject, die zum insert/update benötigt werden
     */
    private ContentValues createContentValues(DBObject dbo) {

        ContentValues cv = new ContentValues();
        // Holen der Daten zum Schreiben in die Datenbank
        HashMap<String, String> dto = dbo.getDTO();

        // Iterieren über alle Daten und füllen der Content Values mit Daten zum einfügen
        for (Map.Entry<String, String> entry : dto.entrySet()) {

            String key = entry.getValue();
            String value = entry.getValue();

            if(key != null && !key.equals(this.currentPrimaryKey) && value != null) {  // Daten werden nur gespeichert, wenn der Wert ungleich null ist
                cv.put(entry.getKey(), value);                                          // und der Key nicht dem PrimaryKey entspricht
            }
        }

        return cv;
    }

    /**
     * Setzen der Werte für currentTableName, currentPrimaryKey, currentKeyList
     * anhand des Typs.
     *
     * @param type Typ des DBObject
     * @return boolean
     */
    private boolean initDBOperation(DBObject.Type type) {

        switch (type) {
            case USER:
                this.currentTableName = DATABASE_TABLE_USER;
                this.currentPrimaryKey = KEY_USER_ID;
                this.currentKeyList = KEYS_USER;
                return true;
            case ENEMY:
                this.currentTableName = DATABASE_TABLE_ENEMIES;
                this.currentPrimaryKey = KEY_ENEMY_ID;
                this.currentKeyList = KEYS_ENEMY;
                return true;
            case GAME:
                this.currentTableName = DATABASE_TABLE_GAMES;
                this.currentPrimaryKey = KEY_GAME_ID;
                this.currentKeyList = KEYS_GAME;
                return true;
            case OPTIONS:
                this.currentTableName = DATABASE_TABLE_OPTIONS;
                this.currentPrimaryKey = KEY_OPTIONS_ID;
                this.currentKeyList = KEYS_OPTIONS;
                return true;
            default: // ungültiger Typ
                this.currentTableName = null;
                this.currentPrimaryKey = null;
                this.currentKeyList = null;
                return false;
        }

    }

    // Helper-Class zum Öffnen und erzeugen der Datenbank und der Tabellen
    private class DBOpenHelper extends SQLiteOpenHelper {

        // Create Kommando für die Tabelle User
        private static final String TABLE_USER_CREATE = "create table "
                + DATABASE_TABLE_USER + " (" + KEY_USER_ID
                + " integer primary key autoincrement, "
                + KEY_USER_NAME + " text not null, "
                + KEY_CREATEDON + " integer not null, "
                + KEY_MODIFIEDON + " integer"
                + ");";

        // Create Kommando für die Tabelle Games
        private static final String TABLE_GAMES_CREATE = "create table "
                + DATABASE_TABLE_GAMES + " (" + KEY_GAME_ID
                + " integer primary key autoincrement, "
                + KEY_CREATEDON + " integer not null, "
                + KEY_MODIFIEDON + " integer, "
                + KEY_USER_ID + " integer not null, "
                + KEY_ENEMY_ID + " integer not null, "
                + KEY_GAME_POINTS_USER + " integer not null, "
                + KEY_GAME_POINTS_ENEMY + " integer not null "
                + ");";

        // Create Kommando für die Tabelle Enemies
        private static final String TABLE_ENEMIES_CREATE = "create table "
                + DATABASE_TABLE_ENEMIES + " (" + KEY_ENEMY_ID
                + " integer primary key autoincrement, "
                + KEY_CREATEDON + " integer not null, "
                + KEY_MODIFIEDON + " integer, "
                + KEY_ENEMY_GUID + " text not null, "
                + KEY_ENEMY_NAME + " text not null "
                + ");";

        // Create Kommando für die Tabelle Options
        private static final String TABLE_OPTIONS_CREATE = "create table "
                + DATABASE_TABLE_OPTIONS + " (" + KEY_OPTIONS_ID
                + " integer primary key autoincrement, "
                + KEY_CREATEDON + " integer not null, "
                + KEY_MODIFIEDON + " integer, "
                + KEY_OPTIONS_KEY + " text not null, "
                + KEY_OPTIONS_VALUE + " text not null "
                + ");";

        /**
         *
         * @param c Der aktuelle Kontext
         * @param dbname Der Name der Datenbank
         * @param factory Factory-Klasse zur Erzeugung eines Cursors
         * @param version Die Version der Datenbank
         */
        public DBOpenHelper(Context c, String dbname, SQLiteDatabase.CursorFactory factory, int version) {
                super(c, dbname, factory, version);
        }

        /**
         *
         * @param db Referenz auf die Datenbanklasse
         */
        @Override
        public void onCreate(SQLiteDatabase db) {
            // Erzeugung aller Tabellen
            db.execSQL(TABLE_USER_CREATE);
            db.execSQL(TABLE_ENEMIES_CREATE);
            db.execSQL(TABLE_GAMES_CREATE);
            db.execSQL(TABLE_OPTIONS_CREATE);
        }

        /**
         *
         * @param db Referenz auf die Datenbanklasse
         * @param oldVersion Alte Versionsnummer der Datenbank
         * @param newVersion Neue Versionsnummer der Datenbank
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

}
