package de.cmdg.cmdpong.helper;

import android.content.Context;
import android.media.MediaPlayer;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.model.DBObjects.Options;

/**
 * Erstellt von Michael Lorenz
 * am 05.08.2016.
 * Der SoundStore ist der zentrale Anlaufpunkt, wenn Sounds/Musik abegespielt werden sollen.
 * Er verwaltet alle im Projekt benötigten Dateien und bietet für jede eine play()-Methode an.
 */
public class SoundStore {

    public static int WALLHIT = R.raw.wallhit;
    private MediaPlayer mPWallHit;
    public static int BATHIT = R.raw.bathit;
    private MediaPlayer mPBatHit;
    public static int GOALUSER = R.raw.goaluser;
    private MediaPlayer mPGoalUser;
    public static int GOALENEMY = R.raw.goalenemy;
    private MediaPlayer mPGoalEnemy;
    public static int WINGAME = R.raw.wingame;
    private MediaPlayer mPWinGame;
    public static int LOSEGAME = R.raw.losegame;
    private MediaPlayer mPLoseGame;
    private boolean play;

    public SoundStore(Context context) {
        DBAdapter dba = new DBAdapter(context);
        Options opt = dba.getOption(DBAdapter.KEYS_OPTIONS_ID_SOUND_ON_OFF);
        play = opt.getValue().equals("true");
        mPWallHit = MediaPlayer.create(context, WALLHIT);
        mPBatHit = MediaPlayer.create(context, BATHIT);
        mPGoalUser = MediaPlayer.create(context, GOALUSER);
        mPGoalEnemy = MediaPlayer.create(context, GOALENEMY);
        mPWinGame = MediaPlayer.create(context, WINGAME);
        mPLoseGame = MediaPlayer.create(context, LOSEGAME);
    }

    private void playSound(MediaPlayer mP) {
        if (play) {
            try {
                mP.start();
            } catch (Exception e) {
                CmdgLog.e(e.getMessage());
            }
        }
    }

    public void playWallHit() {
        playSound(mPWallHit);
    }

    public void playBatHit() {
        playSound(mPBatHit);
    }

    public void playGoalUser() {
        playSound(mPGoalUser);
    }

    public void playGoalEnemy() {
        playSound(mPGoalEnemy);
    }

    public void playWinGame() {
        playSound(mPWinGame);
    }

    public void playLoseGame() {
        playSound(mPLoseGame);
    }
}
