package de.cmdg.cmdpong.helper;

import java.util.ArrayList;

/**
 * Erstellt von Torsten Panzer
 * am 26.07.2016.
 */

// Helper-Class zum Erzeugen von Conditions für Update, Delete and Select
public class ConditionBuilder {

    public static final String KEY_ORDER_BY = "order_by";
    public static final String KEY_ORDER_BY_ARGS = "order_by_args";
    public static final String KEY_CONDITION = "condition";
    public static final String KEY_ARGS = "args";

    public enum ORDERTYPES {
        DESC, ASC
    }

    String condition = null;
    ArrayList<String> conditionArgs = null;
    String orderBy = null;

    /**
     *  Standardkonstruktor
     */
    public ConditionBuilder() {

        conditionArgs = new ArrayList<>();

    }

    /**
     *
     * @param column Name der Spalte
     * @param value Wert der Spalte
     * @return ConditionBuilder-Objekt für Chaining
     */
    public ConditionBuilder whereAnd(String column, String value) {
        return this.where(column, value, "AND");
    }

    /**
     *
     * @param column Name der Spalte
     * @param value Wert der Spalte
     * @return ConditionBuilder-Objekt für Chaining
     */
    public ConditionBuilder whereOr(String column, String value) {
        return this.where(column, value, "OR");
    }

    /**
     *
     * @param column Name der Spalte
     * @param value Wert der Spalte
     * @param type Type OR | AND
     * @return ConditionBuilder-Objekt für Chaining
     */
    private ConditionBuilder where(String column, String value, String type) {

        if(type.equals("OR") || type.equals("AND")) {

            if(this.condition == null) {
                this.condition = "";
            }

            this.condition += (condition.equals("")) ? column : " " + type + " " + column;
            this.condition += "=?";

            this.conditionArgs.add(value);

        }

        return this;
    }

    /**
     *
     * @param column
     * @return ConditionBuilder
     */
    public ConditionBuilder orderBy(String column) {
        return this.orderBy(column, ORDERTYPES.ASC);
    }

    /**
     *
     * @param column
     * @param orderType
     * @return ConditionBuilder
     */
    public ConditionBuilder orderBy(String column, ORDERTYPES orderType) {
        this.orderBy = column + " " + ((orderType == ORDERTYPES.ASC) ? "ASC" : "DESC");
        return this;
    }

    /**
     *
     * @return Eine HashMap mit zwei Keys: 'condition' liefert den condition-String, 'args' liefert das Args-Array
     */
    public ConditionResult build() {
        return new ConditionResult(condition, conditionArgs, orderBy);
    }

    /**
     * Ergebnis vom ConditionBuilder. Erlaubt Zugriff auf einen fertigen conditionString und eine argsArray
     */
    public class ConditionResult {

        public String condition = "";
        public String[] conditionArgs = null;
        public String orderBy = null;

        /**
         *
         * @param condition ConditionString
         * @param conditionArgs ArrayList mit allen Args
         */
        public ConditionResult(String condition, ArrayList<String> conditionArgs, String orderBy) {
            this.condition = condition;
            this.conditionArgs = new String[conditionArgs.size()];

            for(int i = 0; i < conditionArgs.size(); i++) {
                this.conditionArgs[i] = conditionArgs.get(i);
            }

            this.orderBy = orderBy;
        }

    }

}
