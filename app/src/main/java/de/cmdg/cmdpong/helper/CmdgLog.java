package de.cmdg.cmdpong.helper;

import android.util.Log;

/**
 * Erstellt von Torsten Panzer
 * am 27.07.2016.
 */

public class CmdgLog {

    // Enum für alle Staging-Typen.
    // Zeigen den aktuellen Status des Projektes
    public enum STAGINGTYPES {
        DEVELOPMENT, TEST, PRODUCTION
    }

    // Enum für alle Log-Levels.
    // In Abhängigkeit davon werden andere Ausgaben erzeugt.
    public enum LOGTYPES {
        DEBUG, WARNING, ERROR, INFO
    }

    // Aktueller Status des Projektes
    public static final STAGINGTYPES STAGING = STAGINGTYPES.PRODUCTION;

    // Standardtag unter dem ein Log-Eintrag hinzugefügt wird,
    // wenn keine eigener Tag angegeben wurde.
    public static final String DEFAULTTAG = "CmdgLog";

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param s
     */
    public static void d(float s) {
        d(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void d(String tag, float s) {
        d(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param s
     */
    public static void d(double s) {
        d(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void d(String tag, double s) {
        d(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param s
     */
    public static void d(long s) {
        d(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void d(String tag, long s) {
        d(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param s
     */
    public static void d(int s) {
        d(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void d(String tag, int s) {
        d(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param s
     */
    public static void d(short s) {
        d(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void d(String tag, short s) {
        d(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param s
     */
    public static void d(boolean s) {
        d(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void d(String tag, boolean s) {
        d(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param s
     */
    public static void d(String s) {
        d(DEFAULTTAG, s);
    }

    /**
     * Fügt dem Log eine Debug-Meldung hinzu.
     *
     * @param s
     */
    public static void d(String tag, String s) {
        l(LOGTYPES.DEBUG, tag, s);
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param s
     */
    public static void i(float s) {
        i(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void i(String tag, float s) {
        i(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param s
     */
    public static void i(double s) {
        i(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void i(String tag, double s) {
        i(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param s
     */
    public static void i(long s) {
        i(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void i(String tag, long s) {
        i(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param s
     */
    public static void i(int s) {
        i(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void i(String tag, int s) {
        i(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param s
     */
    public static void i(short s) {
        i(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void i(String tag, short s) {
        i(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param s
     */
    public static void i(boolean s) {
        i(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void i(String tag, boolean s) {
        i(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param s
     */
    public static void i(String s) {
        i(DEFAULTTAG, s);
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param s
     */
    public static void i(String tag, String s) {
        l(LOGTYPES.INFO, tag, s);
    }

    /**
     * Fügt dem Log eine Info-Meldung hinzu.
     *
     * @param s
     */
    public static void w(float s) {
        w(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void w(String tag, float s) {
        w(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param s
     */
    public static void w(double s) {
        w(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void w(String tag, double s) {
        w(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param s
     */
    public static void w(long s) {
        w(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void w(String tag, long s) {
        w(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param s
     */
    public static void w(int s) {
        w(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void w(String tag, int s) {
        w(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param s
     */
    public static void w(short s) {
        w(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void w(String tag, short s) {
        w(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param s
     */
    public static void w(boolean s) {
        w(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void w(String tag, boolean s) {
        w(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param s
     */
    public static void w(String s) {
        w(DEFAULTTAG, s);
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param s
     */
    public static void w(String tag, String s) {
        l(LOGTYPES.WARNING, tag, s);
    }

    /**
     * Fügt dem Log eine Warning-Meldung hinzu.
     *
     * @param s
     */
    public static void e(float s) {
        e(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void e(String tag, float s) {
        e(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param s
     */
    public static void e(double s) {
        e(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void e(String tag, double s) {
        e(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param s
     */
    public static void e(long s) {
        e(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void e(String tag, long s) {
        e(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param s
     */
    public static void e(int s) {
        e(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void e(String tag, int s) {
        e(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param s
     */
    public static void e(short s) {
        e(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void e(String tag, short s) {
        e(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param s
     */
    public static void e(boolean s) {
        e(String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param tag
     * @param s
     */
    public static void e(String tag, boolean s) {
        e(tag, String.valueOf(s));
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param s
     */
    public static void e(String s) {
        e(DEFAULTTAG, s);
    }

    /**
     * Fügt dem Log eine Error-Meldung hinzu.
     *
     * @param s
     */
    public static void e(String tag, String s) {
        l(LOGTYPES.ERROR, tag, s);
    }

    /**
     * Fügt dem Log einen Eintrag hinzu. Typ und Art des Eintrages ist abhängig
     * vom Parameter logType.
     *
     * @param logType
     * @param tag
     * @param s
     */
    private static void l(LOGTYPES logType, String tag, String s) {
        switch(logType) {
            case DEBUG:
                if(STAGING == STAGINGTYPES.DEVELOPMENT || STAGING == STAGINGTYPES.TEST) {
                    Log.d(tag, s);
                }
                break;
            case INFO:
                if(STAGING == STAGINGTYPES.DEVELOPMENT || STAGING == STAGINGTYPES.TEST) {
                    Log.i(tag, s);
                }
                break;
            case WARNING:
                if(STAGING == STAGINGTYPES.DEVELOPMENT || STAGING == STAGINGTYPES.TEST) {
                    Log.w(tag, s);
                }
                break;
            case ERROR: // Wird immer ausgegeben
                    Log.e(tag, s);
                break;
            default: // Default wird eine Info ausgegeben.
                    Log.i(tag, s);
                break;
        }
    }


}
