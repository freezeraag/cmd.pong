package de.cmdg.cmdpong.controller;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

import de.cmdg.cmdpong.viewController.GamePanel;

/**
 * Erstellt von Michael Lorenz
 * am 28.07.2016.
 * Diese Klasse stellt den eigentlichen Main-Thread des Spiels dar.
 * Die Aufgabe ist das Bereitstellen eines nahezu FPS-konstanten Game-Loops.
 * Dieser wird, bei den hier konfigurierten 60 FPS, 60 mal in der Sekunde durchlaufen
 * und ruft dabei jedesmal die GamePanel-Methoden update() und draw() auf.
 * Die run()-Methode startet den Game-Loop
 */
public class MainGameThread extends Thread {
    public static final int FPS = 60;
    private final SurfaceHolder surfaceHolder;
    private GamePanel gamePanel;
    private boolean running;
    private boolean paused;
    private final Object waitObject = new Object();
    public static Canvas canvas;

    public MainGameThread(SurfaceHolder surfaceHolder, GamePanel gamePanel) {
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel = gamePanel;
        this.paused = false;
    }

    /**
     * Diese Methode beinhaltet den Game-Loop.
     * Vorgehensweise:
     * - Zielzeit ermitteln, teile 1 Sekunde durch 60 (FPS)
     * - versuche in einem Durchlauf, d.h. update()- und draw()-Methode diese Zeit einzuhalten
     * - wurde die Zeit unterschritten, warte bis Zielzeit erreicht, sonst nächster Loop
     */
    @Override
    public void run() {
        long startTime;
        long timeMillis;
        long waitTime;
        int frameCount = 0;
        long targetTime = 1000 / FPS;

        while (running) {
            if(paused) {  // Spiel wurde durch einen Spieler pausiert
                synchronized (waitObject) {
                    try {
                        waitObject.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            startTime = System.nanoTime();

            // versuche den canvas für das Pixeleditieren zu laden
            canvas = null;
            try {
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder) {
                    this.gamePanel.update();
                    this.gamePanel.draw(canvas);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (canvas != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            timeMillis = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - timeMillis;

            if (waitTime > 0) {
                try {
                    Thread.sleep(waitTime);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            frameCount++;

            if (frameCount == FPS) {
                //double averageFps = 1000 / ((totalTime / frameCount) / 1000000);
                //CmdgLog.d("FPS: ", averageFps);
                frameCount = 0;
            }
        }
    }

    /**
     * Bedingt das Starten bzw. Stoppen des Game-Loops
     * @param running
     */
    public void setRunning(boolean running) {
        this.running = running;
    }

    /**
     * Veranlasst, dass der Game-Loop pausiert wird.
     * @param paused
     */
    public void setPaused(boolean paused) {
        this.paused = paused;
        if(!paused) {
            synchronized (waitObject) {
                    waitObject.notify();
            }
        }
    }
}
