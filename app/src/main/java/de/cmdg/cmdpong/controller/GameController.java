package de.cmdg.cmdpong.controller;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.ArrayList;

import de.cmdg.cmdpong.R;
import de.cmdg.cmdpong.communication.bluetooth.BTClient;
import de.cmdg.cmdpong.communication.messages.BallPositionMessage;
import de.cmdg.cmdpong.communication.messages.AcknowledgeMessage;
import de.cmdg.cmdpong.communication.messages.InitMessage;
import de.cmdg.cmdpong.communication.messages.InterruptMessage;
import de.cmdg.cmdpong.communication.messages.Message;
import de.cmdg.cmdpong.communication.MessageService;
import de.cmdg.cmdpong.communication.bluetooth.BTController;
import de.cmdg.cmdpong.helper.CmdgLog;
import de.cmdg.cmdpong.helper.ConditionBuilder;
import de.cmdg.cmdpong.helper.DBAdapter;
import de.cmdg.cmdpong.model.DBObjects.Enemy;
import de.cmdg.cmdpong.model.DBObjects.Game;
import de.cmdg.cmdpong.model.GameObjects.Ball;
import de.cmdg.cmdpong.model.DBObjects.Options;
import de.cmdg.cmdpong.model.DBObjects.User;
import de.cmdg.cmdpong.viewController.GamePanel;
import de.cmdg.cmdpong.viewController.GameScreenActivity;

/**
 * Erstellt von Michael Lorenz und Torsten Panzer
 * am 02.08.2016.
 */
public class GameController extends BroadcastReceiver {

    public static final String INIT_SERVER = "de.cmdg.cmdpong.initServer";
    public static final String INIT_CLIENT = "de.cmdg.cmdpong.initClient";
    public static final String STATE_CHANGED = "de.cmdg.cmdpong.stateChanged";

    private GameScreenActivity activity;
    private BTController btController;
    private state currentState;
    public static final String STATE_KEY = "stateKey";
    private User currentUser;
    private Enemy currentEnemy;
    private Game currentGame;
    private DBAdapter dba;
    private MessageService messageService;
    private boolean host;
    private GamePanel gamePanel;
    private long ballPositionIndex;
    private BluetoothDevice enemyBTDevice;

    public enum state {INIT, READY}

    public GameController(Activity activity, BTController btController) {
        this.activity = (GameScreenActivity) activity;
        this.btController = btController;
        this.messageService = btController.getMessageService();
        activity.registerReceiver(this, new IntentFilter(MessageService.NEW_MESSAGE_RECEIVED));

        dba = new DBAdapter(activity.getApplicationContext());
        Options lastUser = dba.getOption(DBAdapter.KEYS_OPTIONS_ID_LAST_USER);
        if (lastUser != null) {
            currentUser = dba.getUserById(Long.parseLong(lastUser.getValue()));
            if (currentUser == null) {
                CmdgLog.e("No User found. (GameController/Constructor)");
            }
        }
        CmdgLog.e("No User found. (GameController/Constructor)");
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public Enemy getCurrentEnemy() {
        return currentEnemy;
    }

    public Game getCurrentGame() {
        return currentGame;
    }

    public state getCurrentState() {
        return currentState;
    }

    private void changeState(state state) {
        currentState = state;
        Intent intent = new Intent(STATE_CHANGED);
        intent.putExtra(STATE_KEY, state.ordinal());
        activity.sendBroadcast(intent);
    }

    /**
     * @return true wenn der Spieler das Spiel eröffnet hat.
     */
    public boolean isHost() {
        return host;
    }

    /**
     * Veranlasst das versenden einer Ballpositionsnachricht via Bluetooth an das Device des Gegenspielers.
     * @param ball
     * @param index
     */
    public void sendBallPosition(Ball ball, long index) {
        if (!ball.isBlink()) {
            messageService.sendMessage(new BallPositionMessage(ball, index));
        }
    }

    /**
     * Veranlasst das versenden einer Bestätigungsnachricht (Acknowledge) via Bluetooth an das Device des Gegenspielers.
     * @param ackType
     */
    public void sendAcknowledge(AcknowledgeMessage.ackknowledgeType ackType) {
        messageService.sendMessage(new AcknowledgeMessage(ackType));
    }

    /**
     * Veranlasst das versenden einer Unterbrechungsnachricht (Interrupt) via Bluetooth an das Device des Gegenspielers.
     * @param state
     */
    public void sendInterruptMessage(InterruptMessage.INTERRUPTSTATE state) {
        messageService.sendMessage(new InterruptMessage(state));
    }

    /**
     * Lässt den Status wechseln in INIT-Status.
     * In diesem Status wird definiert, wer der Host ist.
     * Beide Spieler hören auf Nachrichten des Gegenspeielers und versenden jeweils eine INIT-Nachricht.
     * @param isHost
     */
    public void stateInit(boolean isHost) {
        this.host = isHost;
        changeState(state.INIT);
        messageService.listenForMessages();
        messageService.sendMessage(new InitMessage(currentUser.getName(), btController.getBluetoothAdapter().getAddress() + "-" + currentUser.getId()));
    }

    /**
     * Veranlasst das wiederverbinden beider Devices via Bluetooth, falls die Verbindung während
     * des Spiels abbrechen sollte.
     */
    public void reconnectGame() {
        CmdgLog.d("Reconnect: " + isHost());
        gamePanel.pauseGameLoop();
        dba.update(currentGame);
        messageService.closeInputStream();
        if (isHost()) {
            btController.startServer();
        } else {
            if (enemyBTDevice != null) {
                CmdgLog.d("Reconnect: " + enemyBTDevice.toString());
                btController.startClientConnection(enemyBTDevice);
            } else {
                CmdgLog.d("Reconnect: Exit");
                exitGame(false);
            }
        }
    }

    /**
     * Veranlasst das Pausieren des Spiels, sowohl auf dem eigenen, als auch auf dem anderen Device.
     * Der Übergabeparameter gibt an, wer der Pause-Initiierende Spieler war.
     * @param pauseSender
     */
    public void pauseGame(boolean pauseSender) {
        if (pauseSender) {
            sendInterruptMessage(InterruptMessage.INTERRUPTSTATE.PAUSE);
        }
        gamePanel.pauseGameLoop();
        if (!pauseSender) {
            activity.showPauseFragment(false);
        }
        dba.update(currentGame);
    }

    /**
     * Veranlasst die Wiederkehr in das Spiel nach einer Pause für beide Devices.
     * Der Übergabeparameter gibt an, wer der Wiederkehr-Initiierende Spieler war.
     * @param resumeSender
     */
    public void resumeGame(boolean resumeSender) {
        if (resumeSender) {
            sendInterruptMessage(InterruptMessage.INTERRUPTSTATE.RESUME);
        }
        activity.removePauseFragement();
        activity.showCountDownFragment((GamePanel.PAUSE_BEFORE_RESUME / 1000));
        gamePanel.resumeGameLoop();
    }

    /**
     * Veranlasst das Beenden des Spiels für beide Devices.
     * Der Übergabeparameter gibt an, wer der Beenden-Initiierende Spieler war.
     * @param exitSender
     */
    public void exitGame(boolean exitSender) {
        gamePanel.stopGameLoop();
        btController.getMessageService().stopListenForMessages();
        if (exitSender) {
            sendInterruptMessage(InterruptMessage.INTERRUPTSTATE.EXIT);
        }
        if (currentGame != null) {
            dba.update(currentGame);
        }
        activity.finish();
    }

    /**
     * Diese Methode wird bei Nachrichterhalt aufgerufen.
     * Hier wird der Typ der empfangenen Nachricht ausgewertet und dementsprechend weiter verfahren.
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        switch (action) {
            case INIT_CLIENT:
                enemyBTDevice = intent.getParcelableExtra(BTClient.KEY_BLUETOOTHDEVICE);
            case INIT_SERVER:
                if (currentState != state.READY) {
                    stateInit((action.equals(INIT_SERVER)));
                } else {
                    resumeGame(false);
                }
                break;
            case MessageService.NEW_MESSAGE_RECEIVED:
                checkState();
                break;
            case GamePanel.NEW_ENEMY_POINT:
            case GamePanel.NEW_USER_POINT:
                handleNewPoint((action.equals(GamePanel.NEW_USER_POINT)));
                break;
        }
    }

    /**
     * Umsetzung der State-Machine des Spiels.
     */
    private void checkState() {

        if (currentState == state.INIT) { // Wir sind im STATE INIT, der nächste wäre READY

            // Um in READY zu kommen müssen wir eine gültie InitMessage vom Gegenspieler erhalten haben.
            Message message = messageService.getNextMessage();
            if (message != null) {

                // Mit einer gültigen InitMessage können wir das Spiel initieren
                initGame((InitMessage) message.convertMessage());

                // Ändern des aktuellen States zu READY
                changeState(state.READY);

                // Zurücksetzen des Bluetooth-Anzeigenamens. Wir müssen nun nicht mehr gefiltert werden.
                btController.resetBluetoothName();
            }
        } else if (currentState == state.READY) {

            Message message = messageService.getNextMessage();

            if (message != null) {

                if (message.getType() == Message.messageTypes.BALLPOSITION) { // Ball Position vom Gegner
                    handleBallPositionMessage((BallPositionMessage) message.convertMessage());
                } else if (message.getType() == Message.messageTypes.INTERRUPT) { // Unterbrechung
                    handleInterruptMessage((InterruptMessage) message.convertMessage());
                } else if (message.getType() == Message.messageTypes.ACKNOWLEDGE) {
                    handleChangeAckMessage((AcknowledgeMessage) message.convertMessage());
                }

            }

        }
    }

    /**
     * Veranlasst die Übernahme der Spieldetails in die Datenbank nach Beendigung eines Spiels.
     */
    public void saveGame() {
        if (currentGame != null) {
            dba.update(currentGame);
        }
    }

    /**
     * Handler für Acknowledge-Nachrichten.
     * @param message
     */
    private void handleChangeAckMessage(AcknowledgeMessage message) {
        if (message.getAckType() == AcknowledgeMessage.ackknowledgeType.CHANGE) {
            gamePanel.changeAcknowledgeRetrieved();
        } else if (message.getAckType() == AcknowledgeMessage.ackknowledgeType.NEW_POINT) {
            gamePanel.newPointAcknowledgeRetrieved();
        }
    }

    /**
     * Handler für Interrupt-Nachrichten.
     * @param message
     */
    private void handleInterruptMessage(InterruptMessage message) {

        if (message.getState() == InterruptMessage.INTERRUPTSTATE.PAUSE) {
            pauseGame(false);
            activity.setPaused(true);
        } else if (message.getState() == InterruptMessage.INTERRUPTSTATE.RESUME) {
            resumeGame(false);
        } else if (message.getState() == InterruptMessage.INTERRUPTSTATE.EXIT) {
            currentGame.setPointsUser(currentGame.getPointsToWin());
            exitGame(false);
            activity.notifyExitGame(true);
        }

    }

    /**
     * Handler für Nachrichten über Punktevergabe.
     * @param userPoint
     */
    private void handleNewPoint(boolean userPoint) {
        if (userPoint) {
            currentGame.incrementUserPoints();
        } else {
            currentGame.incrementEnemyPoints();
        }

        int winner = currentGame.isOver();

        activity.updatePoints();
        activity.notifyNewPoint(userPoint);

        if (winner != Game.NO_WINNER) {
            gamePanel.stopGameLoop();
            btController.getMessageService().stopListenForMessages();
            if (winner == Game.WINNER_WINNER_CHICKEN_DINNER) {
                activity.showEndFragment(true, currentGame.getPointsUser(), currentGame.getPointsEnemy());
            } else {
                activity.showEndFragment(false, currentGame.getPointsUser(), currentGame.getPointsEnemy());
            }
        }

        saveGame();

    }

    /**
     * Handler für Ballpositionsnachrichten.
     * @param message
     */
    private void handleBallPositionMessage(BallPositionMessage message) {
        if (message.getIndex() > ballPositionIndex) {
            if (gamePanel != null) {
                gamePanel.setEnemyBallPosition(message.getX(), message.getY(), message.getMovementX(), message.getMovementY());
                ballPositionIndex++;
            }
        }
    }

    /**
     * Initialisiert das Spiel.
     * @param message
     */
    private void initGame(InitMessage message) {

        /*** Erzeugen des Gegners ***/
        // Holen aller Daten aus der Nachricht
        String enemyName = message.getSenderName();
        String guid = message.getGuid();
        ballPositionIndex = -1;

        // prüfen anhand der guid ob Gegner bereits in der Datenbank
        ConditionBuilder cb = new ConditionBuilder();
        cb.whereAnd(DBAdapter.KEY_ENEMY_GUID, guid);
        ArrayList<Enemy> el = dba.getEnemiesByCondition(cb.build());

        if (el != null && !el.isEmpty()) { // Gegner bereits in der DB

            currentEnemy = el.get(0);

            if (!currentEnemy.getName().equals(enemyName)) { // Hat sich der Name geändert?
                // Dann Enemy in der DB updaten
                currentEnemy.setName(enemyName);

                dba.update(currentEnemy);

                currentEnemy.setBluetoothDevice(enemyBTDevice);

            }
        } else { // Gegner noch nicht in der DB. Neuen Gegner erzeugen und Speichern
            currentEnemy = new Enemy(enemyName, guid);
            dba.insert(currentEnemy);
        }
        /*** Ende Erzeugung Gegner ***/

        // Neues Spiel erzeugen und in der DB speichern (für Spielstart), jetzt kann's losgehen ;)
        currentGame = new Game(currentUser, currentEnemy);
        dba.insert(currentGame);
    }

    public void setGamePanel(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }

    /**
     * Veranlasst ein sauberes Beenden der Spielumgebung.
     */
    public void finish() {
        try {
            activity.unregisterReceiver(this);
        } catch (Exception ex) {
            CmdgLog.e(ex.getMessage());
        }
    }
}
